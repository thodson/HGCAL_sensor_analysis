#### Analysis file HGC1006 ####

current mouse bite [nA]                           	2.80	1.75
current half cell [nA]                            	1.43	0.75
current standard [nA]                             	1.29	0.99
current calibration cell [nA]                     	0.17	0.02
current around calibration cell [nA]              	0.80	0.10
current guard ring [nA]                           	24.23	0.00
current test capacity 0 [nA]                      	0.05	0.00
current test capacity 1 [nA]                      	0.26	0.00
current test capacity 2 [nA]                      	0.10	0.00
current test capacity 3 [nA]                      	0.26	0.00
saturation current standard cells at voltage [V]  	622.99	142.21
capacity mouse bite [pF]                          	25.96	1.49
capacity half cell [pF]                           	31.45	1.86
capacity standard [pF]                            	52.57	2.13
capacity calibration cell [pF]                    	16.79	0.36
capacity around calibration cell [pF]             	49.76	0.10
capacity guard ring [pF]                          	68.18	0.00
capacity test capacity 0 [pF]                     	35.44	0.00
capacity test capacity 1 [pF]                     	53.27	0.00
capacity test capacity 2 [pF]                     	69.81	0.00
capacity test capacity 3 [pF]                     	147.14	0.00
saturation capacity standard cells at voltage [V] 	180.00	0.00
depletion voltage mouse bite [V]                  	175.46	10.06
depletion voltage half cell [V]                   	178.23	2.28
depletion voltage standard [V]                    	187.88	5.53
depletion voltage calibration cell [V]            	187.50	3.33
depletion voltage around calibration cell [V]     	187.80	0.90
depletion voltage guard ring [V]                  	130.02	0.00
depletion voltage test capacity 0 [V]             	88.11	0.00
depletion voltage test capacity 1 [V]             	111.70	0.00
depletion voltage test capacity 2 [V]             	105.67	0.00
depletion voltage test capacity 3 [V]             	107.00	0.00
