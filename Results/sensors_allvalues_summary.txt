#### Analysis summary file ####

#Sensors included in this file: 
#HGC1003, HGC1004, HGC1005, HGC1006, HGC1007, HGC1008, HGC1009, HGC1010, HGC1011, HGC1012, HGC1013, HGC1014, HGC1015, HGC1016, HGC1017, HGC1018, HGC1019, HGC1020, HGC1001, HGC1002;

#Categories                                       	Mean	RMS
current mouse bite [nA]                           	7.84	12.92
current half cell [nA]                            	1.91	0.81
current standard [nA]                             	1.51	0.80
current calibration cell [nA]                     	0.33	0.36
current around calibration cell [nA]              	1.03	0.78
current guard ring [nA]                           	206.02	633.17
current test capacity 0 [nA]                      	0.32	0.54
current test capacity 1 [nA]                      	0.29	0.12
current test capacity 2 [nA]                      	0.18	0.16
current test capacity 3 [nA]                      	0.25	0.09
saturation current standard cells at voltage [V]  	520.86	190.17
capacity mouse bite [pF]                          	26.42	2.25
capacity half cell [pF]                           	31.43	0.15
capacity standard [pF]                            	52.50	0.23
capacity calibration cell [pF]                    	16.78	0.03
capacity around calibration cell [pF]             	49.64	0.07
capacity guard ring [pF]                          	72.36	17.13
capacity test capacity 0 [pF]                     	35.55	0.57
capacity test capacity 1 [pF]                     	53.70	1.70
capacity test capacity 2 [pF]                     	69.62	0.74
capacity test capacity 3 [pF]                     	146.50	1.86
saturation capacity standard cells at voltage [V] 	179.94	0.14
depletion voltage mouse bite [V]                  	174.52	1.39
depletion voltage half cell [V]                   	179.35	1.07
depletion voltage standard [V]                    	187.66	0.96
depletion voltage calibration cell [V]            	183.57	3.62
depletion voltage around calibration cell [V]     	189.45	1.95
depletion voltage guard ring [V]                  	131.44	9.59
depletion voltage test capacity 0 [V]             	87.76	7.92
depletion voltage test capacity 1 [V]             	111.66	0.81
depletion voltage test capacity 2 [V]             	107.20	1.93
depletion voltage test capacity 3 [V]             	111.08	12.78
