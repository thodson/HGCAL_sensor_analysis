#### Analysis file HGC1017 ####

current mouse bite [nA]                           	55.72	175.67
current half cell [nA]                            	2.32	1.59
current standard [nA]                             	1.26	1.14
current calibration cell [nA]                     	0.36	0.12
current around calibration cell [nA]              	0.82	0.13
current guard ring [nA]                           	26.37	0.00
current test capacity 0 [nA]                      	0.06	0.00
current test capacity 1 [nA]                      	0.28	0.00
current test capacity 2 [nA]                      	0.08	0.00
current test capacity 3 [nA]                      	0.26	0.00
saturation current standard cells at voltage [V]  	710.37	128.42
capacity mouse bite [pF]                          	25.92	1.50
capacity half cell [pF]                           	31.39	1.88
capacity standard [pF]                            	52.48	2.11
capacity calibration cell [pF]                    	16.79	0.39
capacity around calibration cell [pF]             	49.66	0.15
capacity guard ring [pF]                          	68.64	0.00
capacity test capacity 0 [pF]                     	35.27	0.00
capacity test capacity 1 [pF]                     	53.11	0.00
capacity test capacity 2 [pF]                     	69.67	0.00
capacity test capacity 3 [pF]                     	147.14	0.00
saturation capacity standard cells at voltage [V] 	180.00	0.00
depletion voltage mouse bite [V]                  	173.53	9.17
depletion voltage half cell [V]                   	178.65	2.99
depletion voltage standard [V]                    	186.33	5.76
depletion voltage calibration cell [V]            	187.47	3.95
depletion voltage around calibration cell [V]     	186.69	7.64
depletion voltage guard ring [V]                  	126.37	0.00
depletion voltage test capacity 0 [V]             	89.43	0.00
depletion voltage test capacity 1 [V]             	110.99	0.00
depletion voltage test capacity 2 [V]             	107.93	0.00
depletion voltage test capacity 3 [V]             	106.77	0.00
