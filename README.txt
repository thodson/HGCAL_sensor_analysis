################################################################
######################### H E X P L O T ########################
################################################################

This is a small program to display data for hexagonal shapes.

################################################################
REQUIREMENTS:
You need gcc and root.

################################################################
INSTALLATION:
Just type 'make'. 

On SLC6 PCs or lxplus try to source this before:
. /cvmfs/sft.cern.ch/lcg/releases/gcc/4.9.3/x86_64-slc6-gcc49-opt/setup.sh
. /cvmfs/sft.cern.ch/lcg/releases/ROOT/6.06.06-a2c9d/x86_64-slc6-gcc49-opt/bin/thisroot.sh

################################################################
USAGE:
Just type './HexPlot -h', the help will show you the next steps. 
Examples of usage are printed via './HexPlot -examples'.


################################################################
TODO:
	- prepare inter-cell capacity for averages
	- verify depletion voltage fit on DSF data
	- enter correct test capacities
	- prepare 512 cells geo file
	- add wildcard for input files

DONE THIS COMMIT:

COMMIT 31.5.2017:
	- added correct depletion voltage fit for negative voltages
	- improved examples

COMMIT 25.4.2017:
	- appearance option to select nth appearance of a selector to be displayed. Useful for data files with repeated measurements of the same quantity.

COMMIT 4.4.2017
	- inter cell-capacity plots by adding INTER0:INTER1:... to the input format -> see examples
	- changed color of second axis title
	- save depletion voltage fits already at verbose = 1
	- fix -o, -od, -on, -of now clearly documented in help. "-o" overwrites everything.

PREVIOUS COMMITS:
	- range bug: hex cells filled until highest cell number in datafile minus 50 
	- fix calibration cell position for 256 inch wafer (there are 4!), check GDS file
	- comment examples
	- write out depletion voltages to data file
	- Udep across sensors
	- add distributions for all type cells
	- U_depleted koennte man dann auch fuer alle Pads ueber den Sensor in 2D plotten
	- CV saturation plot vs sqrt(U) to see depletion voltage. Koenntest du bei Folie 8 1/C^2 gegen U plotten? Das sollte zwei Geraden geben, an deren Schnittpunkt die Verarmungsspannung liegt. http://physics.unm.edu/Fields/group/FVTX/CVfit.jpg. Mean and stdev of full depletion voltage with proper evaluation (see e.g. Leo, W.R. - Techniques for Nuclear and Particle Physics Experiments, pg. 223 - 227 or talk to Nilou, she has done something similar already).
	- fix test capacity swap for 1001 and 1002 data files
	- highlight special cells in hexplot
	- analyse regions of different inter-pad distance
	- average per sensor
	- add uncertainties to average values, Hexplot with RMS
	- add zaxis zoom e.g. -z 30-50

REJECTED:
	- no depletion voltage for guard ring and test capacities
