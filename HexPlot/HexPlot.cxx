#ifdef _WIN32
	bool IsWindowsCompilation=true;
	#include <windows.h>
#else
    bool IsWindowsCompilation=false;
#endif



#include <iostream>
#include <sstream>
#include <fstream>
#include "TRandom.h"
#include "TTree.h"
#include "TLegend.h"
#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>


//my own class
#include "THex.h"
#include "RootUtils.h"
#include "FitFunctions.h"

using namespace std;

const double SKIPVALUE=-1000000;
const int MAXCELLS=1000;
const int MAXCELLTYPES=100;
const int MAXSENSORS=200;
string OUTPUTDIR="";
int N_VALID_FILES;
string VALID_SENSOR_NAMES[MAXSENSORS];
TGraph*gr_contents[MAXCELLS];
double SensorValuesForAverage[MAXSENSORS][MAXCELLS];

#include "HexValues.h"







int main(int argc, char **argv){

	//parameters without setter
	double canvwidth=750;
	string sensorname="";
	//setters that cannot overwrite standard parameters
	bool help=false;
	bool help_examples=false;
	bool help_defaults=false;
	bool noinfo=false;
	bool isIVinput=false;
	bool isCVinput=false;
	bool showmaxval=true;
	bool noaxis=false;
	bool greyoutpads=false;
	int verbose=0;
	int padnumberoption=1;
	int channelnumber=136;
	int numdigits=1;
	int appearance=-1;
	int execute_example=-1;
	string inputfile="test_values.txt";
	string mapfile="";
	string outputformat="pdf";
	string plotoption="HEX";
	string geofile="geo/hex_positions_HPK_128ch_6inch.txt";
	string specialcells="";
	string specialcellnames="";
	string mergefilelist="";
	string zoom="";
	string zoomunc="";
	string fitfunc="linint";

	//setters that can overwrite standard parameters
	bool isAverageValues=false;
	bool set_noAverageValues=false;
	double set_yscale=0;
	double yscale=1;
	double selector=DEFAULT_SELECTOR;
	double set_selector=DEFAULT_SELECTOR;
	string selectorname = "voltage:U:V";
	string set_selectorname = "";
	string inputformat = "SELECTOR:PADNUM:VAL";
	string set_inputformat="";
	string outputfile="";
	string set_outputfile="";
	string set_outputdir="";
	string outputname="";
	string set_outputname="";
	string valuename="current:I:nA";
	string set_valuename="";
	string datafilename="";
	string set_datafilename="";
	string measunc="0";
	string set_measunc="";
	string set_specialcelloption="";
	string specialcelloption="dottedredbordersmallnumbers";


	cout<<"Executing:"<<endl;
	for (int i=0; i<argc; i++) {
		cout<<argv[i]<<" ";
	}
	cout<<endl;

	for (int i=1; i<argc; i+=2) {
		std::string option(argv[i]);
		if (option.compare("-h") == 0) {help=true; --i; continue;}
		if (option.compare("-help") == 0) {help=true; --i; continue;}
		if (option.compare("-examples") == 0) {help_examples=true; --i; continue;}
		if (option.compare("-defaults") == 0) {help_defaults=true; --i; continue;}
		if (option.compare("-noav") == 0) {set_noAverageValues=true; --i; continue;}
		if (option.compare("-?") == 0) {help=true; --i; continue;}
		if (option.compare("-noinfo") == 0) {noinfo=true; --i; continue;}
		if (option.compare("-noaxis") == 0) {noaxis=true; --i; continue;}
		if (option.compare("-IV") == 0) {isIVinput=true; --i; continue;}
		if (option.compare("-CV") == 0) {isCVinput=true; --i; continue;}
		if (option.compare("-showmaxval") == 0) {showmaxval=true; --i; continue;}
		if (option.compare("-greyoutpads") == 0) {greyoutpads=true; --i; continue;}

		std::string argument(argv[i+1]);
		if (option.compare("-example") == 0) execute_example = int(std::atof(argument.c_str()));
		if (option.compare("-ap") == 0) appearance = int(std::atof(argument.c_str()));
		if (option.compare("-v") == 0) verbose = int(std::atof(argument.c_str()));
		if (option.compare("-ys") == 0) set_yscale = std::atof(argument.c_str());
		if (option.compare("-i") == 0) inputfile = argument;
		if (option.compare("-if") == 0) set_inputformat = argument;
		if (option.compare("-m") == 0) mapfile = argument;
		if (option.compare("-o") == 0) set_outputfile = argument;
		if (option.compare("-on") == 0) set_outputname = argument;
		if (option.compare("-of") == 0) outputformat = argument;
		if (option.compare("-od") == 0) set_outputdir = argument;
		if (option.compare("-df") == 0) set_datafilename = argument;
		if (option.compare("-g") == 0) geofile = argument;
		if (option.compare("-p") == 0) plotoption = argument;
		if (option.compare("-pn") == 0) padnumberoption = int(std::atof(argument.c_str()));
		if (option.compare("-volt") == 0) set_selector = std::atof(argument.c_str());
		if (option.compare("-select") == 0) set_selector = std::atof(argument.c_str());
		if (option.compare("-sn") == 0) set_selectorname = argument;
		if (option.compare("-sc") == 0) specialcells = argument;
		if (option.compare("-scn") == 0) specialcellnames = argument;
		if (option.compare("-sco") == 0) set_specialcelloption = argument;
		if (option.compare("-vn") == 0) set_valuename = argument;
		if (option.compare("-c") == 0) channelnumber = int(std::atof(argument.c_str()));
		if (option.compare("-mf") == 0) mergefilelist = argument;
		if (option.compare("-z") == 0) zoom = argument;
		if (option.compare("-zu") == 0) zoomunc = argument;
		if (option.compare("-u") == 0) set_measunc = argument;
		if (option.compare("-nd") == 0) numdigits = int(std::atof(argument.c_str()));
		if (option.compare("-fitfunc") == 0) fitfunc = argument;
	}
	if (argc==1) help=true;


	string commandstring = "./HexPlot";
	if (IsWindowsCompilation) commandstring = "HexPlot.exe";

	if(help) {
		SwitchColor("BOLDBLUE");
		printf("=======================================================\n");
		printf("===================== HexPlot help ====================\n");
		printf("=======================================================\n");
		SwitchColor("RESET");
		printf("\n");
		SwitchColor("BLUE");
		printf("General information:\n");
		SwitchColor("RESET");
		printf("The value %.0f in the datafile is treated as a non-existent value. Use it like nan.\n",SKIPVALUE);
		printf("\n");
		SwitchColor("BLUE");
		printf("General options:\n");
		SwitchColor("RESET");
		printf(" -?, h, help  Display this help\n");
		printf(" -examples    Display examples of usage\n");
		printf(" -example     Execute example of the given number (e.g. -example 2)\n");
		printf(" -defaults    Display default values\n");
		printf(" -v           Set verbosity (0-2)\n");
		printf("\n");
		SwitchColor("BLUE");
		printf("Steering options:\n");
		SwitchColor("RESET");
		printf(" -ap          For every cell, use value of nth appearance of selector in file, if existing. Otherwhise\n");
		printf("              take last one. 0-based.\n");
		printf(" -c           Set channel number for plot of single channel\n");
		printf(" -df          Set optional data filename for analysis data\n");
		printf(" -g           Set geometry file\n");
		printf(" -i           Set input file or multiple files, separated by ':' on Linux systems or ';' for Windows\n");
		printf(" -if          Set inputformat (e.g. SELECTOR:PADNUM:no:no:no:VAL:no)\n");
		printf("              Format is TTree:ReadFile() format without type specifications.\n");
		printf("              The column 'SELECTOR' allows to select special rows matching the selector, usually a voltage value.\n");
		printf("              The column 'PADNUM' gives the pad number.\n");
		printf("              The column 'VAL' will be taken as data.\n");
		printf("              The columns 'INTER*', with * an integer, allow to select rows indicating an inter-cell value,\n");
		printf("              usually an intercell capacitance.\n");
		printf(" -m           Set mapfile or list of mapfiles, separated by ':' on Linux systems or ';' for Windows.\n");
		printf("              If none is provided, identity of pad number and PADNUM is assumed.\n");
		printf(" -mf          MERGE: list files to merge, separated by ':' on Linux systems or ';' for Windows\n");
		printf(" -noav        Concatenate input files as single file\n");
		printf(" -o           Set entire output file path (overwrites -od, -of and -on)\n");
		printf(" -od          Set output directory\n");
		printf(" -of          Set output format to be given to Root, e.g. 'png', 'pdf' or 'eps'\n");
		printf(" -on          Set output file name without extension\n");
		printf(" -p           Set plot options\n");
		printf("              HEX              : draw hexagonal sensor pads\n");
		printf("              SEL              : draw value vs voltage (eg. IV curve for a single channel)\n");
		printf("              CHAN             : draw value curve for all channels (eg. I/C for a specific voltage for all channel numbers)\n");
		printf("              FULL             : draw all values for all channels\n");
		printf("              MERGE            : merge data files in output dir in one file\n");
		printf("              Optional plot option suffixes (* indicates any of the above options)\n");
		printf("              *DEP             : draw depletion voltage from values instead of value (makes only sense for CV values)\n");
		printf("              *VAL             : draw voltage corresponding to value\n");
		printf(" -select      Set selector: voltage to display only part of the data or capacity/current with option *VAL\n");
		printf(" -ys          Set additional scale of y-axis\n");
		printf("\n");
		SwitchColor("BLUE");
		printf("Analysis options:\n");
		SwitchColor("RESET");
		printf(" -fitfunc     Specify the fit function to be used e.g. in depletion voltage fits\n");
		printf("              linint           : two intersecting linear functions. Returns the intersection x position as value\n");
		printf("              linear           : a single linear function. Returns the slope as value.\n");
		printf("              The argument can also be given as name,min1,max1,min2,max2... which limits the fit to those ranges\n");
		printf("\n");
		SwitchColor("BLUE");
		printf("Text options:\n");
		SwitchColor("RESET");
		printf(" -CV          Shortcut to use standard formats and labels for CV figures. Can be overwritten as needed.\n");
		printf(" -IV          Shortcut to use standard formats and labels for IV figures. Can be overwritten as needed.\n");
		printf(" -nd          Set number of digits after point in pad value for HEX option\n");
		printf(" -noinfo      HEX: do not display additional information\n");
		printf(" -pn          HEX: set pad number option\n");
		printf("              0                : draw pad numbers\n");
		printf("              1                : draw pad content\n");
		printf("              2                : draw pad type\n");
		printf("              3                : no pad numbers\n");
		printf(" -scn         Give names to special cells or collections of cells (e.g. -scn 'top left center:top right center'))\n");
		printf(" -sn          Set selector name, symbol and unit (eg. 'voltage:U:V')\n");
		printf(" -vn          Set value name, symbol and unit (eg. 'current:I:nA')\n");
		printf(" -yl          Set label of y-axis\n");
		printf("\n");
		SwitchColor("BLUE");
		printf("Display options:\n");
		SwitchColor("RESET");
		printf(" -greyoutpads Overwrite color with grey. Useful for inter-cell display or simple layoutplots.\n");
		printf(" -noaxis      For hex plot: do not display z-axis\n");
		printf(" -sc          Define special cells to be highlighted or collections of special cells, separated with ':'.\n");
		printf("              The cells are highlighted and average numbers are determined (non-HEX plot options).\n");
		printf(" -sco         HEX: set option to plot special and non-special cells.\n");
		printf("              Can be concatenated as needed, e.g. -sco %s\n",specialcelloption.c_str());
		printf("              dotted           : non-special cells are drawn in dotted style\n");
		printf("              dense            : non-special cells are drawn in dense dotted style\n");
		printf("              loose            : non-special cells are drawn in loose dotted style\n");
		printf("              checked          : non-special cells are drawn in checked style\n");
		printf("              greyout          : grey out non-special cells and draw them without numbers\n");
		printf("              smallnumbers     : show numbers in non-special cells smaller\n");
		printf("              nonumbers        : no numbers in non-special cells\n");
		printf("              redborder        : red border for special cells\n");
		printf("              blackborder      : black border for non-special cells\n");
		printf(" -showmaxval  For FULL, superimpose max or min value per pad\n");
		printf(" -z           Zoom in on value axis (z in hex plot, y in regular plots) (e.g. -z 30.5:50.5)\n");
		printf(" -zu          Zoom in on uncertainty plots\n");
		// printf(" -u           Specify measurement uncertainty in root TF1 form (e.g. -u 0.01*x+0.05) (not implemented yet)\n");
		printf("\n");
	}


	std::vector<string> example_options;
	std::vector<string> example_description;

	example_description.push_back("Create hexagonal plot for 1000 V with an additional y value scaling:");
	example_options.push_back(" -i examples/test_data.txt -g geo/hex_positions_HPK_256ch_8inch.txt -o examples/example.pdf -IV -select 1000 -p HEX -pn 1 -ys 10e-2");
	example_description.push_back("Use first appearance of the 10 V selector:");
	example_options.push_back(" -i examples/test_data.txt -g geo/hex_positions_HPK_256ch_8inch.txt -o examples/example.pdf -IV -select 10 -p HEX -pn 1 -ys 10e-2 -ap 0");
	example_description.push_back("Draw pad numbers instead of values:");
	example_options.push_back(" -i examples/test_data.txt -g geo/hex_positions_HPK_256ch_8inch.txt -o examples/example.pdf -IV -select 1000 -p HEX -pn 0 -ys 10e-2");
	example_description.push_back("Draw inter-cell values without numbering in pads:");
	example_options.push_back(" -i examples/test_data3.txt -g geo/hex_positions_HPK_256ch_8inch.txt -o examples/example.pdf -CV -if SELECTOR:PADNUM:no:no:no:VAL:INTER0:INTER1:INTER2:INTER3:INTER4:INTER5 -select 1000 -p HEX -pn 3 -ys 10e-2");
	example_description.push_back("Show special cells:");
	example_options.push_back(" -i examples/test_data.txt -g geo/hex_positions_HPK_256ch_6inch_jumpers.txt -o examples/example.pdf -IV -select 1000 -p HEX -pn 1 -ys 10e-2 -sc 52,53,54,55,68,69,70:57,58,59,60,73,74,75:161,162,163,176,177,178,179:166,167,168,181,182,183,184");
	example_description.push_back("Show special cell regions with names (see additional figures):");
	example_options.push_back(" -i examples/test_data.txt -g geo/hex_positions_HPK_256ch_6inch_jumpers.txt -o examples/example.pdf -IV -select 1000 -p CHAN -pn 1 -ys 10e-2 -sc 52,53,54,55,68,69,70:57,58,59,60,73,74,75:161,162,163,176,177,178,179:166,167,168,181,182,183,184 -scn 'top left:top right:lower left:lower right'");
	example_description.push_back("With other geometry and zoom in on y-axis (underflow grey, overflow red):");
	example_options.push_back(" -i examples/test_data.txt -g geo/hex_positions_IFX_256ch_8inch.txt -o examples/example.pdf -IV -select 1000 -p HEX -pn 0 -ys 10e-2 -z 30:200");
	example_description.push_back("Suppress z-axis drawing, show pad type:");
	example_options.push_back(" -i examples/test_data.txt -g geo/hex_positions_IFX_256ch_8inch.txt -o examples/example.pdf -IV -select 1000 -p HEX -pn 2 -noaxis");
	example_description.push_back("Plot the average of different sensor files.:");
	example_options.push_back(" -i examples/test_data.txt:examples/test_data2.txt -g geo/hex_positions_IFX_256ch_8inch.txt -o examples/example.pdf -IV -select 1000 -p HEX -pn 1 -ys 10e-2");
	example_description.push_back("Concatenate data from different input files as if it were read from the same file:");
	example_options.push_back(" -i examples/test_data.txt:examples/test_data2.txt -g geo/hex_positions_IFX_256ch_8inch.txt -o examples/example.pdf -IV -select 20 -p HEX -pn 1 -noav");
	example_description.push_back("Plot available shapes:");
	example_options.push_back(" -i examples/test_data.txt -g geo/example_shapes.txt -o examples/example.pdf -select 1000 -p HEX -pn 2 -noaxis");
	example_description.push_back("Plot with switch matrix layout:");
	example_options.push_back(" -i examples/test_data.txt -g geo/switch_matrix.txt -o examples/example.pdf -select 1000 -p HEX -pn 0");
	example_description.push_back("Draw all channel values in xy plot:");
	example_options.push_back(" -i examples/test_data.txt -g geo/hex_positions_HPK_256ch_8inch.txt -o examples/example.pdf -IV -select 1000 -ys 10e-2 -p CHAN");
	example_description.push_back("Draw single channel values in xy plot:");
	example_options.push_back(" -i examples/test_data.txt -g geo/hex_positions_HPK_256ch_8inch.txt -o examples/example.pdf -IV -c 118 -ys 10e-2 -p SEL");
	example_description.push_back("Some real world examples:");
	example_options.push_back(" -i ../Data/HGC1017_IV.txt -if SELECTOR:PADNUM:no:no:no:VAL -g geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o examples/example.pdf -IV -select 1000 -p HEX -ys 1e9");
	example_options.push_back(" -i ../Data/HGC1017_IV.txt -if SELECTOR:PADNUM:no:no:no:VAL -g geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o examples/example.pdf -IV -select 1000 -p HEX -z 0:6 -ys 1e9");
	example_options.push_back(" -i ../Data/HGC1017_IV.txt -if SELECTOR:PADNUM:no:no:no:VAL -g geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o examples/example.pdf -IV -select 1000 -p CHAN -ys 1e9");
	example_options.push_back(" -i ../Data/HGC1017_IV.txt -if SELECTOR:PADNUM:no:no:no:VAL -g geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o examples/example.pdf -IV -c 118 -p SEL -ys 1e9");
	example_options.push_back(" -i ../Data/HGC1017_IV.txt -if SELECTOR:PADNUM:no:no:no:VAL -g geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o examples/example.pdf -IV -select 0.5 -p HEXVAL -ys 1e9");
	example_options.push_back(" -i ../Data/HGC1017_CV.txt -if SELECTOR:PADNUM:no:no:no:no:VAL -g geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o examples/example.pdf -CV -select 300 -p HEX -ys 1e12");
	example_options.push_back(" -i ../Data/HGC1017_CV.txt -if SELECTOR:PADNUM:no:no:no:no:VAL -g geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o examples/example.pdf -CV -p HEXDEP -z 140:200 -ys 1e12");
	example_options.push_back(" -i ../Data/HGC1017_CV.txt -if SELECTOR:PADNUM:no:no:no:no:VAL -g geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o examples/example.pdf -CV -p CHANDEP -z 140:200 -ys 1e12");
	example_options.push_back(" -i ../Data/HGC1017_CV.txt -if SELECTOR:PADNUM:no:no:no:no:VAL -g geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o examples/example.pdf -CV -select 70 -p HEXVAL -ys 1e12");

	if(help_examples) {
		printf("Example commands can be executed via %s -example [number]\n",commandstring.c_str());
		for (unsigned int i = 0; i < TMath::Max(example_options.size(),example_description.size()); ++i)
		{
			if (i<example_description.size() && example_description.at(i).size()){
				SwitchColor("BOLDBLUE");
				printf("# %s\n",example_description.at(i).c_str());
				SwitchColor("RESET");
			}
			SwitchColor("BOLDBLACK");
			printf("(%d)",i);
			SwitchColor("RESET");
			if (i<example_options.size() && example_options.at(i).size()) printf(" %s%s\n",commandstring.c_str(),example_options.at(i).c_str());
		}
	}

	if (execute_example != -1){
		string command = commandstring + example_options.at(execute_example);
		system(command.c_str());
		return 0;
	}


	vector<string> inputfiles = split(inputfile, IsWindowsCompilation?';':':');
	if (!set_noAverageValues && inputfiles.size()>1) isAverageValues=true;



	string ValueOption="";
	if (plotoption.find("DEP")<plotoption.size()) ValueOption="DEP";
	if (plotoption.find("VAL")<plotoption.size()) ValueOption="VAL";
	plotoption=rmString(plotoption,ValueOption);



	//if (OUTPUTDIR.empty) OUTPUTDIR = "."; //warning: default has changed from ../Results
	if (set_outputdir.size()) OUTPUTDIR=set_outputdir;


	if (isIVinput) {
		if (isAverageValues) valuename="current:<I>:nA";
		else valuename="current:I:nA";
		outputname=isAverageValues?"sensors_allvalues_IV":getFileNameNoExt(inputfile);
		showmaxval=true;
		//info from Keithley 2450 datasheet
		// for 100uA range resolution: 100 pA
		// measunc="0.1";
		measunc="0";
	}
	else if (isCVinput) {
		if (ValueOption=="DEP"){
			if (isAverageValues) valuename="depletion voltage:<U_{dep}>:V";
			else valuename="depletion voltage:U_{dep}:V";
		}else{
			if (isAverageValues) valuename="capacity:<C>:pF";
			else valuename="capacity:C:pF";
		}
		outputname=isAverageValues?"sensors_allvalues_CV":getFileNameNoExt(inputfile);
		showmaxval=false;
		//more info in E4980A Precision LCR Meter datasheet
		//http://cp.literature.agilent.com/litweb/pdf/5989-4435EN.pdf
		//just take resolution of 0.1% for now, which seems like a conservative value
		// measunc="0.001*x"; //depletion voltage fit does not converge
		measunc="0"; //zero error results in root std error for transformed curve and depletion voltage to converges
	}else{
		outputname=getFileNameNoExt(inputfile);
		outputfile = OUTPUTDIR + (OUTPUTDIR.empty() ? "" : "/") + outputname + "_" + plotoption + string(specialcells.size()>0 ? "_SCELL" : "") + "." + outputformat;
	}
	if (isCVinput || isIVinput) {
		if (ValueOption=="VAL"){
			if (isAverageValues) valuename="voltage:<U>:V";
			else valuename="Voltage:U:V";
			if (isIVinput) selectorname = "current:I:nA";
			else if (isCVinput) selectorname = "capacity:C:pF";
		}
		outputfile = OUTPUTDIR + (OUTPUTDIR.empty() ? "" : "/") + outputname + "_" + plotoption + string(ValueOption.size() ? "_" : "") + ValueOption + string(specialcells.size()>0 ? "_SCELL" : "") + "." + outputformat;
		sensorname=getFileNameNoExt(outputname,"_");
		if (!isAverageValues) datafilename = OUTPUTDIR + (OUTPUTDIR.empty() ? "" : "/") + sensorname + ".txt";
	}


	if (set_yscale!=0) yscale=set_yscale;
	if (set_measunc.size()) measunc=set_measunc;
	if (set_selector!=DEFAULT_SELECTOR) selector=set_selector;
	if (!set_inputformat.empty()) inputformat=set_inputformat;
	if (set_valuename.size()) valuename=set_valuename;
	if (set_selectorname.size()) selectorname=set_selectorname;
	if (set_outputname.size()) outputname=set_outputname;
	if (set_outputfile.size()) outputfile=set_outputfile;
	if (set_datafilename.size()) datafilename=set_datafilename;
	if (set_specialcelloption.size()) specialcelloption=set_specialcelloption;


	bool isinterpadplot = (inputformat.find("INTER")<inputformat.size());

	if (datafilename == "NONE") datafilename = "";

	//now fill the vectors holding several arguments:
	std::vector<string> mergefiles = split(mergefilelist, IsWindowsCompilation?';':':');
	std::vector<string> selectorlabels = split(selectorname, ':');
	std::vector<string> valuelabels = split(valuename, ':');
	std::vector<string> special_cell_names = split(specialcellnames, ':');
	std::vector<string> special_cell_collection = split(specialcells, ':');
	std::vector<std::vector<string>> special_cells;
	std::vector<std::vector<int>> special_cell_numbers;
	for (unsigned int i = 0; i < special_cell_collection.size(); ++i)
	{
		std::vector<string> tmp_special_cell=split(special_cell_collection.at(i),',');
		special_cells.push_back(tmp_special_cell);
	}
	for (unsigned int i = 0; i < special_cells.size(); ++i)
	{
		std::vector<int> tmp_special_cell_numbers;
		for (unsigned int j = 0; j < special_cells.at(i).size(); ++j)
		{
			int thisnumber=int(std::atof(special_cells.at(i).at(j).c_str()));
			tmp_special_cell_numbers.push_back(thisnumber);
		}
		special_cell_numbers.push_back(tmp_special_cell_numbers);
	}
	std::vector<string> zoom_tmp = split(zoom, ':');
	double zoom_min=0;
	double zoom_max=0;
	if (zoom.size()){
		if (zoom_tmp.size() != 2) printf("Error: zoom parameter should be of type double-double, e.g. 30.5:50.5! You provided %s with %d parameters!\n", zoom.c_str(), int(zoom_tmp.size()));
		zoom_min=std::atof(zoom_tmp.at(0).c_str());
		zoom_max=std::atof(zoom_tmp.at(1).c_str());
	}
	std::vector<string> zoomunc_tmp = split(zoomunc, ':');
	double zoomunc_min=0;
	double zoomunc_max=0;
	if (zoomunc.size()){
		if (zoomunc_tmp.size() != 2) printf("Error: zoomunc parameter should be of type double-double, e.g. 30.5:50.5! You provided %s with %d parameters!\n", zoomunc.c_str(), int(zoomunc_tmp.size()));
		zoomunc_min=std::atof(zoomunc_tmp.at(0).c_str());
		zoomunc_max=std::atof(zoomunc_tmp.at(1).c_str());
	}





	if (help_defaults){
		printf("\n");
		printf("Default values:\n");
	}else if (verbose){
		printf("+++++++ HexPlot +++++++\n");
		printf("Arguments set to:\n");
	}
	if (verbose || help_defaults){
		printf("-v                 verbosity                    %d\n",verbose);
		printf("-ys                yscale                       %.2e\n",yscale);
		printf("-i                 inputfile                    %s\n",inputfile.c_str());
		printf("-o                 outputfile                   %s\n",outputfile.c_str());
		printf("-on                outputname                   %s\n",outputname.c_str());
		printf("-of                outputformat                 %s\n",outputformat.c_str());
		printf("-od                outputdir                    %s\n",OUTPUTDIR.c_str());
		printf("-df                datafilename                 %s\n",datafilename.c_str());
		printf("-g                 geometry file                %s\n",geofile.c_str());
		printf("-if                inputformat                  %s\n",inputformat.c_str());
		printf("-IV                use IV inputformat           %d\n",isIVinput);
		printf("-CV                use CV inputformat           %d\n",isCVinput);
		printf("-m                 mapfile                      %s\n",mapfile.c_str());
		printf("-p                 plot option                  %s\n",plotoption.c_str());
		printf("-mf                mergefilelist                %s\n",mergefilelist.c_str());
		printf("-select            selector value               %.2e\n",selector);
		printf("-sn                selector name                %s\n",selectorname.c_str());
		printf("-sc                special cells                %s\n",specialcells.c_str());
		printf("-scn               special cell names           %s\n",specialcellnames.c_str());
		printf("-sco               special cell option          %s\n",specialcelloption.c_str());
		printf("-c                 channel number               %d\n",channelnumber);
		printf("-pn                pad number option            %d\n",padnumberoption);
		printf("-nd                number of digits             %d\n",numdigits);
		printf("-noinfo            noinfo                       %d\n",noinfo);
		printf("-showmaxval        showmaxval                   %d\n",showmaxval);
		printf("-z                 zoom                         %s\n",zoom.c_str());
		printf("-zu                zoom uncertainty             %s\n",zoomunc.c_str());
		printf("-u                 measurement uncertainty      %s\n",measunc.c_str());
	}
	if (verbose) printf("++++ Start HexPlot ++++\n");
	if (help || help_defaults || help_examples) {
		return 0;
		//system("pause");
	}



	//create analysis file

	ofstream fileout;
	if (!datafilename.empty() && !ifstream(datafilename)){
		fileout.open(datafilename.c_str());
		fileout << "#### Analysis file "<< sensorname<<" ####"<<endl;
		fileout << endl;
		printf("Created new file for sensor analysis: %s\n",datafilename.c_str());
		fileout.close();
	}

	gStyle->SetOptStat(0);

	THex*hc=new THex();
	hc->SetDebug(verbose>1);
	hc->SetGeoFile(geofile);














	if (plotoption == "HEX"){
		//draw main hexplot
		TH1F **h_full = (TH1F**) LoadDataHisto(isAverageValues,ValueOption,inputfile,inputformat,mapfile,yscale,verbose,selector,-1,measunc,appearance,fitfunc);
		TH1F *h_fill = h_full[0];
		if (!h_fill) return 1;
		hc->Fill(h_fill);
		if (isinterpadplot){
			hc->FillInterCell(h_full[1]);
			hc->FillInterCell(h_full[2]);
			hc->FillInterCell(h_full[3]);
			hc->FillInterCell(h_full[4]);
			hc->FillInterCell(h_full[5]);
			hc->FillInterCell(h_full[6]);
			hc->FillInterCell(h_full[7]);
			hc->FillInterCell(h_full[8]);
			hc->FillInterCell(h_full[9]);
			hc->FillInterCell(h_full[10]);
		}
		if (greyoutpads) hc->GreyOutPads();
		hc->SetOutputfile(outputfile);
		hc->SetZtitle(Form("%s [%s]",valuelabels.at(1).c_str(),valuelabels.at(2).c_str()));
		hc->SetZeroSuppress(true);
		if (zoom.size()) hc->SetZrange(zoom_min,zoom_max);
		hc->SetNoAxis(noaxis);
		hc->SetPadValuePrecision(numdigits);
		if (noinfo) hc->SetInfo(false);
		hc->SetPadNumberOption(padnumberoption);
		// if (isAverageValues) hc->SetTopLeftInfo("Average values");
		if (isAverageValues) hc->SetTopLeftInfo(Form("Average values of %d sensor%s",N_VALID_FILES,N_VALID_FILES>1?"s":""));
		//else hc->SetTopLeftInfo(getFileNameNoExt(inputfile).c_str());
		if (ValueOption != "DEP" && !noaxis) hc->SetLowRightInfo(Form("Values for %s = %.1f %s",selectorlabels.at(1).c_str(),selector,selectorlabels.at(2).c_str()));
		if (padnumberoption==2) hc->SetLowLeftInfo("Numbering accoring to pad type");
		hc->HighlightPadOption(specialcelloption);
		for (unsigned int i = 0; i < special_cell_numbers.size(); ++i)
		{
			for (unsigned int j = 0; j < special_cell_numbers.at(i).size(); ++j)
			{
				hc->HighlightPad(special_cell_numbers.at(i).at(j));
			}
		}
		hc->Draw();

		//draw uncertainty hexplot
		bool drawrelativeerror=true;
		if (isAverageValues){
			hc->Reset();
			TH1F*h_errors=(TH1F*)h_fill->Clone();
			h_errors->Clone();
			for (int i = 0; i < h_errors->GetNbinsX(); ++i)
			{
				if (drawrelativeerror) h_errors->SetBinContent(i+1,h_fill->GetBinError(i+1)/h_fill->GetBinContent(i+1)*100);
				else h_errors->SetBinContent(i+1,h_fill->GetBinError(i+1));
			}
			string outputfile_error=stripExtension(outputfile)+"_unc."+outputformat;
			hc->Fill(h_errors);
			hc->SetOutputfile(outputfile_error);
			if (drawrelativeerror){
				hc->SetZtitle(Form("Relative spread of %s [%%]",valuelabels.at(1).c_str()));
				if (zoomunc.size()) hc->SetZrange(zoomunc_min,zoomunc_max);
				else hc->SetZrange(0,100);
			}else{
				hc->SetZtitle(Form("Spread of %s [%s]",valuelabels.at(1).c_str(),valuelabels.at(2).c_str()));
				if (zoomunc.size()) hc->SetZrange(zoomunc_min,zoomunc_max);
				else if (zoom.size()) hc->SetZrange(zoom_min,zoom_max);
			}
			if (isAverageValues) hc->SetTopLeftInfo(Form("Uncertainties of average values of %d sensor%s",N_VALID_FILES,N_VALID_FILES>1?"s":""));
			else hc->SetTopLeftInfo(getFileNameNoExt(inputfile).c_str());
			if (ValueOption != "DEP") hc->SetLowRightInfo(Form("Values for %s = %.1f %s",selectorlabels.at(1).c_str(),selector,selectorlabels.at(2).c_str()));
			hc->Draw();
		}
	}














	else if (plotoption == "CHAN"){
		const bool TakeSingleCellsForAverage=true;

		bool uselogy=true;
		if (zoom.size()) uselogy=false;

		//first draw raw black data points
		TH1F **h_full = (TH1F**) LoadDataHisto(isAverageValues,ValueOption,inputfile,inputformat,mapfile,yscale,verbose,selector,-1,measunc,appearance,fitfunc);
		TH1F *h_fill = h_full[0];
		if (!h_fill) return 1;
		bool SuccessfullyLoaded=true;
		if (h_fill->Integral()==0) SuccessfullyLoaded=false;
		h_fill->SetTitle("");
		h_fill->SetXTitle("Pad number");
		h_fill->SetYTitle(Form("%s [%s]",valuelabels.at(1).c_str(),valuelabels.at(2).c_str()));
		h_fill->GetYaxis()->SetTitleOffset(1.3);
		h_fill->SetLineColor(kBlack);
		h_fill->SetMarkerStyle(20);
		h_fill->SetMarkerSize(1);
		if (zoom.size()) h_fill->GetYaxis()->SetRangeUser(zoom_min,zoom_max);
		else if (!uselogy) {
			h_fill->SetMaximum(h_fill->GetMaximum()+(h_fill->GetMaximum()-h_fill->GetMinimum())*0.2);
		}else{
			h_fill->SetMaximum(h_fill->GetMaximum()+(h_fill->GetMaximum()-h_fill->GetMinimum())*4);
		}
		// h_fill->Scale(yscale);
		TCanvas *c_chan = new TCanvas("c_chan","c_chan",800,500,int(canvwidth),int(canvwidth/1.5));
		c_chan->cd();
		gPad->SetRightMargin(0.01);
		if (uselogy) gPad->SetLogy();
		TH1F* h_fill_draw=(TH1F*)h_fill->Clone("h_fill_draw");
		RemoveNaNBins(h_fill_draw);
		h_fill_draw->Draw("*");

		TGraph *gr_fill=new TGraph(h_fill);
		gr_fill->Draw("plsame");
		gPad->SetGridx();
		gPad->SetGridy();

		//now draw different colors
		TH1F* h_markers[MAXCELLTYPES];
		TH1F* h_cat_sensors[MAXCELLTYPES][MAXSENSORS];
		TH1F* h_categories[MAXCELLTYPES+1];
		TH1F* h_categories_meanerror[MAXCELLTYPES+1];
		h_categories[MAXCELLTYPES]=new TH1F(Form("h_categories[%d]",MAXCELLTYPES),Form("h_categories[%d]",MAXCELLTYPES),MAXCELLTYPES,0,MAXCELLTYPES);
		h_categories[MAXCELLTYPES]->SetName("all");
		TGraphErrors*gr_categories_sensors[MAXCELLTYPES];
		std::vector<std::vector<int>> CellWithType;
		for (int i = 0; i < MAXCELLS; ++i)
		{
			std::vector<int> v_tmp;
			CellWithType.push_back(v_tmp);
		}
		double totalmax(-1e20),totalmin(1e20);
		std::vector<int> types;
		std::vector<int> special_types;
		std::vector<int> processed_special_cells;
		for (int i_bin = 0; i_bin < h_fill->GetNbinsX(); ++i_bin)
		{
			int type=hc->GetPadType(i_bin+1);
			if (type == -1) continue;
			string typestring=hc->GetPadTypeString(i_bin+1);

			bool isSpecialCell=false;
			int special_type(-1);
			string special_typestring("");
			for (unsigned int i = 0; i < special_cell_numbers.size(); ++i)
			{
				for (unsigned int j = 0; j < special_cell_numbers.at(i).size(); ++j)
				{
					if (i_bin+1 == special_cell_numbers.at(i).at(j)){
						isSpecialCell=true;
						if (special_cell_names.size() != special_cell_collection.size()) {
							special_typestring=special_cell_collection.at(i);
						}
						else {
							// special_typestring=special_cell_names.at(i)+" ("+special_cell_collection.at(i)+")";
							special_typestring=special_cell_names.at(i);
						}
						special_type=10000+i;
						break;
					}
				}
				if (isSpecialCell==true) break;
			}
			bool reduce_i_bin=false;
			if (isSpecialCell) {
				if (!IsInVector(i_bin+1,processed_special_cells)){
					// printf("Bin %d is special type value %d, description %s\n",i_bin+1,special_type,special_typestring.c_str() );
					if(!IsInVector(special_type,special_types)) special_types.push_back(special_type);
					processed_special_cells.push_back(i_bin+1);
					type=special_type;
					typestring=special_typestring;
					reduce_i_bin=true;
				}
			}
			//from here on type includes "normal" and "special" types!
			CellWithType.at(i_bin).push_back(type);



			// cout<<"Bin "<<i_bin+1<<" has type "<<type<<" "<<typestring<<" with content "<<h_fill->GetBinContent(i_bin+1)<<endl;
			int pos = find(types.begin(), types.end(), type) - types.begin();
			if(!IsInVector(type,types)) {
				types.push_back(type);
				h_categories[types.size()-1]=new TH1F(Form("h_categories[%d]",int(types.size())-1),Form("h_categories[%d]",int(types.size())-1),MAXCELLTYPES,0,MAXCELLTYPES);
				for (int i_files = 0; i_files < N_VALID_FILES; ++i_files)
				{
					h_cat_sensors[types.size()-1][i_files]=new TH1F(Form("h_cat_sensors[%d][%d]",int(types.size())-1,i_files),Form("h_cat_sensors[%d][%d]",int(types.size())-1,i_files),MAXCELLTYPES,0,200*MAXCELLTYPES);
				}
				gr_categories_sensors[types.size()-1]=new TGraphErrors(0);
				h_markers[types.size()-1]=(TH1F*)h_fill->Clone(Form("h_markers[%d]",int(types.size())-1));
				h_markers[types.size()-1]->Reset();
				h_markers[types.size()-1]->SetMarkerSize(0.8);
				int color=GetColLineFillStyleNoBlack(pos)[0];
				if (type == -1) color=20;
				else if (type == 0) color=1;
				else if (typestring.find("test capacity")<typestring.size()) color=18;
				h_markers[types.size()-1]->SetMarkerColor(color);
				h_markers[types.size()-1]->SetName(typestring.c_str());
				h_categories[types.size()-1]->SetLineColor(color);
				h_categories[types.size()-1]->SetMarkerColor(color);
				h_categories[types.size()-1]->SetName(typestring.c_str());
				gr_categories_sensors[types.size()-1]->SetLineColor(color);
				gr_categories_sensors[types.size()-1]->SetMarkerColor(color);
				gr_categories_sensors[types.size()-1]->SetName(typestring.c_str());
				gr_categories_sensors[types.size()-1]->SetMarkerSize(0.7);
				gr_categories_sensors[types.size()-1]->SetMarkerStyle(20);
			}
			double cont=h_fill->GetBinContent(i_bin+1);
			// if (zoom.size() && cont>h_fill->GetMaximum()) cont=h_fill->GetMaximum(); //if outside, show at the edge
			// if (zoom.size() && cont<h_fill->GetMinimum()) cont=h_fill->GetMinimum(); //if outside, show at the edge
			h_markers[pos]->SetBinContent(i_bin+1,cont);
			h_markers[pos]->SetBinError(i_bin+1,0.01*cont);
			for (int i_files = 0; i_files < N_VALID_FILES; ++i_files)
			{
				double filebincont=gr_contents[i_bin]->GetY()[i_files];
				totalmax=TMath::Max(filebincont,totalmax);
				totalmin=TMath::Min(filebincont,totalmin);
				// printf("Bin %d file %d pos %d type %d content %f (in h_fill %f)\n",i_bin,i_files,pos,types.at(pos), filebincont,cont);
				h_cat_sensors[pos][i_files]->Fill(filebincont);
			}
			if (reduce_i_bin) --i_bin;
		}
		double totalrange=totalmax-totalmin;

		for (unsigned int i = 0; i < CellWithType.size(); ++i)
		{
			for (unsigned int j = 0; j < CellWithType.at(i).size(); ++j)
			{
				if (verbose > 1) printf("CellWithType.at(%d).at(%d) (pad %d): %d\n",i,j,i+1,CellWithType.at(i).at(j));
			}
		}


		//legend
		TLegend *leg=new TLegend(0.15,0.8,0.95,0.88);
		// leg->SetFillColor(kNone);
		// leg->SetLineColor(kNone);
		leg->SetNColumns(4);
		std::vector<int> legendorder;
		int standardpos = find(types.begin(), types.end(), 0) - types.begin();
		if(standardpos<int(types.size())) {
			leg->AddEntry(h_markers[standardpos],h_markers[standardpos]->GetName(),"p");
			legendorder.push_back(standardpos);
		}
		bool testcapacityalreadyinlegend=false;
		for (unsigned int i = 0; i < types.size(); ++i)
		{
			// printf("Looping on type %d / %d : %d\n",i,int(types.size()),types.at(i) );
			if (IsInVector(types.at(i),special_types)) continue;
			if (types.at(i)==0) continue;
			else if (types.at(i)==-1) continue;
			else if (testcapacityalreadyinlegend) {
				legendorder.push_back(i);
				continue;
			}
			// printf("Fill legend for type[%d]: %d %s\n",i,types.at(i),h_markers[i]->GetName() );
			string entryname=h_markers[i]->GetName();
			if (entryname.find("test capacity")<entryname.size()) {
				entryname="test capacity";
				testcapacityalreadyinlegend=true;
			}
			legendorder.push_back(i);
			leg->AddEntry(h_markers[i],entryname.c_str(),"p");
		}
		//now special types
		TLegend *full_leg=(TLegend*)leg->Clone();
		TLegend *special_leg=(TLegend*)leg->Clone();
		special_leg->Clear();
		if (!TakeSingleCellsForAverage) special_leg->SetHeader("Warning: different bin sizes!");
		for (unsigned int i = 0; i < special_types.size(); ++i)
		{
			int thispos = find(types.begin(), types.end(), 10000+i) - types.begin();
			// printf("Filling legend for special_types[%d]: %d %s\n",i,types.at(thispos),h_markers[thispos]->GetName() );
			full_leg->AddEntry(h_markers[thispos],h_markers[thispos]->GetName(),"p");
			special_leg->AddEntry(h_markers[thispos],h_markers[thispos]->GetName(),"p");
			legendorder.push_back(thispos);
		}
		int unspecpos = find(types.begin(), types.end(), -1) - types.begin();
		if(unspecpos<int(types.size())) {
			leg->AddEntry(h_markers[unspecpos],h_markers[unspecpos]->GetName(),"p");
			full_leg->AddEntry(h_markers[unspecpos],h_markers[unspecpos]->GetName(),"p");
			special_leg->AddEntry(h_markers[unspecpos],h_markers[unspecpos]->GetName(),"p");
		}
		std::vector<int> invlegendorder;
		for (unsigned int i = 0; i < legendorder.size(); ++i)
		{
			int legbin=find(legendorder.begin(), legendorder.end(), i) - legendorder.begin();
			invlegendorder.push_back(legbin);
			// printf("Legendorder type entry %d (%s) displayed at legend position %d\n",i,h_markers[i]->GetName(),invlegendorder.at(i));
		}


		//draw colored dots
		for (unsigned int i = 0; i < types.size(); ++i)
		{
			if (IsInVector(types.at(i),special_types)) continue;
			h_markers[i]->Draw("psame");
		}
		h_fill_draw->Draw("axissame");
		leg->Draw();
		//info
		TPaveText *padnum=0;
		padnum = new TPaveText(0.1,0.92,0.1,0.92,"NDC");
		padnum->SetFillColor(kNone);
		padnum->SetLineColor(kNone);
		padnum->SetTextSize(0.03);
		padnum->SetTextAlign(12);
		padnum->SetTextColor(kBlack);
		string all_sensors_string=isAverageValues?intToString(N_VALID_FILES)+" sensor"+(N_VALID_FILES>1?"s":"")+" at ":"";
		padnum->AddText(Form("Values for %s%s = %.1f %s",all_sensors_string.c_str(),selectorlabels.at(1).c_str(),selector,selectorlabels.at(2).c_str()));
		padnum->Draw("same");
		//print
		c_chan->Print(outputfile.c_str());



		//write out average values
		TH1F* h_markers_proj[MAXCELLTYPES];
		TH1F* h_markers_proj_zoom[MAXCELLTYPES];
		for (unsigned int itype = 0; itype < types.size(); ++itype)
		{
			if (!TakeSingleCellsForAverage || !isAverageValues){
				h_markers_proj[itype]=(TH1F*)ProjectionY(h_markers[itype],true)->Clone(Form("h_markers_proj[%d]",itype));
				h_markers_proj_zoom[itype]=(TH1F*)ProjectionY(h_markers[itype],true,h_markers[standardpos]->GetBinContent(h_markers[standardpos]->GetMaximumBin()))->Clone(Form("h_markers_proj[%d]",itype));
				h_markers_proj[itype]->SetTitle(Form("%s;%s;Number of pads",h_markers[itype]->GetName(),h_fill->GetYaxis()->GetTitle()));
				h_markers_proj_zoom[itype]->SetTitle(Form("%s;%s;Number of pads",h_markers[itype]->GetName(),h_fill->GetYaxis()->GetTitle()));
			}else{
				//determine appropriate min and max for plot
				double mincont=1000000000;
				double maxcont=0;
				for (int i = 0; i < N_VALID_FILES; ++i)
				{
					for (int j = 0; j < MAXCELLS; ++j)
					{
						if (types.at(itype) != 1 && hc->GetPadType(j+1) == 1) {
							// printf("Skipping pad %d with type %d\n",j+1, hc->GetPadType(j+1));
							continue;
						}
						double cont=SensorValuesForAverage[i][j];
						if (cont>maxcont) {
							// printf("File %d cell %d: new max %.2f\n",i,j,cont );
							maxcont=cont;
						}
						if (cont<mincont) {
							// printf("File %d cell %d: new max %.2f\n",i,j,cont );
							mincont=cont;
						}
					}
				}
				double range=maxcont-mincont;
				mincont-=range*0.05;
				maxcont+=range*0.05;
				double zoom_range=zoom_max-zoom_min;
				int nbin=zoom_range>0?int(20.*range/zoom_range):40;
				// printf("Final range %.2f - %.2f with %d bins\n",mincont,maxcont,nbin);
				h_markers_proj[itype]=new TH1F(Form("h_markers_proj[%d]",itype),Form("h_markers_proj[%d]",itype),nbin,mincont,maxcont);
				h_markers_proj_zoom[itype]=new TH1F(Form("h_markers_proj_zoom[%d]",itype),Form("h_markers_proj_zoom[%d]",itype),nbin,mincont,maxcont);
				//fill the plots with single cells of each type
				for (unsigned int i = 0; i < CellWithType.size(); ++i)
				{
					for (unsigned int j = 0; j < CellWithType.at(i).size(); ++j)
					{
						int thistype=CellWithType.at(i).at(j);
						if (thistype == types.at(itype)){
							for (int k = 0; k < N_VALID_FILES; ++k)
							{
								h_markers_proj[itype]->Fill(SensorValuesForAverage[k][i]);
								h_markers_proj_zoom[itype]->Fill(SensorValuesForAverage[k][i]);
							}

						}
						// if (verbose > 1) printf("CellWithType.at(%d).at(%d): %d\n",i,j,CellWithType.at(i).at(j));
					}
				}
				h_markers_proj[itype]->SetTitle(Form("%s;%s;Number of cells",h_markers[itype]->GetName(),h_fill->GetYaxis()->GetTitle()));
				h_markers_proj_zoom[itype]->SetTitle(Form("%s;%s;Number of cells",h_markers[itype]->GetName(),h_fill->GetYaxis()->GetTitle()));
			}

			// double average=SuccessfullyLoaded?GetAverageBinContent(h_markers[itype])[0]:0;
			// double meanerror=SuccessfullyLoaded?GetAverageBinContent(h_markers[itype])[1]:-1;
			// double stddeviation=SuccessfullyLoaded?GetAverageBinContent(h_markers[itype])[2]:-1;
			double average=SuccessfullyLoaded?h_markers_proj[itype]->GetMean():0;
			double meanerror=SuccessfullyLoaded?h_markers_proj[itype]->GetMeanError():-1;
			double stddeviation=SuccessfullyLoaded?h_markers_proj[itype]->GetStdDev():-1;
			printf("Cells %s have average value of %.2f +- %.2f (meanerr %.2f)\n",h_markers[itype]->GetName(),average,stddeviation,meanerror);
			// int legbin=find(legendorder.begin(), legendorder.end(), itype) - legendorder.begin();
			int legbin=invlegendorder.at(itype);
			h_categories[itype]->SetBinContent(legbin+1,average);
			h_categories[itype]->SetBinError(legbin+1,TMath::Max(stddeviation,h_fill->GetMaximum()*1e-10)); //needed to get rid of drawn empty bins
			h_categories[itype]->SetMarkerStyle(20);
			h_categories[itype]->SetMarkerSize(0.8);
			h_categories_meanerror[itype]=(TH1F*)h_categories[itype]->Clone();
			h_categories_meanerror[itype]->SetBinError(legbin+1,TMath::Max(meanerror,h_fill->GetMaximum()*1e-10)); //needed to get rid of drawn empty bins
			h_categories[MAXCELLTYPES]->SetBinContent(legbin+1,average);
			h_categories[MAXCELLTYPES]->SetBinError(legbin+1,TMath::Max(stddeviation,h_fill->GetMaximum()*1e-10)); //needed to get rid of drawn empty bins
			h_categories[MAXCELLTYPES]->GetXaxis()->SetBinLabel(legbin+1,"");//Form("%d -> %d",itype,legbin));

			for (int i_files = 0; i_files < N_VALID_FILES; ++i_files)
			{
				// printf("File %d %s: %f\n", i_files,gr_categories_sensors[itype]->GetName(),h_cat_sensors[itype][i_files]->GetMean());
				// double xpos=i_files+(itype+1.)*(1./(types.size()+1));
				double xpos=i_files+0.5;
				// printf("gr_categories_sensors[%d]->SetPoint(%d,%.2f,%.2f) \n",itype,i_files,xpos,h_cat_sensors[itype][i_files]->GetMean());
				gr_categories_sensors[itype]->SetPoint(i_files,xpos,h_cat_sensors[itype][i_files]->GetMean());
				gr_categories_sensors[itype]->SetPointError(i_files,h_cat_sensors[itype][i_files]->GetRMS());
			}


			string linename=Form("%s %s [%s]",valuelabels.at(0).c_str(),h_markers[itype]->GetName(),valuelabels.at(2).c_str());
			string lineval=Form("%.2f\t%.2f",average,stddeviation );
			WriteValue(datafilename,linename,lineval);
		}


		TCanvas *c_cat_distr = new TCanvas("c_cat_distr","c_cat_distr",800,500,int(canvwidth),int(canvwidth/1.5));
		c_cat_distr->Divide(5,4);
		for (unsigned int i = 0; i < types.size(); ++i)
		{
			c_cat_distr->cd(i+1);
			gPad->SetRightMargin(0.01);
			int itype=legendorder.at(i);
			h_markers_proj[itype]->SetLineColor(h_markers[itype]->GetMarkerColor());
			h_markers_proj[itype]->SetLineWidth(2);
			// h_markers_proj[itype]->Rebin(5);
			if (zoom.size()) h_markers_proj[itype]->GetXaxis()->SetRangeUser(zoom_min,zoom_max);
			h_markers_proj[itype]->Draw("");
		}
		string outfilename_cat_distr=stripExtension(outputfile)+"_cat_distr."+outputformat;
		c_cat_distr->Print(outfilename_cat_distr.c_str());


		if (specialcells.size()){
			TCanvas *c_cat_distr_spec = new TCanvas("c_cat_distr_spec","c_cat_distr_spec",800,500,int(canvwidth),int(canvwidth/1.5));
			c_cat_distr_spec->cd();
			gPad->SetRightMargin(0.01);
			//first determine xrange and ymax
			double ymax=0;
			for (unsigned int i = 0; i < types.size(); ++i)
			{
				int itype=legendorder.at(i);
				if (!IsInVector(types.at(itype),special_types)) continue;
				// h_markers_proj_zoom[itype]->Rebin(5);
				double thisymax=h_markers_proj_zoom[itype]->GetBinContent(h_markers_proj_zoom[itype]->GetMaximumBin());
				if (ymax<thisymax) ymax=thisymax;
			}
			bool isdrawn=false;
			for (unsigned int i = 0; i < types.size(); ++i)
			{
				int itype=legendorder.at(i);
				if (!IsInVector(types.at(itype),special_types)) continue;
				h_markers_proj_zoom[itype]->SetTitle("");
				h_markers_proj_zoom[itype]->SetLineColor(h_markers[itype]->GetMarkerColor());
				h_markers_proj_zoom[itype]->SetLineWidth(2);
				h_markers_proj_zoom[itype]->SetMaximum(ymax*1.2);
				if (zoom.size()) h_markers_proj_zoom[itype]->GetXaxis()->SetRangeUser(zoom_min,zoom_max);
				h_markers_proj_zoom[itype]->Draw(isdrawn?"same":"");
				isdrawn=true;
			}
			special_leg->Draw();
			string outfilename_cat_distr_spec=stripExtension(outputfile)+"_cat_distr_spec."+outputformat;
			c_cat_distr_spec->Print(outfilename_cat_distr_spec.c_str());
		}


		TCanvas *c_cat = new TCanvas("c_cat","c_cat",800,500,int(canvwidth),int(canvwidth/1.5));
		c_cat->cd();
		gPad->SetRightMargin(0.01);
		gPad->SetGridx();
		gPad->SetGridy();
		if (uselogy) gPad->SetLogy();
		h_categories[MAXCELLTYPES]->GetXaxis()->SetRangeUser(0,types.size());
		h_categories[MAXCELLTYPES]->SetTitle("");
		h_categories[MAXCELLTYPES]->SetXTitle("Cell categories");
		h_categories[MAXCELLTYPES]->GetXaxis()->SetTitleOffset(0.7);
		h_categories[MAXCELLTYPES]->SetYTitle(h_fill->GetYaxis()->GetTitle());
		h_categories[MAXCELLTYPES]->GetYaxis()->SetTitleOffset(1.3);
		h_categories[MAXCELLTYPES]->SetLineColor(kBlack);
		h_categories[MAXCELLTYPES]->SetMarkerStyle(20);
		h_categories[MAXCELLTYPES]->SetMarkerSize(1);
		if (zoom.size()) h_categories[MAXCELLTYPES]->GetYaxis()->SetRangeUser(zoom_min,zoom_max);
		else if (!uselogy) {
			// h_categories[MAXCELLTYPES]->SetMaximum(h_categories[MAXCELLTYPES]->GetMaximum()+(h_categories[MAXCELLTYPES]->GetMaximum()-h_categories[MAXCELLTYPES]->GetMinimum())*0.2);
			h_categories[MAXCELLTYPES]->SetMinimum(h_fill->GetMinimum());
			h_categories[MAXCELLTYPES]->SetMaximum(h_fill->GetMaximum());
		}else{
			h_categories[MAXCELLTYPES]->SetMaximum(h_categories[MAXCELLTYPES]->GetMaximum()+(h_categories[MAXCELLTYPES]->GetMaximum()-h_categories[MAXCELLTYPES]->GetMinimum())*4);
		}
		h_categories[MAXCELLTYPES]->Draw("*ex0");

		TGraph *gr_cat=new TGraph(h_categories[MAXCELLTYPES]);
		gr_cat->Draw("psame");

		for (unsigned int i = 0; i < types.size(); ++i)
		{
			h_categories[i]->Draw("pex0same");
			h_categories_meanerror[i]->Draw("e1x0same");
		}
		padnum->Draw("same");
		full_leg->Draw();
		string outfilename_cat=stripExtension(outputfile)+"_cat."+outputformat;
		c_cat->Print(outfilename_cat.c_str());




		TCanvas *c_relcat = new TCanvas("c_relcat","c_relcat",800,500,int(canvwidth),int(canvwidth/1.5));
		c_relcat->cd();
		gPad->SetRightMargin(0.01);
		gPad->SetGridx();
		gPad->SetGridy();
		// gPad->SetLogy();
		TH1F*h_dummy=(TH1F*)h_categories[MAXCELLTYPES]->Clone("h_dummy");
		h_dummy->Reset();
		h_dummy->SetYTitle(Form("(%s #times A^{std})/ (%s^{std} #times A)",valuelabels.at(1).c_str(),valuelabels.at(1).c_str()));;
		h_dummy->SetMinimum(0);
		h_dummy->SetMaximum(4);
		h_dummy->Draw();
		TH1F* h_categories_norm[MAXCELLTYPES];
		TH1F* h_categories_meanerror_norm[MAXCELLTYPES];
		for (unsigned int i = 0; i < types.size(); ++i)
		{
			h_categories_norm[i]=(TH1F*)h_categories[i]->Clone(Form("h_categories_norm[%d]",i));
			h_categories_meanerror_norm[i]=(TH1F*)h_categories_meanerror[i]->Clone(Form("h_categories_meanerror_norm[%d]",i));
			double scalefac=1./h_categories[MAXCELLTYPES]->GetBinContent(1);
			//now size or capacity specific scales
			scalefac/=hc->GetPadSurface(types.at(i));

			h_categories_norm[i]->Scale(scalefac);
			h_categories_norm[i]->Draw("pex0same");
			h_categories_meanerror_norm[i]->Scale(scalefac);
			h_categories_meanerror_norm[i]->Draw("e1x0same");
		}
		padnum->Draw("same");
		full_leg->Draw();
		TPaveText *padcalibnorm=0;
		padcalibnorm = new TPaveText(0.96,0.92,0.96,0.92,"NDC");
		padcalibnorm->SetFillColor(kNone);
		padcalibnorm->SetLineColor(kNone);
		padcalibnorm->SetTextSize(0.03);
		padcalibnorm->SetTextAlign(32);
		padcalibnorm->SetTextColor(kBlack);
		padcalibnorm->AddText("Test capacities normalized to design values");
		padcalibnorm->Draw("same");
		string outfilename_relcat=stripExtension(outputfile)+"_relcat."+outputformat;
		c_relcat->Print(outfilename_relcat.c_str());


		if (isAverageValues){
			TCanvas *c_sensors = new TCanvas("c_sensors","c_sensors",800,500,int(canvwidth),int(canvwidth/1.5));
			c_sensors->cd();
			gPad->SetRightMargin(0.01);
			if (uselogy) gPad->SetLogy();
			gPad->SetGridx();
			gPad->SetGridy();
			TH1F*h_gr_contents=new TH1F("h_gr_contents","h_gr_contents",N_VALID_FILES,0,N_VALID_FILES);
			for (int i = 0; i < N_VALID_FILES; ++i)
			{
				// h_gr_contents->SetBinContent(i+1,gr_contents[135]->GetY()[i]);
				h_gr_contents->GetXaxis()->SetBinLabel(i+1,VALID_SENSOR_NAMES[i].c_str());
			}
			h_gr_contents->SetTitle("");
			h_gr_contents->SetXTitle("Sensors");
			h_gr_contents->GetXaxis()->SetTitleOffset(1.3);
			h_gr_contents->SetYTitle(h_fill->GetYaxis()->GetTitle());
			h_gr_contents->SetMarkerColor(0);
			h_gr_contents->SetLineColor(0);


			if (zoom.size()) h_gr_contents->GetYaxis()->SetRangeUser(zoom_min,zoom_max);
			else if (!uselogy) {
				h_gr_contents->SetMinimum(totalmin-totalrange*0.1);
				h_gr_contents->SetMaximum(totalmax+totalrange*0.2);
			}else{
				h_gr_contents->SetMinimum(totalmin/2);
				h_gr_contents->SetMaximum(totalmax*10);
			}


			h_gr_contents->Draw("*ex0");
			for (unsigned int i = 0; i < types.size(); ++i)
			{
				int drawnow=legendorder.at(types.size()-(i+1));
				gr_categories_sensors[drawnow]->SetLineWidth(2);
				gr_categories_sensors[drawnow]->Draw("plsame");
				// printf("%s: %f\n",gr_categories_sensors[i]->GetName(), gr_categories_sensors[i]->GetY()[0]);
			}
			padnum->Draw("same");
			full_leg->Draw();

			string outfilename_sensors=stripExtension(outputfile)+"_cat_sensors."+outputformat;
			c_sensors->Print(outfilename_sensors.c_str());
		}
	}












	else if (plotoption == "SEL"){
		TH1F **h_full = (TH1F**) LoadDataHisto(isAverageValues,ValueOption,inputfile,inputformat,mapfile,yscale,verbose,selector,channelnumber,measunc,appearance,fitfunc);
		TH1F *h_fill = h_full[0];
		if (!h_fill) return 1;
		h_fill->SetTitle("");
		h_fill->SetXTitle(Form("%s [%s]",selectorlabels.at(1).c_str(),selectorlabels.at(2).c_str()));
		h_fill->SetYTitle(Form("%s [%s]",valuelabels.at(1).c_str(),valuelabels.at(2).c_str()));
		h_fill->GetYaxis()->SetTitleOffset(1.3);
		h_fill->GetXaxis()->SetTitleOffset(1.2);
		h_fill->SetLineColor(kBlack);
		h_fill->SetMarkerStyle(20);
		h_fill->SetMarkerSize(1);
		// h_fill->Scale(yscale);
		TCanvas *c_chan = new TCanvas("c_chan","c_chan",800,500,int(canvwidth),int(canvwidth/1.5));
		c_chan->cd();
		gPad->SetRightMargin(0.01);
		gPad->SetGridx();
		gPad->SetGridy();
		// gPad->SetLogx();
		// gPad->SetLogy();
		h_fill->Draw("pl");

		TPaveText *padnum = MakeLabel(0.1,0.92,0.1,0.92,0.03,12);
		string all_sensors_string=isAverageValues?intToString(N_VALID_FILES)+" sensor"+(N_VALID_FILES>1?"s":"")+" in ":"";
		padnum->AddText(Form("Values for %schannel %d",all_sensors_string.c_str(),channelnumber));
		padnum->Draw("same");

		//add some textual info to the CV plot
		if (isCVinput) {
			//get the position of the last bin
			int last_bin = h_fill->GetNbinsX();
			double last_capacitance = h_fill->GetBinContent(last_bin); //pF
			const char* last_voltage = h_fill->GetXaxis()->GetBinLabel(last_bin);;

			//vacuum permittivity and relative permittivity for silicon
			const double epsilon_0 = 8.854187817e-12;
			const double epsilon_r = 11.7;

			//TODO: read cell area from the geometry file
			const double cell_area = 1; //cm^2
			double effective_width = 1e6 * epsilon_r * epsilon_0 * (cell_area*1e-4) / (last_capacitance*1e-12); //micrometers

			TPaveText *info = MakeLabel(0.9,0.25,0.9,0.32);
			info->AddText(Form("Sat. Capacitance: %.1fpF at %sV\n",last_capacitance, last_voltage));
			info->AddText(Form(" Effective Thickness: %.0fum at %sV",effective_width, last_voltage));
			info->Draw("same");
		}

		string outfilename=outputfile;
		if (!set_outputfile.size()) outfilename = stripExtension(outputfile)+"_chan"+intToString(channelnumber)+"."+outputformat;
		c_chan->Print(outfilename.c_str());


		if (isCVinput){
			//now draw the same as 1/pow(2)
			TCanvas *c_chanbias = new TCanvas("c_chanbias","c_chanbias",800,500,int(canvwidth),int(canvwidth/1.5));
			c_chanbias->cd();
			gPad->SetRightMargin(0.01);
			gPad->SetGridx();
			gPad->SetGridy();
			// gPad->SetLogx();
			// gPad->SetLogy();
			TGraphErrors*gr_fill=new TGraphErrors(0);
			double multiplocator=1000;
			for (int i = 0; i < h_fill->GetNbinsX(); ++i)
			{
				double cont=h_fill->GetBinContent(i+1);
				double err=h_fill->GetBinError(i+1);
				double newcont=multiplocator/pow(cont,2);
				double newerr=multiplocator*err*2./pow(cont,3);
				gr_fill->SetPoint(i, std::atof(h_fill->GetXaxis()->GetBinLabel(i+1)),newcont);
				gr_fill->SetPointError(i,0,newerr);
			}
			gr_fill->GetHistogram()->SetXTitle(h_fill->GetXaxis()->GetTitle());
			gr_fill->GetHistogram()->SetYTitle(Form("%.0f/%s^{2} [%s^{-2}]",multiplocator,valuelabels.at(1).c_str(),valuelabels.at(2).c_str()));
			gr_fill->SetTitle("");
			gr_fill->SetMarkerStyle(20);
			gr_fill->SetMarkerSize(0.8);
			gr_fill->Draw("pla");
			gr_fill->Draw("esame");

			//fit with two linear functions
			double minbinx=gr_fill->GetX()[0];
			double maxbinx=gr_fill->GetX()[gr_fill->GetN()-1];
			double almostminbinx=gr_fill->GetX()[1];
			double almostmaxbinx=gr_fill->GetX()[gr_fill->GetN()-2];
			TF1*f_sel_bias=new TF1("f_sel_bias",fit_function_linearintersection,minbinx,maxbinx,4);
			f_sel_bias->SetParLimits(0,almostminbinx,almostmaxbinx);
			gr_fill->Fit(f_sel_bias,"Q");

			TPaveText *padbias=0;
			padbias = new TPaveText(0.9,0.25,0.9,0.25,"NDC");
			padbias->SetFillColor(kNone);
			padbias->SetLineColor(kNone);
			padbias->SetTextSize(0.03);
			padbias->SetTextAlign(32);
			padbias->SetTextColor(kBlack);
			padbias->AddText(Form("Full depletion at %.1f #pm %.1f V",f_sel_bias->GetParameter(0),f_sel_bias->GetParError(0)));
			padbias->Draw("same");

			padnum->Draw("same");
			outfilename=stripExtension(outputfile)+"_bias_chan"+intToString(channelnumber)+"."+outputformat;
			c_chanbias->Print(outfilename.c_str());
		}
	}















	else if (plotoption == "FULL"){
		TH2F *h_fill=0;
		double allchan_selector=ALLCHAN_SELECTOR;
		if (!isAverageValues) h_fill = (TH2F*)LoadValues(inputfile,inputformat,mapfile,yscale,verbose,allchan_selector,-1,measunc,appearance)[0];
		else                  h_fill = (TH2F*)LoadAverageValues(inputfile,inputformat,mapfile,yscale,verbose,allchan_selector,-1,measunc,appearance);

		// TH2F *h_fill = (TH2F*)LoadValues(inputfile,inputformat,mapfile,yscale,verbose,-1,-1)[0];
		if (!h_fill) return 1;
		//get max of each column
		int nsels_help=h_fill->GetNbinsY();
		int minpad_help=1;
		int maxpad_help=h_fill->GetNbinsX();
		TH1F *hmax=new TH1F("hmax","hmax",maxpad_help-minpad_help+1,minpad_help-0.5,maxpad_help+0.5);
		hmax->SetLineColor(kRed);
		hmax->SetLineWidth(2);
		TH1F *hmin=new TH1F("hmin","hmin",maxpad_help-minpad_help+1,minpad_help-0.5,maxpad_help+0.5);
		hmin->SetLineColor(kRed);
		hmin->SetLineWidth(2);
		for (int j = 0; j < nsels_help; ++j)
		{
			for (int i = minpad_help; i <= maxpad_help; ++i)
			{
				double binc=h_fill->GetBinContent(i,j+1);
				if (hmax->GetBinContent(i+1)<binc || j==0) hmax->SetBinContent(i+1,binc);
				if (hmin->GetBinContent(i+1)>binc || j==0) hmin->SetBinContent(i+1,binc);
			}
		}
		TH2F* h2draw=(TH2F*) h_fill->Clone();
		TH1F*h_scale=0;
		if (showmaxval) h_scale=(TH1F*)hmax->Clone();
		else h_scale=(TH1F*)hmin->Clone();
		h2draw->Reset();
		//fill them normalised to column max
		for (int i = minpad_help; i <= maxpad_help; ++i)
		{
			for (int j = 0; j < nsels_help; ++j)
			{
				double cont_new=0;
				double cont_scale=h_scale->GetBinContent(i+1);
				if (cont_scale) cont_new=h_fill->GetBinContent(i,j+1)/cont_scale;
				h2draw->Fill(i,j,cont_new);
			}
		}

		// find asymptotic values
		TH1F *h_saturation=new TH1F("h_saturation",Form("h_saturation;Pad;saturation %s [%s]",selectorlabels.at(0).c_str(),selectorlabels.at(2).c_str()),maxpad_help-minpad_help+1,minpad_help-0.5,maxpad_help+0.5);
		for (int i = minpad_help; i <= maxpad_help; ++i)
		{
			TH1F* h_proj=(TH1F*)h2draw->ProjectionY("h_proj",i,i);
			double minval=showmaxval?0:1;
			double maxval=h_proj->GetBinContent(h_proj->GetMaximumBin());
			// printf("This pad %d has min %f and max %f\n",i,minval,maxval );
			int saturated_sel=-1;
			for (int j = 0; j < nsels_help; ++j)
			{
				double cont=h2draw->GetBinContent(i,j+1);
				if (showmaxval){
					if (cont>0.9*maxval){
						// printf("Maximum reached at %s (%f)\n",h2draw->GetYaxis()->GetBinLabel(j+1),cont);
						saturated_sel=j;
						break;
					}
				}else{
					if (cont<1.1*minval){
						// printf("Minimum reached at %s (%f)\n",h2draw->GetYaxis()->GetBinLabel(j+1),cont);
						saturated_sel=j;
						break;
					}
				}
			}
			if (saturated_sel!= -1){
					// printf("Saturation of pad %d (%s) reached at %s %s\n",i,hc->GetPadTypeString(i).c_str(),h2draw->GetYaxis()->GetBinLabel(saturated_sel+1),selectorlabels.at(2).c_str());
					if (hc->GetPadTypeString(i)=="standard") h_saturation->SetBinContent(i,std::atof(h2draw->GetYaxis()->GetBinLabel(saturated_sel+1)));
			}



			// if (i == 101){
			// 	TCanvas *c_test = new TCanvas("c_test","c_test",800,500,int(canvwidth),int(canvwidth/1.5));
			// 	c_test->cd();
			// 	h_proj->Draw("hist");
			// 	c_test->Print("test.png");
			// }
		}
		double average=GetAverageBinContent(h_saturation)[0];
		double stddeviation=GetAverageBinContent(h_saturation)[2];
		// printf("Average saturation %s of standard cells: %f +- %f %s\n",selectorlabels.at(2).c_str(),average,stddeviation,selectorlabels.at(2).c_str());
		string linename=Form("saturation %s standard cells at %s [%s]",valuelabels.at(0).c_str(),selectorlabels.at(0).c_str(),selectorlabels.at(2).c_str());
		string lineval=Form("%.2f\t%.2f",average,stddeviation );
		WriteValue(datafilename,linename,lineval);


		// TCanvas *c_saturation = new TCanvas("c_saturation","c_saturation",800,500,int(canvwidth),int(canvwidth/1.5));
		// c_saturation->cd();
		// h_saturation->SetTitle((outputname+": standard cells saturation "+selectorlabels.at(0)).c_str());
		// h_saturation->Draw("hist");
		// string outputfile_saturation=stripExtension(outputfile)+"_saturation_"+selectorlabels.at(0)+"."+outputformat;
		// c_saturation->Print(outputfile_saturation.c_str());


		//draw it
		TCanvas *c_chan = new TCanvas("c_chan","c_chan",800,500,int(canvwidth),int(canvwidth/1.5));
		// gPad->SetLogz();
		gPad->SetRightMargin(0.19);
		if (showmaxval) h2draw->GetZaxis()->SetTitle(Form("%s/%s_{max}",valuelabels.at(1).c_str(),valuelabels.at(1).c_str()));
		else h2draw->GetZaxis()->SetTitle(Form("%s/%s_{min}",valuelabels.at(1).c_str(),valuelabels.at(1).c_str()));
		h2draw->GetZaxis()->SetTitleOffset(0.9);
		if (showmaxval) h2draw->GetZaxis()->SetRangeUser(0,1);
		else h2draw->GetZaxis()->SetRangeUser(1,h2draw->GetMaximum());
		// h2draw->GetZaxis()->SetNdivisions(101);
		h2draw->SetTitle("");
		h2draw->SetXTitle("Pad number");
		h2draw->SetYTitle(Form("%s [%s]",selectorlabels.at(1).c_str(),selectorlabels.at(2).c_str()));
		h2draw->Draw("COLZ");
		//scale hmax to the pad coordinates
		double rightmax = 1.1*h_scale->GetMaximum();
		double scale = 1.*nsels_help/rightmax;
		h_scale->Scale(scale);
		h_scale->Draw("samehist");
		//draw an axis on the right side
		TGaxis *redaxis = new TGaxis(maxpad_help+0.5,0,maxpad_help+0.5,nsels_help,0,rightmax,110,"+L");
		if (showmaxval) redaxis->SetTitle(Form("%s_{max} [%s]",valuelabels.at(1).c_str(),valuelabels.at(2).c_str()));
		else redaxis->SetTitle(Form("%s_{min} [%s]",valuelabels.at(1).c_str(),valuelabels.at(2).c_str()));
		redaxis->SetTitleOffset(1);
		redaxis->SetTitleSize(0.035);
		redaxis->SetTitleColor(kRed);
		redaxis->SetLabelOffset(0.01);
		redaxis->SetLabelColor(kRed);
		redaxis->SetLabelSize(0.035);
		redaxis->SetLineColor(kRed);
		redaxis->Draw();

		gPad->Update();
		TPaletteAxis *palette = (TPaletteAxis*)h2draw->GetListOfFunctions()->FindObject("palette");
		palette->SetX1NDC(0.895);
		palette->SetX2NDC(0.92);

		c_chan->Print(outputfile.c_str());
	}
















	else if (plotoption=="MERGE"){
		ofstream fileout;
		fileout.open(outputfile.c_str());
		fileout.close();
		printf("Merge all files in one: %s !\n",outputfile.c_str());
		std::vector<string> names_master;
		//define histograms to collect info
		TH1F*h_cat[MAXCELLTYPES];
		TH1F*h_cat_sensors[MAXCELLTYPES];
		TCanvas*c_cat_sensors=new TCanvas("c_cat_sensors","c_cat_sensors",800,500,int(canvwidth),int(canvwidth/1.5));
		TCanvas*c_cat=new TCanvas("c_cat","c_cat",800,500,int(canvwidth),int(canvwidth/1.5));
		for (unsigned int im = 0; im < mergefiles.size(); ++im)
		{
			string infilename = OUTPUTDIR + (OUTPUTDIR.empty() ? "" : "/") + mergefiles.at(im);
			string insensorname=getFileNameNoExt(mergefiles.at(im));
			if (!std::ifstream(infilename))
			{
				printf("Warning: file %s doesn't exist!\n",infilename.c_str());
				continue;
			}
			if (verbose) printf("Merging file %s\n",infilename.c_str());
			TTree *tree_merge = new TTree("tree_merge", "tree_merge");

			tree_merge->ReadFile(infilename.c_str(), "name/C:val/D:err/D",'\t');
			char name[300];
			double val, err;
			tree_merge->SetBranchAddress("name",&name);
			tree_merge->SetBranchAddress("val",&val);
			tree_merge->SetBranchAddress("err",&err);
			std::vector<string> names;
			std::vector<double> vals;
			std::vector<double> errs;
			std::vector<int> masterpositions;
			fileout.open(outputfile.c_str(),std::ofstream::app);
			for (unsigned int ientry = 0; ientry < tree_merge->GetEntries(); ++ientry)
			{
				tree_merge->GetEntry(ientry);
				names.push_back(string(name));
				vals.push_back(val);
				errs.push_back(err);
			}

			//write it out and deal with canvasses
			if (im==0){
				//description line 0
				fileout<<"Category"<<"\t";
				for (unsigned int i = 0; i < names.size(); ++i)
				{
					fileout<<names.at(i)<<"\t\t";
					names_master.push_back(names.at(i));
					masterpositions.push_back(i);
				}
				fileout<<endl;
				//description line 1
				fileout<<"Name"<<"\t";
				for (unsigned int i = 0; i < names.size(); ++i)
				{
					fileout<<"Value"<<"\t"<<"Unc"<<"\t";
				}
				fileout<<endl;
				//divide the canvasses
				int npads=names_master.size();
				c_cat_sensors->Divide(5,int(ceil(1.0*npads/5)));
				c_cat->Divide(5,int(ceil(1.0*npads/5)));
			}else{
				//make sure same order of categories in the file
				for (unsigned int i = 0; i < names.size(); ++i)
				{
					int pos = find(names_master.begin(), names_master.end(), names.at(i)) - names_master.begin();
					if (verbose) printf("Found %s (%s) at position %d in master!\n",names.at(i).c_str(),names_master.at(pos).c_str(),pos );
					if (pos<int(names_master.size())) masterpositions.push_back(pos);
					else{
						masterpositions.push_back(-1);
						printf("Warning: entry '%s' in %s is not in master! Skipping this entry!\n",names.at(i).c_str(),infilename.c_str());
					}
				}
			}
			//fill histograms and write values out
			fileout<<getFileNameNoExt(infilename)<<"\t";
			for (unsigned int i = 0; i < names_master.size(); ++i)
			{
				bool foundit=false;
				double value(-9999999),error(-9999999);
				for (unsigned int j = 0; j < masterpositions.size(); ++j)
				{
					if (masterpositions.at(j)==int(i)) {
						foundit=true;
						value=vals.at(j);
						error=errs.at(j);
						fileout<<value<<"\t"<<error<<"\t";
						break;
					}
				}
				if (!foundit){
					printf("Warning: entry '%s' not found in %s! Leaving values empty!\n",names_master.at(i).c_str(), infilename.c_str());
					fileout<<"\t"<<"\t";
					continue;
				}
				string histname=names_master.at(i);
				rtrim(histname);
				std::replace(histname.begin(),histname.end(),' ','_');
				string axislabel=histname;
				std::replace(axislabel.begin(),axislabel.end(),'_',' ');
				if (im==0) {
					h_cat_sensors[i]=new TH1F(Form("h_sensor_%s",histname.c_str()),Form("h_sensor_%s;;%s",histname.c_str(),axislabel.c_str()),mergefiles.size(),0-0.5,mergefiles.size()-0.5);
				}
				// if (!h_cat_sensors[i]) h_cat_sensors[i]=new TH1F(Form("h_sensor_%s",histname.c_str()),Form("h_sensor_%s",histname.c_str()),mergefiles.size(),0-0.5,mergefiles.size()-0.5);
				h_cat_sensors[i]->GetXaxis()->SetBinLabel(im+1,insensorname.c_str());
				if (error>=0){
					h_cat_sensors[i]->SetBinContent(im+1,value);
					h_cat_sensors[i]->SetBinError(im+1,error);
				}else{
					h_cat_sensors[i]->SetBinContent(im+1,0);
					h_cat_sensors[i]->SetBinError(im+1,13371337);
				}
			}
			fileout<<endl;
			fileout.close();
		}
		//create and fill summary histograms
		for (unsigned int i = 0; i < names_master.size(); ++i)
		{
			string histname=names_master.at(i);
			rtrim(histname);
			std::replace(histname.begin(),histname.end(),' ','_');
			string axislabel=histname;
			std::replace(axislabel.begin(),axislabel.end(),'_',' ');
			double histmax=h_cat_sensors[i]->GetMaximum();
			double histmin=h_cat_sensors[i]->GetMinimum();
			h_cat[i]=new TH1F(Form("h_%s",histname.c_str()),Form("h_%s;%s;entries",histname.c_str(),axislabel.c_str()),30,histmin-(histmax-histmin)*0.1,histmax+(histmax-histmin)*0.1);
			for (int jb = 0; jb < h_cat_sensors[i]->GetNbinsX(); ++jb)
			{
				double cont=h_cat_sensors[i]->GetBinContent(jb+1);
				if (cont!=13371337) h_cat[i]->Fill(cont);
			}
		}
		//now plot the histograms and write out summary values
		string outputfile_summary = OUTPUTDIR + (OUTPUTDIR.empty() ? "" : "/") + getFileNameNoExt(outputfile) + "_summary.txt";
		printf("Write analysis summary to %s\n",outputfile_summary.c_str() );
		fileout.open(outputfile_summary.c_str());
		fileout << "#### Analysis summary file ####"<<endl;
		fileout << endl;
		fileout << "#Sensors included in this file: "<<endl;
		fileout << "#";
		for (unsigned int i = 0; i < mergefiles.size(); ++i)
		{
			fileout << getFileNameNoExt(mergefiles.at(i));
			if (i<mergefiles.size()-1) fileout <<", ";
			else fileout <<";";
		}
		fileout << endl;
		fileout << endl;

		WriteValue(outputfile_summary,"#Categories","Mean\tRMS");
		for (unsigned int i = 0; i < names_master.size(); ++i)
		{
			c_cat_sensors->cd(i+1);
			// c_cat_sensors->GetPad(i+1)->SetGridx();
			h_cat_sensors[i]->Draw("e");
			c_cat->cd(i+1);
			h_cat[i]->SetMaximum(h_cat[i]->GetMaximum()*1.3);
			h_cat[i]->Draw("hist");

			TPaveText*infopave = new TPaveText(0.15,0.73,0.88,0.88,"NDC");
			infopave->SetFillColor(kNone);
			infopave->SetLineColor(kNone);
			infopave->SetTextSize(0.05);
			infopave->SetTextAlign(22);
			infopave->SetTextColor(kBlack);
			infopave->AddText(Form("Mean = %.2f",h_cat[i]->GetMean()));
			infopave->AddText(Form(" RMS = %.2f",h_cat[i]->GetRMS()));
			infopave->Draw("same");

			string lineval=Form("%.2f\t%.2f",h_cat[i]->GetMean(),h_cat[i]->GetRMS() );
			WriteValue(outputfile_summary,names_master.at(i),lineval);

			h_cat[i]->Draw("same");
			h_cat[i]->Draw("axissame");
		}
		fileout.close();
		string cat_sensors_outputfile = OUTPUTDIR + (OUTPUTDIR.empty() ? "" : "/") + getFileNameNoExt(outputfile) + "_summary_sensors." + outputformat;
		c_cat_sensors->Print(cat_sensors_outputfile.c_str());
		string cat_outputfile = OUTPUTDIR + (OUTPUTDIR.empty() ? "" : "/") + getFileNameNoExt(outputfile) + "_summary_distr." + outputformat;
		c_cat->Print(cat_outputfile.c_str());
	}









	else{
		printf("Warning: plot option '%s' not implemented. Aborting.\n",plotoption.c_str() );
	}

	if (verbose) printf("+++++ End HexPlot +++++\n");
	return 0;
}
