#include "TTree.h"

int mappednumbers[MAXCELLS];
bool mapsloaded=false;

inline static void LoadMaps(string mapfile, int verbose){
///This loads a number to pad mapping	
	if (mapsloaded) {
		// printf("Maps already loaded! Skip!\n");
		return;
	}


	if (verbose) printf("Executing LoadMaps...\n");
	
	//load maps
#ifdef _WIN32
	std::vector<string> maps=split(mapfile,':=;');
#else
	std::vector<string> maps=split(mapfile,':');
#endif

	TTree *tree_map = new TTree("tree_map", "tree_map");
	for (int ipad = 0; ipad < MAXCELLS; ++ipad)
	{
		if (mapfile.size()){
			int ibin=ipad;
			int num(-1);
			for (unsigned int i_map = 0; i_map < maps.size(); ++i_map)
			{
				tree_map->ReadFile(maps.at(i_map).c_str(), "old_num/I:new_num/I");
				int old_num, new_num;
				tree_map->SetBranchAddress("old_num",&old_num);
				tree_map->SetBranchAddress("new_num",&new_num);
				for (int i = 0; i < tree_map->GetEntries(); ++i)
				{
					tree_map->GetEntry(i);
					if(old_num==ibin) {
						num=new_num;
						break;
					}
				}
				if (verbose && ibin != num && num != -1) printf("Map %d %s: linking %d to %d\n",int(i_map),maps.at(i_map).c_str(),ibin,num );
				ibin=num;
				tree_map->Reset();
			}
			mappednumbers[ipad]=num;
		}else{
			mappednumbers[ipad]=ipad;
		}
	}
	if (verbose > 1){
		if (!mapfile.size()) {
			printf("No map file provided, assuming identity mapping!\n");
		}
		else{
			for (int i = 0; i < MAXCELLS; ++i)
			{
				if (mappednumbers[i] != -1) printf("Number %d links to pad %d\n",i,mappednumbers[i]);
			}
		}
	}
	mapsloaded=true;
	if (verbose) printf("LoadMaps finished!\n");
}


inline static int MappedNumber(int ibin, string mapfile, int verbose){
	LoadMaps(mapfile,verbose);
	return mappednumbers[ibin];
}