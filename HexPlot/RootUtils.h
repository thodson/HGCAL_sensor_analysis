#ifndef MYROOTUTILS_H
#define MYROOTUTILS_H

#include "TMatrixD.h"
#include "TGraphAsymmErrors.h"
#include "TH2F.h"
#include "TH2D.h"
#include "TPad.h"
#include "TVirtualPad.h"
#include "TPolyLine.h"
#include "TSpline.h"

#include "CppUtils.h"

double* GetBarlowPexpCorrFactor( double, double, double, int verbose = 1, int replacement = 1);
double round_to_n_digits(double, int);
void round_to_error_n_digits(double, double, int, string &rx, string &re);
void check_rounding();
double DiffUnc(double, double,double rho = 0,double factor = 1);
double ratiounc(double, double, double, double, double, bool v=false);
double ratiounc(int, int,  double, bool);
double ratiounc(int, int, bool);
double GetPhi(double, double);
double GetCorrelationFactorError(TH2F*);
double* GetMinHistoRegion(TH1F* h,double frac=1.0,string func="RMS", bool verbose=false);
double IncrMeanCalc(double,int,double);
double IncrSdtDevCalc(double,double,int,double);
void RMStest(int);
double DeltaR(double,double, double, double);
double EtaFromTheta(double);
int* GetColLineFillStyle(int);
int* GetColLineFillStyleNoBlack(int);
TH1F* CutOffBins(TH1F* h,int);
void SavePad(TPad* pad, const char*, int x = 700, int y = 500);
void SavePad(TVirtualPad* pad, const char*, int x = 700, int y = 500);
void RemoveNegBins(TH1* hist);
void RemoveNaNBins(TH1* hist);
void AbsValBins(TH1F* hist);
void AddSymmBinErrFromDiff(TH1F*, TH1F*, TH1F*, TH1F*, bool ExcludeNormUnc = false);
void SquareRootBins(TH1F*);
double Chi2Hist(TH1F*, TH1F*, bool norm = false, double xmin = 0, double xmax = 0);
double Chi2Hist(TH1F*, TF1*, bool norm = false, double xmin = 0, double xmax = 0);
void SmearBins(TH1F*, TH1F*, int seed = 0);
void SmearBins(TH1F*, int seed = 0);
void PrintMatrix(const TMatrixD&, string format ="", string name = "", int cols_per_sheet = 10);
TMatrixD* RemoveRowCol(TMatrixD*, std::vector<int>, std::vector<int>, bool verbose=false);
TMatrixD* RemoveRowCol(TMatrixD*, int row=-1, int col=-1, int verbose=1);
TH2D* Matrix2TH2D(TMatrixD);
TMatrixD TH2D2Matrix(TH2D*);
void ScaleBins(TH1F* hist, double);
TH1F* FuncRatio(TF1*, TF1*,double, double, int npoints = 10000);
double AverageErrorPerBin(TH1F*);
double CumulativeBinResidual(TH1F*,TH1F*);
double GetMaximum(TGraph*, bool forplotting=false);
double GetMinimum(TGraph*, bool forplotting=false);
void PrintTree2Txt(string filename, int maxevt = 0);
double BinPosError(TH1F*,int);
double FWHM(TH1F*);
double FWHMError(TH1F*);
void PrintBins(TH1F*);
TH2F* Divide2d(TH2F*, TH2F*);
void SetLargeErrorBinsToOne(TH2F*);
void SetMaxOfHistos(TH1* h=0,TH1* h1=0);
void DeleteObjectFromFile(string filename,string objectname);
TH2D* RotateNinetyDegrees(TH2D*);
TH1F* RandomizeHisto(TH1F*,int events=-1);
void DrawWhiteBin(int, int, TH2D* h=0);
void FILL(TH1F*, double x=1, double w=1);
TH1F* GetRatio(TH1F*, TH1F*, int col = -1, double min=0.9, double max=1.1, bool normalized = false, double corrsinglebins = 0);
double GC_up(double);
double GC_down(double);
TGraphAsymmErrors* poissonize(TH1* h);
void CheckHistoCompatibility(TH1F*, TH1F*, double textx=0.12, bool displaychisq = true, float xmin = 0, float xmax = 0, int nfuncpar = 0, TH2D* h_invcov=0);
void CheckHistoCompatibility(TH1F*, TF1*, double textx=0.12, bool displaychisq = true, float xmin = -1, float xmax = -1);
TH1F* ProjectionY(TH1F*,bool ExcludeEmptyBins=true, double max=0);
double* GetAverageBinContent(TH1F*);
double GraphIntegral(TGraph*);
void DrawTH2F(TH2F*, string);
void RotatePoly(TPolyLine*,double,double,double);
double* GetPolyCenterOfGravity(TPolyLine*);
double FindFirstX(TSpline*,double,double,double);
TPaveText* MakeLabel(Double_t x1, Double_t y1, Double_t x2, Double_t y2, Double_t size=0.03, Double_t align=32);

#endif



