#ifndef CPPUTILS_H
#define CPPUTILS_H

#ifdef _WIN32
	#include <windows.h>
#endif

#include <string>
#include <vector>


#define ANSI_COLOR_RESET   "\x1b[0m"
#define ANSI_COLOR_BLACK   "\x1b[30m"
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_WHITE   "\x1b[37m"
#define ANSI_COLOR_BOLDBLACK   "\033[1m\033[30m"
#define ANSI_COLOR_BOLDRED     "\033[1m\033[31m"
#define ANSI_COLOR_BOLDGREEN   "\033[1m\033[32m"
#define ANSI_COLOR_BOLDYELLOW  "\033[1m\033[33m"
#define ANSI_COLOR_BOLDBLUE    "\033[1m\033[34m"
#define ANSI_COLOR_BOLDMAGENTA "\033[1m\033[35m"
#define ANSI_COLOR_BOLDCYAN    "\033[1m\033[36m"
#define ANSI_COLOR_BOLDWHITE   "\033[1m\033[37m"



using namespace std;
bool StartsWith(string, string);
bool Contains(string, string);
bool file_is_empty(string);
void PrintProgressBar(int, int);
bool isFile (const std::string&);
bool isFile (char*);
string floatToString(float, unsigned digitsafterpoint = 1);
string doubleToString(double, unsigned int digitsafterpoint = 1);
string PrintThisValue(string, double, double, string NameOfUnit = "", int SigDigits = -1);
int getNdigitsinstring(string);
int digitfromstring(string, int appearance = 0);
bool stringcomp (string,string) ;
string intToString(int, unsigned intdigits = 0);
bool IsInArray(int, int[], int);
bool IsInVector(int,std::vector<int>);
void stringToArray(string, int[]);
void stringToArrayDouble(string, double[]);
std::string &ltrim(std::string&);
std::string &rtrim(std::string&);
std::string &trim(std::string&);
vector<string> split(const string&, char);
void WriteValue(string,string, string);
string getFileName(const string&);
string getFileNameNoExt(const string&, string delim=".");
string getFileDir(const string&);
string stripExtension(const string&, string delim="." );
string rmString(const string&, string rmstring=" ");
bool YesNoRequest(string);
string HoursMinutes(double);
int Sign(double);
double Positive(double);
void SwitchColor(string);

#endif
