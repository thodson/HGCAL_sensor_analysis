#include <fstream>
#include <sstream>
#include <iostream>
#include <cstring>
#include <iomanip>
#include <limits>
#include <vector>
#include <algorithm>
#include <cmath>
#include <functional>
#include <cctype>

#include "CppUtils.h"

using namespace std;

#ifdef _WIN32
bool WindowsStandardColorFilled = false;
WORD saved_attributes;
#endif
//______________________________________________________________________________
//______________________________________________________________________________
//
//Shell magic
//______________________________________________________________________________
//______________________________________________________________________________
void SwitchColor(string color){
#ifdef _WIN32
  HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
  if (color != "RESET" && !WindowsStandardColorFilled){
  	CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
  	GetConsoleScreenBufferInfo(hConsole, &consoleInfo);
    saved_attributes = consoleInfo.wAttributes;
    WindowsStandardColorFilled = true;
  }
  if (color == "BLACK") SetConsoleTextAttribute(hConsole, 80);
  if (color == "RED") SetConsoleTextAttribute(hConsole, 92);
  if (color == "GREEN") SetConsoleTextAttribute(hConsole, 90);
  if (color == "YELLOW") SetConsoleTextAttribute(hConsole, 94);
  if (color == "BLUE") SetConsoleTextAttribute(hConsole, 91);
  if (color == "MAGENTA") SetConsoleTextAttribute(hConsole, 93);
  if (color == "CYAN") SetConsoleTextAttribute(hConsole, 83);
  if (color == "WHITE") SetConsoleTextAttribute(hConsole, 86);
  if (color == "BOLDBLACK") SetConsoleTextAttribute(hConsole, 6);
  if (color == "BOLDRED") SetConsoleTextAttribute(hConsole, 192);
  if (color == "BOLDGREEN") SetConsoleTextAttribute(hConsole, 160);
  if (color == "BOLDYELLOW") SetConsoleTextAttribute(hConsole, 224);
  if (color == "BOLDBLUE") SetConsoleTextAttribute(hConsole, 176);
  if (color == "BOLDMAGENTA") SetConsoleTextAttribute(hConsole, 208);
  if (color == "BOLDCYAN") SetConsoleTextAttribute(hConsole, 48);
  if (color == "BOLDWHITE") SetConsoleTextAttribute(hConsole, 240);
  if (color == "RESET") {
    SetConsoleTextAttribute(hConsole, saved_attributes);
    // SetConsoleTextAttribute(hConsole, 95); //for windows powershell
  }
#else
    if (color == "RESET") printf(ANSI_COLOR_RESET);
    if (color == "BLACK") printf(ANSI_COLOR_BLACK);
    if (color == "RED") printf(ANSI_COLOR_RED);
    if (color == "GREEN") printf(ANSI_COLOR_GREEN);
    if (color == "YELLOW") printf(ANSI_COLOR_YELLOW);
    if (color == "BLUE") printf(ANSI_COLOR_BLUE);
    if (color == "MAGENTA") printf(ANSI_COLOR_MAGENTA);
    if (color == "CYAN") printf(ANSI_COLOR_CYAN);
    if (color == "WHITE") printf(ANSI_COLOR_WHITE);
    if (color == "BOLDBLACK") printf(ANSI_COLOR_BOLDBLACK);
    if (color == "BOLDRED") printf(ANSI_COLOR_BOLDRED);
    if (color == "BOLDGREEN") printf(ANSI_COLOR_BOLDGREEN);
    if (color == "BOLDYELLOW") printf(ANSI_COLOR_BOLDYELLOW);
    if (color == "BOLDBLUE") printf(ANSI_COLOR_BOLDBLUE);
    if (color == "BOLDMAGENTA") printf(ANSI_COLOR_BOLDMAGENTA);
    if (color == "BOLDCYAN") printf(ANSI_COLOR_BOLDCYAN);
    if (color == "BOLDWHITE") printf(ANSI_COLOR_BOLDWHITE);
#endif
}


//______________________________________________________________________________
//______________________________________________________________________________
//
//CPP MAGIC
//______________________________________________________________________________
//______________________________________________________________________________

bool StartsWith(string a, string b)
{
  if(strncmp(a.c_str(), b.c_str(), strlen(b.c_str())) == 0) return 1;
  return 0;
}
bool Contains(string a, string b)
{
  return a.find(b) < a.size();
}
bool file_is_empty(string fname)
{
  std::ifstream pFile(fname.c_str(), std::ios_base::binary);
  return pFile.peek() == std::ifstream::traits_type::eof();
}
void PrintProgressBar(int now, int max)
{
  for (int i=1;i<=now+1;i++)
  {
    fprintf(stdout, "\r[");
    for (int j=1;j<i;j++) fprintf(stdout, "-"); // ,.-'`'-.,.-'`'-.,.-'`'-
      if(i<=max)
      {
        fprintf(stdout, ">");
        for (int p=1; p<max-i+1;p++) fprintf(stdout, " ");
      }
    fprintf(stdout, "] ");
    fflush(stdout);
  }
}
bool isFile (const std::string& filename) {
  ifstream ifile(filename);
  return ifile.is_open();
}
bool isFile (char* filename) {
  return isFile(string(filename));
}
string floatToString(float number, unsigned int digitsafterpoint)
{
  stringstream stream;
  stream << fixed << setprecision(digitsafterpoint) << number;
  return stream.str();
}
string doubleToString(double number, unsigned int digitsafterpoint)
{
  stringstream stream;
  stream << fixed << setprecision(digitsafterpoint) << number;
  return stream.str();
}
string PrintThisValue(string NameOfValue, double Value, double Unc, string NameOfUnit, int SigDigits)
{
  string text="";
  double foo = Unc/100000000.;
  int digitsafterpointe = -8;
  while (foo<1 && digitsafterpointe<10)
  {
    digitsafterpointe++;
    foo*=10;
  }


  if (NameOfUnit != "") NameOfUnit = " " + NameOfUnit;

  if (digitsafterpointe <= 0){
    text=NameOfValue+" = "+doubleToString(Value,0)+" #pm "+doubleToString(Unc,0)+NameOfUnit;
  }else if (digitsafterpointe == 1){
    text=NameOfValue+" = "+doubleToString(Value,1)+" #pm "+doubleToString(Unc,1)+NameOfUnit;
  }else if (digitsafterpointe == 2){
    text=NameOfValue+" = "+doubleToString(Value,2)+" #pm "+doubleToString(Unc,2)+NameOfUnit;
  }else if (digitsafterpointe == 3){
    text=NameOfValue+" = "+doubleToString(Value,3)+" #pm "+doubleToString(Unc,3)+NameOfUnit;
  }else if (digitsafterpointe == 4){
    text=NameOfValue+" = "+doubleToString(Value,4)+" #pm "+doubleToString(Unc,4)+NameOfUnit;
  }else if (digitsafterpointe == 5){
    text=NameOfValue+" = "+doubleToString(Value,5)+" #pm "+doubleToString(Unc,5)+NameOfUnit;
  }else if (digitsafterpointe == 6){
    text=NameOfValue+" = "+doubleToString(Value,6)+" #pm "+doubleToString(Unc,6)+NameOfUnit;
  }else if (digitsafterpointe == 7){
    text=NameOfValue+" = "+doubleToString(Value,7)+" #pm "+doubleToString(Unc,7)+NameOfUnit;
  }else if (digitsafterpointe == 8){
    text=NameOfValue+" = "+doubleToString(Value,8)+" #pm "+doubleToString(Unc,8)+NameOfUnit;
  }else
    text=NameOfValue+" = "+doubleToString(Value,4)+NameOfUnit;
  if (SigDigits != -1){
    text=NameOfValue+" = "+doubleToString(Value,SigDigits)+" #pm "+doubleToString(Unc,SigDigits)+NameOfUnit;
  }
  return text;
}
int getNdigitsinstring(string str)
{
  std::string temp;
  int nappearance=0;
  for (unsigned int i=0; i < str.size(); i++)
  {
    if (i>0 && !isdigit(str[i]) && isdigit(str[i-1])){
      nappearance++;
    }
    if (i == str.size()-1 && isdigit(str[i])) nappearance++;
  }
  return nappearance;
}
int digitfromstring(string str, int appearance)
{
  std::string temp;
  int number=-1;
  int nappearance=0;
  for (unsigned int i=0; i < str.size(); i++){
    if (isdigit(str[i])){
      if (appearance == nappearance){
        for (unsigned int a=i; a<str.size(); a++){
          temp += str[a];
        }
        break;
      }
    }else{
      if (i>0 && isdigit(str[i-1])){
        nappearance++;
      }
    }
  }
  std::istringstream stream(temp);
  stream >> number;
  return number;
}
bool stringcomp (string i,string j) {
  return (i.compare(j) == -1);
}
string intToString(int number, unsigned int digits)
{
  std::stringstream ss;
  ss << number;
  std::string temp = ss.str();
  if (!digits) digits=temp.size();
  while (temp.size()<digits) {
    temp = "0" + temp;
  }
  return temp;
}
bool IsInArray(int element, int arr[], int arr_size)
{
  for (int i = 0; i < arr_size; ++i)
  {
    if (element == arr[i]) return true;
  }
  return false;
}
bool IsInVector(int num,std::vector<int> vec)
{
  int pos = find(vec.begin(), vec.end(), num) - vec.begin();
  if(pos>=int(vec.size())) return false;
  return true;
}
void stringToArray(string str, int arr[])
{
  for (int i = 0; i < getNdigitsinstring(str); ++i)
  {
    int val=digitfromstring(str,i);
    // printf("digit %i: %d\n",i,val );
    arr[i]=val;
  }
}
void stringToArrayDouble(string str, double arr[])
{
  for (int i = 0; i < getNdigitsinstring(str); ++i)
  {
    double val=digitfromstring(str,i);
    // printf("digit %i: %.2f\n",i,val );
    arr[i]=val;
  }
}
// trim from start
std::string &ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}
// trim from end
std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}
// trim from both ends
std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}
vector<string> split(const string &s, char delim) {
    stringstream ss(s);
    string item;
    vector<string> tokens;
    while (getline(ss, item, delim)) {
        tokens.push_back(item);
    }
    return tokens;
}
void WriteValue(string datafilename,string search, string val)
{

  ifstream filein;

  string line;
  unsigned int curLine = 0;
  filein.open(datafilename.c_str());
  bool foundit=false;
  while(getline(filein, line))
  {
      curLine++;
      if (line.find(search, 0) != string::npos)
      {
          // cout << "found: " << search << "line: " << curLine <<"! Changing value!" << endl;
      foundit=true;
      break;
      }
  }
  filein.close();
  filein.open(datafilename.c_str());
  if (foundit)
  {
    //change the line
    ofstream fileout;
    fileout.open((datafilename+".tmp").c_str());
    while (getline(filein,line))
    {
        if (line.find(search, 0) == string::npos)
        {
            fileout << line << endl;
        }else{
        fileout << left << setw(50) << search << "\t" <<val <<endl;
        }
    }
    fileout.close();
    rename((datafilename+".tmp").c_str(),datafilename.c_str());
  }
  else
  {
        // cout << "Failed to find " << search << " in "<< datafilename<<"! Adding info! " << endl;
    ofstream fileout;
    fileout.open(datafilename.c_str(),std::ofstream::app);
    fileout << left << setw(50) << search << "\t" <<val <<endl;
    fileout.close();

    }
  filein.close();
}
string getFileName(const string& s) {
   char sep = '/';
   size_t i = s.rfind(sep, s.length());
   if (i != string::npos) {
      return(s.substr(i+1, s.length() - i));
   }
   return("");
}
string getFileNameNoExt(const string& s, string delim) {
   char sep = '/';
   size_t i = s.rfind(sep, s.length());
   string fullname=s;
   if (i != string::npos) {
      fullname=s.substr(i+1, s.length() - i);
   }
   size_t lastindex = fullname.find_last_of(delim);
   string rawname = fullname.substr(0, lastindex);
   return(rawname);
}
string getFileDir(const string& s) {
   char sep = '/';
   size_t lastindex = s.find_last_of(sep);
   string dirname = s.substr(0, lastindex);
   return(dirname);
}
string stripExtension(const string& s, string delim)
{
  string fullname=s;
  size_t lastindex = fullname.find_last_of(delim);
  string rawname = fullname.substr(0, lastindex);
  return(rawname);
}
string rmString(const string& s, string rmstring)
{
  size_t i = s.find(rmstring);
  string es=s;
  if (i != std::string::npos) es.erase(i, rmstring.length());
  return es;
}
bool YesNoRequest(string text="Do you want to continue?")
{
    std::cout << text<<" (Y/n)  ";
    string answer = "";
    std::string input;
    std::getline( std::cin, input );
    if ( !input.empty() ) {
        std::istringstream stream( input );
        stream >> answer;
    }
    return (answer=="Y");
}
string HoursMinutes(double hoursdecimal)
{
  string hours=intToString(int(floor(hoursdecimal)));
  string minutes=intToString(int((hoursdecimal-floor(hoursdecimal))*60));
  if (minutes.length()==1)minutes="0"+minutes;
  string hoursminutes=hours+":"+minutes;
  // cout<<hoursdecimal<<" leads to "<<hoursminutes<<endl;
  return hoursminutes;
}
int Sign(double v)
{
  return v > 0 ? 1 : (v < 0 ? -1 : 0);
}
double Positive(double v)
{
  return Sign(v)>0?v:0;
}
