#include <fstream>
#include <sstream>
#include <iomanip>

#include "TTreePlayer.h"
#include "TPolyLine.h"
#include "TFile.h"
#include "TMath.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TVirtualPad.h"
#include "TF1.h"
#include "TRandom.h"
#include "TGraph.h"
#include "TPaveText.h"
#include "TText.h"
#include "TVector2.h"

#include "RootUtils.h"


using namespace std;


//______________________________________________________________________________
//______________________________________________________________________________
//
//STATISTICS / MATHS MAGIC
//______________________________________________________________________________
//______________________________________________________________________________

double *GetBarlowPexpCorrFactor( double m, double n, double N, int verbose, int replacement){
  // m: events in MC sample
  // n: events in pseudo-dataset
  // N: number of pseudo-experiments

  double rho;
  double CF;
  if (replacement) rho = 1.-exp(-n/ m);
  else             rho = n/m;
  CF = sqrt(1./ N + rho);
  if (N>1)   CF = sqrt(1./ (N-1) + rho); // Bessel correction for unknown population mean
  //if (rho < 0.99) CF /= 1-rho; // correlation reduces spread of the sample
  if (verbose) printf ("BarlowCorrection: m %3.0f, n %3.0f, N %3.0f; rho = %3.5f CF %3.3f noCF %3.3f CF/noCF %3.1f\n", m, n, N, rho, CF,sqrt(1./ N ),CF/sqrt(1./ N ));
  double *value = new double[2];
  value[0] = CF;
  value[1] = rho;
  return value;
}
double round_to_n_digits(double x, int n){
  double scale = pow(10.0, n - ceil(log10(fabs(x))) );
  return TMath::Nint(x * scale) / scale;
}
void round_to_error_n_digits(double x, double e, int n, string &rx, string &re){
  //first round error:
  double _err = round_to_n_digits(e, n);

  //now get order of number and error:
  int ax = int(ceil(log10(fabs(x))));
  int ae = int(ceil(log10(fabs(_err))));

  stringstream sval;
  stringstream serr;
  //check if you need more decimal 0s:
  if (n-ae>0) {

    sval << std::fixed << std::setprecision(n-ae) << round_to_n_digits(x, ax-ae+n);
    serr << std::fixed << std::setprecision(n-ae) << _err; 
  } else {
    sval << round_to_n_digits(x, ax-ae+n);
    serr << _err;
  }

  //return output strings:
  rx = sval.str();
  re = serr.str(); 
}
void check_rounding() {
  string num, err;

  round_to_error_n_digits(128345, 22, 3, num, err);
  cout << "-> " << num << " " << err << endl;

  round_to_error_n_digits(128345, 120, 2, num, err);
  cout << "-> " << num << " " << err << endl;

  round_to_error_n_digits(12800, 5.4, 2, num, err);
  cout << "-> " << num << " " << err << endl;

  round_to_error_n_digits(12800, 0.44, 3, num, err);
  cout << "-> " << num << " " << err << endl;

  round_to_error_n_digits(0.4, 5.4, 2, num, err);
  cout << "-> " << num << " " << err << endl;

  round_to_error_n_digits(0.05, 0.44, 2, num, err);
  cout << "-> " << num << " " << err << endl;

  round_to_error_n_digits(0.05689, 0.44, 2, num, err);
  cout << "-> " << num << " " << err << endl;

  round_to_error_n_digits(0.05689, 0.005, 2, num, err);
  cout << "-> " << num << " " << err << endl;

  round_to_error_n_digits(0.04920, 0.012988, 1, num, err);
  cout << "-> " << num << " " << err << endl;
}
double DiffUnc(double u1, double u2, double rho, double factor){
  double unc = factor*sqrt(pow(u1,2)+pow(u2,2)-2*rho*u1*u2);
  return unc;
}
double ratiounc(double n, double N, double ne, double Ne, double corr, bool v){
  //https://en.wikipedia.org/wiki/Propagation_of_uncertainty#Example_formulas
  double val=-1;
  if (N != 0){
    double nunc=ne/N;
    double Nunc=Ne*n/pow(N,2);
    val=sqrt( pow(nunc,2)+pow(Nunc,2)-2*corr*nunc*Nunc);
  }
  if (v) printf("%f / %f = %f +- %f = %.1f +- %.1f%% (rho = %.2f)\n",n,N,1.*n/N,val,100.*n/N,100*val,corr);
  return val;
}
double ratiounc(int n, int N, double corr, bool v){
  // double val=-1;
  // if (N != 0){
  //  double ne=sqrt(1.*n);
  //  double Ne=sqrt(1.*N);
  //  double nunc=ne/N;
  //  double Nunc=Ne*n/pow(N,2);
  //  val=sqrt( pow(nunc,2)+pow(Nunc,2)-2*corr*nunc*Nunc);
  // }
  // if (v) printf("%f / %f = %f +- %f = %.1f +- %.1f%% (rho = %.2f)\n",n,N,1.*n/N,val,100.*n/N,100*val,corr);
  // return val;
  double ne=sqrt(1.*n);
  double Ne=sqrt(1.*N);
  return ratiounc(n, N, ne, Ne, corr, v);
}
double ratiounc(int n, int N, bool v){
  // double val=-1;
  // if (N != 0) val=sqrt(1.*pow(n,1)/pow(N,2)+pow(n,2)/pow(N,3));
  // if (v) printf("%d / %d = %f +- %f = %.1f +- %.1f%%\n",n,N,1.*n/N,val,100.*n/N,100*val);
 //  return val;
  return ratiounc(n, N, 0, v);
}
double GetPhi(double x, double y){
  double radangle=TMath::ASin(y/sqrt(pow(x,2)+pow(y,2)));
  if (x < 0){
    if (y > 0) radangle = TMath::Pi() - fabs(radangle);
    else       radangle = -TMath::Pi() + fabs(radangle);
  }
  // double degangle=radangle*360/(2*TMath::Pi());
  return radangle;
}
double GetCorrelationFactorError(TH2F* h){
  cout<<"Warning: GetCorrelationFactorError not fully implemented yet!"<<endl;
  double n=h->GetEntries();
  double rho=h->GetCorrelationFactor();
  double rhoe= (1-pow(rho,2))/sqrt(n-1);
  return rhoe;
  //http://de.mathworks.com/matlabcentral/newsreader/view_thread/107045
  // ---> Hotelling, H. (1953). New light on the correlation coefficient and its transforms. Journal of the Royal Statistical Society, Series B, 15(2), 193-232.
  // ---> Ghosh, B. K. (1966). Asymptotic expansions for the moments of the distribution of correlation coefficient. Biometrika, 53(1/2), 258-262.
}
double* GetMinHistoRegion(TH1F *h,double frac,string func, bool verbose){
  //return quantity in smallest region containing frac of all events
  double oldmin=h->GetXaxis()->GetXmin();
  double oldmax=h->GetXaxis()->GetXmax();
  double oldintegral=h->Integral();
  double oldrange=oldmax-oldmin;
  double tmprange(oldrange);
  double finalmin(oldmin);
  double finalmax(oldmax);
  double *value = new double[3];
  for (int i = 0; i < h->GetNbinsX(); ++i)
  {
    for (int j = i+1; j < h->GetNbinsX(); ++j){
      double newmin=h->GetBinCenter(i+1)-h->GetBinWidth(i+1)/2;
      double newmax=h->GetBinCenter(j+1)+h->GetBinWidth(j+1)/2;
      double newrange=newmax-newmin;
      h->GetXaxis()->SetRangeUser(newmin,newmax);
      double newintegral=h->Integral();
      if (newrange>=tmprange){
        break;
      }
      // if (verbose)  printf("Bin %d/%d %.2f - %.2f: %.2f %.2f %.2f %.2f %.2f\n",i,j,newmin,newmax,newrange,tmprange,newintegral,oldintegral,newintegral/oldintegral );
      if (newrange<tmprange && newintegral/oldintegral>=frac){
        tmprange=newrange;
        finalmin=newmin;
        finalmax=newmax;
        if (func=="RMS") value[0]=h->GetRMS();
        if (func=="StdDev") value[0]=h->GetStdDev();
        if (func=="Mean") value[0]=h->GetMean();
        if (func=="MeanError") value[0]=h->GetMean();
        if (func=="") value[0]=-1;
        if (verbose) printf("Bin %d/%d: new smallest region %.2f - %.2f with integral %.2f and %s = %.2f\n",i,j,newmin,newmax,newintegral,func.c_str(),value[0] );
      }
    }
  }
  value[1] = finalmin;
  value[2] = finalmax;
  h->GetXaxis()->SetRangeUser(oldmin,oldmax);
  return value;
}
double IncrMeanCalc(double oldmean,int newn,double newval){
  return (oldmean*(newn-1)+newval)/newn;
}
double IncrSdtDevCalc(double oldsigma,double oldmean,int newn,double newval){
  printf("Doesn't work yet!\n");
  oldsigma=oldsigma;
  oldmean=oldmean;
  newval=newval;
  return 0;
  //as shown on http://math.stackexchange.com/questions/102978/incremental-computation-of-standard-deviation
  if (newn<2) return 0;
  // return sqrt(1.*(newn-2)/(newn-1)*pow(oldsigma,2)+1./newn*pow((newval-oldmean),2)); //comment
  // return sqrt(((newn-2)*pow(oldsigma,2)+(newn-1)*pow(oldmean-IncrMeanCalc(oldmean,newn,newval),2)+pow(newval-IncrMeanCalc(oldmean,newn,newval),2) ) /(newn-1) );

  //as shown on http://mathcentral.uregina.ca/qq/database/qq.09.06/h/murtaza2.html
  // double s=oldsigma;
  // double SSx=(newn-2)*pow(s,2)+1./(newn-1)*pow(Sx,2);
  // double newSSx=SSx+pow(newval,2);
  // double Sx=(newn-1)*oldmean;
  // double newSx=Sx+newval;
  // double newvariance=1./(newn*(newn-1))* ((newn)*newSSx-pow(newSx,2));
  // return newvariance;
}
void RMStest(int nmax){
  double tmpmean(0);
  double tmpRMS(0);
  double tmpStd(0);
  TH1F *h=new TH1F("h","h",100,0,10);
  double valarr[10]={2,4,5,3,4,5,7,2,4,1};
  for (int i = 0; i < nmax; ++i)
  {
    printf("Adding %f\n",valarr[i] );
    double oldmean=tmpmean;
    tmpmean=IncrMeanCalc(oldmean,i+1,valarr[i]);
    tmpRMS=IncrSdtDevCalc(tmpRMS,oldmean,i+1,valarr[i]);
    h->Fill(valarr[i]);
    printf("     Mine    Histogram\n");
    printf("Mean %f   %f\n",tmpmean,h->GetMean() );
    printf("RMS  %f   %f\n",tmpRMS ,h->GetRMS());
    printf("Std  %f   %f\n",tmpStd ,h->GetStdDev());
  }

  printf("Final result:\n");
  printf("     Mine    Histogram    By hand\n");
  printf("Mean %f   %f\n",tmpmean,h->GetMean() );
  printf("RMS  %f   %f\n",tmpRMS ,h->GetRMS());
  printf("Std  %f   %f\n",tmpStd ,h->GetStdDev());

}
double DeltaR(double eta0,double phi0, double eta1, double phi1){
   double deta = eta0-eta1;
   double dphi = TVector2::Phi_mpi_pi(phi0-phi1);
   return TMath::Sqrt( deta*deta+dphi*dphi );
}
double EtaFromTheta(double theta){
  return -TMath::Log(TMath::Tan(theta/2));
}















//______________________________________________________________________________
//______________________________________________________________________________
//
//ROOT MAGIC
//______________________________________________________________________________
//______________________________________________________________________________
const static int NVISIBLECOLORS=15;
static int mycolorvisible[NVISIBLECOLORS]         ={kBlack,62, kViolet,95, 92,kGreen+3 ,kRed,kTeal,kGreen,kMagenta,15,28,46,kBlue,kViolet};
static int mycolorvisiblenoblack[NVISIBLECOLORS-1]={       62, kViolet,95, 92,kGreen+3 ,kRed,kTeal,kGreen,kMagenta,15,28,46,kBlue,kViolet};
const static int NSTYLES=2;
static int mystyles[NSTYLES]={3001,3244};

int* GetColLineFillStyle(int number){
  int color=mycolorvisible[number%NVISIBLECOLORS];
  int linestyle=1+int(number/NVISIBLECOLORS);
  int fillstyle=mystyles[int(number/NVISIBLECOLORS)%NSTYLES];
  int *value = new int[3];
  value[0] = color;
  value[1] = linestyle;
  value[2] = fillstyle;
  return value;
}
int* GetColLineFillStyleNoBlack(int number){
  int color=mycolorvisiblenoblack[number%(NVISIBLECOLORS-1)];
  int linestyle=1+int(number/(NVISIBLECOLORS-1));
  int fillstyle=mystyles[int(number/(NVISIBLECOLORS-1))%NSTYLES];
  int *value = new int[3];
  value[0] = color;
  value[1] = linestyle;
  value[2] = fillstyle;
  return value;
}

TH1F *CutOffBins(TH1F* h,int nlastbins){
  printf("Cutting off the last %d bins of histogram %s with %d bins and integral %f, ranging from %.1f to %.1f!\n",nlastbins,h->GetName(),h->GetNbinsX(),h->Integral(),h->GetXaxis()->GetXmin(),h->GetXaxis()->GetXmax());
  int newnbins=h->GetNbinsX()-nlastbins;
  double newmin=h->GetXaxis()->GetXmin();
  double newmax=h->GetBinCenter(newnbins)+h->GetBinWidth(newnbins)/2;
  TH1F*h_cut=new TH1F(h->GetName(),h->GetTitle(),newnbins,newmin,newmax);
  for (int i = 0; i < newnbins; ++i)
  {
    h_cut->SetBinContent(i+1,h->GetBinContent(i+1));
    h_cut->SetBinError(i+1,h->GetBinError(i+1));
    // printf("Filled bin %d at x= %.2f with %f +- %f\n",i+1,h->GetBinCenter(i+1),h->GetBinContent(i+1),h->GetBinError(i+1) );
  }
  delete h;
  printf("Cutting done! New histogram has %d bins and ranges from %.1f to %.1f\n", h_cut->GetNbinsX(),h_cut->GetXaxis()->GetXmin(),h_cut->GetXaxis()->GetXmax());
  return h_cut;
}
void SavePad(TPad *pad, const char* save_name, int x, int y){
  //Resize the pad to full size
  TCanvas *temp = new TCanvas("temp","temp",x,y);
  temp->cd();
  TPad *clone = (TPad*)pad->Clone();
  clone->SetPad(0,0,1,1);
  clone->Draw();
  temp->Print(save_name);
  delete temp;
}
void SavePad(TVirtualPad *pad, const char* save_name, int x, int y){
  SavePad((TPad*)pad,save_name,x,y);
}
void RemoveNegBins(TH1 *hist){
  int nnegbins=0;
  for (int mb =1; mb<= hist->GetNbinsX(); mb++) {
    double cont=hist->GetBinContent(mb);
    if (cont <0) {
      hist->SetBinContent(mb,0);
      ++nnegbins;
    }
  }
  if (nnegbins) printf("Warning: removed %d negative bins in histogram %s\n",nnegbins,hist->GetName());
}
void RemoveNaNBins(TH1 *hist){
  int NaNbins=0;
  for (int mb =1; mb<= hist->GetNbinsX(); mb++) {
    double cont=hist->GetBinContent(mb);
    if ( cont != cont) {
      hist->SetBinContent(mb,0);
      ++NaNbins;
    }
  }
  if (NaNbins) printf("Warning: removed %d NnN bins in histogram %s\n",NaNbins,hist->GetName());
}
void AbsValBins(TH1F *hist){
  double bcont=0;
  for (int mb =1; mb<= hist->GetNbinsX(); mb++) {
    bcont=hist->GetBinContent(mb);
    if (bcont <0) hist->SetBinContent(mb,-bcont);
  }
}
void AddSymmBinErrFromDiff(TH1F *h_err, TH1F *h_orig, TH1F *h_var_up, TH1F *h_var_dw, bool ExcludeNormUnc) {
  //adds quadratically to the uncertainty histogram h_err the symmetrized error taken from
  //asymmetric up and dw variation histograms

  double int_up   = h_var_up->Integral();
  double int_dw   = h_var_dw->Integral();
  //double integral = h_orig->Integral();

  double bcont     = 0;
  double bcont_up  = 0;
  double bcont_dw  = 0;
  double sym_err   = 0;
  double old_err   = 0;
  double new_err   = 0;
  for (int mb =1; mb<= h_err->GetNbinsX(); mb++) {
    bcont          = h_orig   ->GetBinContent(mb);
    bcont_up       = h_var_up ->GetBinContent(mb);
    bcont_dw       = h_var_dw ->GetBinContent(mb);
    sym_err        = (fabs(bcont-bcont_up) + fabs(bcont-bcont_dw))/2;
    old_err        = h_err    ->GetBinContent(mb);
    if (!ExcludeNormUnc) new_err        = sqrt(pow(sym_err,2)+pow(old_err,2));
    if (ExcludeNormUnc)  new_err        = sqrt(pow(fabs(bcont_up/int_up-bcont_dw/int_dw)/2*bcont,2)+pow(old_err,2));

    h_err          ->SetBinContent(mb,new_err);
    //cout<<mb<<": bcont "<<bcont<<", old_err "<<old_err<<", bcont_up "<<bcont_up<<", bcont_dw "<<bcont_dw<<", new_err "<<h_err->GetBinContent(mb)<<endl;
  }
}
void SquareRootBins(TH1F *hist){
  double bcont=0;
  for (int mb =1; mb<= hist->GetNbinsX(); mb++) {
    bcont=hist->GetBinContent(mb);
    if (bcont > 0) hist->SetBinContent(mb,sqrt(bcont));
  }
}
double Chi2Hist(TH1F *h1, TH1F *h2, bool norm, double xmin, double xmax){
  int badbincounter(0);
  int emptybincounter(0);
  // h2 is considered error free
  double chi2=0;
  if (xmin == xmax) xmin = h1->GetXaxis()->GetXmin();
  if (xmin == xmax) xmax = h1->GetXaxis()->GetXmax();
  double int1 = 1.;
  double int2 = 1.;
  if (norm) int1 = h1->Integral();
  if (norm) int2 = h2->Integral();
  if (h1->GetNbinsX() != h2->GetNbinsX()) return -1;
  for (int mb =1; mb<= h1->GetNbinsX(); mb++) {
    double binx = h1->GetBinCenter(mb);
    double bine1 = h1->GetBinError(mb)/int1;
    if ( binx<xmin || binx>xmax ) continue;
    double binc1 = h1->GetBinContent(mb)/int1;
    double binc2 = h2->GetBinContent(mb)/int2;
    if (bine1 == 0) {
      if (binc1 == 0) ++emptybincounter;
      else{
       cout<<"Warning: Bin "<<mb<<" error = 0 (x="<<binx<<", y="<<binc1<<"). Omitting bin."<<endl; 
        ++badbincounter;
      }
      continue;
    }
    chi2+=pow((binc1-binc2)/bine1,2);
    // printf("bin %d chi2 %.3f h %.3f f %.3f he %.3f\n", mb, chi2, binc1,binc2,bine1);
  }
  if (badbincounter) printf("Warning: omitted %d bins with 0 error\n",badbincounter);
  if (emptybincounter) printf("Warning: omitted %d empty bins\n",emptybincounter);
  return chi2;
}
double Chi2Hist(TH1F *h1, TF1 *f1, bool norm, double xmin, double xmax){
  int badbincounter(0);
  //f1 is considered error free
  if (xmin == xmax) xmin = h1->GetXaxis()->GetXmin();
  if (xmin == xmax) xmax = h1->GetXaxis()->GetXmax();
  double chi2=0;
  double inth = 1.;
  double intf = 1.;
  if (norm) inth = h1->Integral();
  if (norm) intf = f1->Integral(h1->GetXaxis()->GetXmin(),h1->GetXaxis()->GetXmax());
  if (inth == 0 || intf == 0) {
    printf("Error: integral histogram %f function %f\n",inth,intf);
    return 0;
  }
  for (int mb =1; mb<= h1->GetNbinsX(); mb++) {
    double binx = h1->GetBinCenter(mb);
    double bineh = h1->GetBinError(mb)/inth;
    // printf("histo error %f integral %f\n", h1->GetBinError(mb),inth);
    if ( binx<xmin || binx>xmax ) continue;
    double binch = h1->GetBinContent(mb)/inth;
    double bincf = f1->Eval(binx)/intf;
    if (bineh == 0) {
      cout<<"Warning: Bin "<<mb<<" error = 0 (x="<<binx<<", y="<<binch<<"). Omitting bin."<<endl; 
      ++badbincounter;
      continue;
    }
    chi2+=pow((binch-bincf)/bineh,2);
    // printf("bin %d chi2 %.3f h %.3f f %.3f he %.3f\n", mb, chi2, binch,bincf,bineh);
  }
  if (badbincounter) printf("Warning: %d bins with 0 error, probably empty\n",badbincounter);
  // printf("Final chi2 %.2f\n",chi2);
  return chi2;
}
void SmearBins(TH1F *hist, TH1F *h_unc, int seed){
  TRandom *rdm =  new TRandom();
  rdm->SetSeed(seed);
  double bcont=0;
  double berr=0;
  double newcont=0;
  for (int mb =1; mb<= hist->GetNbinsX(); mb++) {
    bcont=hist->GetBinContent(mb);
    berr=h_unc->GetBinContent(mb);
    newcont = rdm->Gaus(bcont,berr);
    hist->SetBinContent(mb,newcont);
  }
  delete rdm;
}
void SmearBins(TH1F *hist, int seed){
  TH1F *h_unc = (TH1F*) hist->Clone();
  SquareRootBins(h_unc);
  SmearBins(hist, h_unc, seed);
}
void PrintMatrix(const TMatrixD& m, string format, string name, int cols_per_sheet){
   // Print the matrix as a table of elements.
   // Based on TMatrixTBase<>::Print, but allowing user to specify name and cols_per_sheet (also option -> format).
   // By default the format "%11.4g" is used to print one element.
   // One can specify an alternative format with eg
   //  format ="%6.2f  "

   if (!m.IsValid()) {
     m.Error("PrintMatrix","%s is invalid",name.c_str());
     return;
   }

   const int ncols  = m.GetNcols();
   const int nrows  = m.GetNrows();
   const int collwb = m.GetColLwb();
   const int rowlwb = m.GetRowLwb();

   if (format.size() == 0) format= "%8.4g ";
   char topbar[1000];
   snprintf(topbar,1000,format.c_str(),123.456789);
   int nch = strlen(topbar)+1;
   if (nch > 18) nch = 18;
   char ftopbar[20];
   for (int i = 0; i < nch; i++) ftopbar[i] = ' ';
   int nk = 1 + int(log10(ncols));
   snprintf(ftopbar+nch/2,20-nch/2,"%s%dd","%",nk);
   int nch2 = strlen(ftopbar);
   for (int i = nch2; i < nch; i++) ftopbar[i] = ' ';
   ftopbar[nch] = '|';
   ftopbar[nch+1] = 0;

   printf("%dx%d %s is as follows",nrows,ncols,name.c_str());

   if (cols_per_sheet <= 0) {
     cols_per_sheet = 5;
     if (nch <= 8) cols_per_sheet =10;
   }
   nk = 5+nch*(cols_per_sheet<ncols ? cols_per_sheet : ncols);
   for (int i = 0; i < nk+1; i++) topbar[i] = '-';
   topbar[nk+1] = 0;
   for (int sheet_counter = 1; sheet_counter <= ncols; sheet_counter += cols_per_sheet) {
      printf("\n\n     | ");
      for (int j = sheet_counter; j < sheet_counter+cols_per_sheet && j <= ncols; j++) printf(ftopbar,j+collwb-1);
      printf("\n%s\n",topbar);
      if (m.GetNoElements() <= 0) continue;
      for (int i = 1; i <= nrows; i++) {
         printf("%4d |",i+rowlwb-1);
         for (int j = sheet_counter; j < sheet_counter+cols_per_sheet && j <= ncols; j++)
            printf(Form(" %s",format.c_str()),m(i+rowlwb-1,j+collwb-1));
         printf("\n");
      }
   }
   printf("\n");
}
TMatrixD* RemoveRowCol(TMatrixD* m, std::vector<int> rows, std::vector<int> cols, bool verbose){
  const int ncols  = m->GetNcols();
  const int nrows  = m->GetNrows();
  // const int collwb = m->GetColLwb();
  // const int rowlwb = m->GetRowLwb();

  if (rows.size() == 0 && cols.size() == 0) {
    printf("RemoveRowCol: no row or column to remove specified!\n");
    return 0;
  } else if (verbose) {
    printf("Removing rows ");
    for (unsigned int i = 0; i < rows.size(); ++i)
    {
      printf("%d ",rows.at(i));
    }
    printf(" and columns ");
    for (unsigned int i = 0; i < cols.size(); ++i)
    {
      printf("%d ",cols.at(i));
    }
    printf("\n");
  }

  int new_nrows=nrows-rows.size();
  int new_ncols=ncols-cols.size();

  //print warning if index appears twice
  for (unsigned int i = 0; i < rows.size(); ++i)
  {
    for (unsigned int j = i+1; j < rows.size(); ++j)
    {
      if (rows.at(i) == rows.at(j)) {
        printf("Warning: row index %d appears more than once! Returnin 0!\n",rows.at(j));
        return 0;
      }
    }
  }
  for (unsigned int i = 0; i < cols.size(); ++i)
  {
    for (unsigned int j = i+1; j < cols.size(); ++j)
    {
      if (cols.at(i) == cols.at(j)) {
        printf("Warning: column index %d appears more than once! Returning 0!\n",cols.at(j));
        return 0;
      }
    }
  }

  TMatrixD m_new(new_nrows,new_ncols);
  int new_i=0;
  for (int i = 0; i < nrows; ++i)
  {
    bool skipthat=false;
    for (unsigned int k = 0; k < rows.size(); ++k)
    {
      if (i == rows.at(k)) skipthat=true;
    }
    if (skipthat) continue;
    int new_j=0;
    for (int j = 0; j < ncols; ++j)
    {
      skipthat=false;
      for (unsigned int k = 0; k < cols.size(); ++k)
      {
        if (j == cols.at(k)) skipthat=true;
      }
      if (skipthat) continue;
      // printf("%d %d %d %d\n",new_i,new_j++,i,j );
      m_new(new_i,new_j++) = (*m)(i,j);
    }
    new_i++;
  }
  m = (TMatrixD*)m_new.Clone();
  // if (verbose) PrintMatrix(*m,"","matrix after removal",30);
  return m;
}
TMatrixD* RemoveRowCol(TMatrixD* m, int row, int col, int verbose){
  std::vector<int> rows;
  std::vector<int> cols;
  rows.push_back(row);
  cols.push_back(col);
  return RemoveRowCol(m,rows,cols,verbose);
}
TH2D *Matrix2TH2D(TMatrixD a){
  TIter next(gDirectory->GetList());
  TH2D *h_del;
  TObject* obj;
  while((obj=(TObject*)next())){
     if(obj->InheritsFrom(TH2D::Class())){
        h_del = (TH2D*)obj;
        if (string(h_del->GetName()).find("hMatrix2TH2D") < 2) h_del->Delete();
     }
  }
  TH2D *h=new TH2D("hMatrix2TH2D","hMatrix2TH2D",a.GetNrows(),0,a.GetNrows(),a.GetNcols(),0,a.GetNcols());
  const double *pData = a.GetMatrixArray();
  int index(0);
  for (int irow = 0; irow < a.GetNrows(); irow++) {
    for (int icol = 0; icol < a.GetNcols(); icol++) {
      h->Fill(irow,icol,pData[index++]);
    }
  }
  return h;
}
TMatrixD TH2D2Matrix(TH2D* h){
  int nrows=h->GetNbinsY();
  int ncols=h->GetNbinsX();
  TMatrixD m(nrows,ncols);
  for (int i = 0; i < nrows; i++) {
    for (int j = 0; j < ncols; j++) {
      m(i,j)=h->GetBinContent(i+1,j+1);
    }
  }
  return m;
}
void ScaleBins(TH1F *hist, double scale){
  //like TH1::Scale() but error goes like scale instead of sqrt(scale)
  for (int mb =1; mb<= hist->GetNbinsX(); mb++) {
    double bcont=hist->GetBinContent(mb);
    double berr=hist->GetBinError(mb);
    hist->SetBinContent(mb,bcont*scale);
    hist->SetBinError(mb,berr*scale);
  }
}
TH1F* FuncRatio(TF1 *f0, TF1* f1,double min, double max, int npoints){
  TH1F *hratio=new TH1F("hratio","hratio",npoints,min,max);
  for (int i = 0; i < hratio->GetNbinsX(); ++i)
  {
    double x=hratio->GetBinCenter(i+1);
    double val0=f0->Eval(x);
    double val1=f1->Eval(x);
    double ratio=val0/val1;
    hratio->SetBinContent(i+1,ratio);
  }
  hratio->SetLineColor(f0->GetLineColor());
  hratio->SetLineWidth(2);
  return hratio;
}
double AverageErrorPerBin(TH1F* h){
  double err(0);
  for (int i = 0; i < h->GetNbinsX(); ++i)
  {
    err+=h->GetBinError(i+1);
  }
  err/=h->GetNbinsX();
  return err;
}
double CumulativeBinResidual(TH1F* h,TH1F* g){
  double diff(0);
  for (int i = 0; i < h->GetNbinsX(); ++i)
  {
    diff+=h->GetBinContent(i+1)-g->GetBinContent(i+1);
  }
  return diff;
}
double GetMaximum(TGraph* gr, bool forplotting){
  double max=-1;
  double xval(0), yval(0);
  for (int i = 0; i < gr->GetN(); ++i)
  {
    gr->GetPoint(i,xval,yval);
    double newmax=yval+(forplotting?gr->GetErrorY(i)*1.5:0);
    if (i == 0 || newmax > max) {
      max=newmax;
    } 
  }
  return max;
}
double GetMinimum(TGraph* gr, bool forplotting){
  double min=-1;
  double xval(0), yval(0);
  for (int i = 0; i < gr->GetN(); ++i)
  {
    gr->GetPoint(i,xval,yval);
    double newmin=yval-(forplotting?gr->GetErrorY(i)*1.5:0);
    if (i == 0 || newmin < min) {
      min=newmin;
    }
  }
  return min;
}
void PrintTree2Txt(string filename, int maxevt){
  TFile *f = (TFile*) TFile::Open(filename.c_str());
  TTree *t = (TTree*) f->Get("nominal");
  TTreePlayer* tp = (TTreePlayer*) t->GetPlayer();
  tp->SetScanRedirect(kTRUE);
  tp->SetScanFileName("output.txt");
  // string varexp = "*"; //all branches
  string varexp = "ssf:eventNumber:runNumber:channel_DL:lep_isTight:lep_trigMatched:lep_type:lep_pt:lep_eta:lep_phi:lep_E:lep_tracksigd0pvunbiased:lep_trackd0pvunbiased:lep_d0sig:el_cl_eta:lep_dR_lj_min:lep_dPhi_lj_min:lep_dEta_lj_min:lep_RealEff:lep_FakeEff";
  string selection = "jet_pt>45 && channel_DL == 3";
  if (maxevt) t->Scan(varexp.c_str(),selection.c_str(),"",maxevt);
  else        t->Scan(varexp.c_str(),selection.c_str());
}
double BinPosError(TH1F *hist,int bin){
  //gives back in what range bins in vicinity have compatible bin content: (up-down)/2
  double cont=hist->GetBinContent(bin);
  double err=hist->GetBinError(bin);
  int nbinhi=bin;
  while (nbinhi<hist->GetNbinsX()){
    ++nbinhi;
    double bincont=hist->GetBinContent(nbinhi);
    double binerr=hist->GetBinError(nbinhi);
    double comperr=sqrt(pow(err,2)+pow(binerr,2));
    // printf("hist: bin %d cont %f +- %f test: bin %d cont %f +- %f\n",bin,cont,err,nbinhi,bincont,binerr);
    if ((bincont < cont && bincont+comperr < cont) || (bincont > cont && bincont-comperr > cont)){
      // printf("break\n");
      break;
    }
  }
  int nbinlo=bin;
  while (nbinlo>0){
    --nbinlo;
    double bincont=hist->GetBinContent(nbinlo);
    double binerr=hist->GetBinError(nbinlo);
    double comperr=sqrt(pow(err,2)+pow(binerr,2));
    // printf("hist: bin %d cont %f +- %f test: bin %d cont %f +- %f\n",bin,cont,err,nbinlo,bincont,binerr);
    if ((bincont < cont && bincont+comperr < cont) || (bincont > cont && bincont-comperr > cont)){
      // printf("break\n");
      break;
    }
  }
  double result=(hist->GetBinCenter(nbinhi)-hist->GetBinCenter(nbinlo))/2;
  // printf("returning %f\n",result);
  return result;
}
double FWHM(TH1F* hist){
  int bin1 = hist->FindFirstBinAbove(hist->GetMaximum()/2);
  int bin2 = hist->FindLastBinAbove(hist->GetMaximum()/2);
  double fwhm = hist->GetBinCenter(bin2) - hist->GetBinCenter(bin1);
  // printf("bin1 %d bin2 %d max %f max/2 %f fwhm %f\n", bin1,bin2,hist->GetMaximum(),hist->GetMaximum()/2,fwhm);
  return fwhm;
}
double FWHMError(TH1F* hist){
  int bin1 = hist->FindFirstBinAbove(hist->GetMaximum()/2);
  int bin2 = hist->FindLastBinAbove(hist->GetMaximum()/2);
  int binm = hist->GetMaximumBin();
  double fwhm = hist->GetBinCenter(bin2) - hist->GetBinCenter(bin1);
  if (hist->GetEntries() < 1000) return fwhm;
  // printf("bin1 %d binm %d bin2 %d\n",bin1,binm,bin2);
  double fwhmerr = sqrt(pow(BinPosError(hist,bin1),2) + pow(BinPosError(hist,binm),2) + pow(BinPosError(hist,bin2),2));
  return fwhmerr;
}
void PrintBins(TH1F *hist){
  printf("Histo integral %f mean %f nbins %d\n",hist->Integral(),hist->GetMean(),hist->GetNbinsX());
  for (int mb =1; mb<= hist->GetNbinsX(); mb++) {
    double bcont=hist->GetBinContent(mb);
    double berr=hist->GetBinError(mb);
    printf("bin %d: %f +- %f\n",mb,bcont,berr);
  }
}
TH2F* Divide2d(TH2F *h, TH2F* g){
  TH2F *ratio2d=(TH2F*) h->Clone();
  for (int mbx =1; mbx<= h->GetNbinsX(); mbx++) {
    for (int mby =1; mby<= h->GetNbinsY(); mby++) {
      ratio2d->SetBinContent(mbx,mby,1.);
      ratio2d->SetBinError(mbx,mby,1.);
      double binc_h=h->GetBinContent(mbx,mby);
      double binc_g=g->GetBinContent(mbx,mby);
      double bine_h=h->GetBinError(mbx,mby);
      double bine_g=g->GetBinError(mbx,mby);
      // if (fabs(binc_h) > 3*bine_h && fabs(binc_g) > 3*bine_g){ //both contents significant
      // if (fabs(binc_h) > 3*bine_h && fabs(binc_g) > 3*bine_g && fabs(binc_h-binc_g) > sqrt(pow(bine_h,2)+pow(bine_g,2))){ //both contents significant and difference significant
      // if (fabs(binc_h) > 1*bine_h && fabs(binc_g) > 1*bine_g && fabs(binc_h-binc_g) > sqrt(pow(bine_h,2)+pow(bine_g,2))){ //both contents significant and difference significant
      if (fabs(binc_h) > 0 && fabs(binc_g) > 0){
        ratio2d->SetBinContent(mbx,mby,binc_h/binc_g);
        double binerror=sqrt(pow(bine_h/binc_g,2)+pow(bine_g*binc_h/pow(binc_g,2),2));
        ratio2d->SetBinError(mbx,mby,binerror);
      }
    }
  }
  ratio2d->GetZaxis()->SetTitle("Ratio");
  return ratio2d;
}
void SetLargeErrorBinsToOne(TH2F* hist){
  //useful for ratio plots
  for (int mbx =1; mbx<= hist->GetNbinsX(); mbx++) {
    for (int mby =1; mby<= hist->GetNbinsY(); mby++) {
      double binc=hist->GetBinContent(mbx,mby);
      double bine=hist->GetBinError(mbx,mby);
      if (bine > fabs(binc-1)) hist->SetBinContent(mbx,mby,1.);
      if (bine > fabs(binc-1)) hist->SetBinError(mbx,mby,0.);
    }
  }
}
void SetMaxOfHistos(TH1* h,TH1* h1){
  //formerly called setmax
  if (!h) return;
  // Add 10% to match behaviour of ROOT's automatic scaling
  double maxval= h1 ? h1->GetMaximum() : -DBL_MAX;
  h->SetMinimum (0.0);
  if (maxval > h->GetMaximum()) h->SetMaximum (1.1*maxval);
}
void DeleteObjectFromFile(string filename,string objectname){
  TFile* file = new TFile(filename.c_str(), "update");
  gDirectory->Delete(objectname.c_str());
  file->Close(); 
}
TH2D *RotateNinetyDegrees(TH2D* h){
  int dim=h->GetNbinsX();
  int dimy=h->GetNbinsY();
  if (dim != dimy){
    printf("Rotation non implemented for non sqare histos! Returning input histo.\n");
    return h;
  }
  TH2D* hRot = (TH2D*) h->Clone();
  hRot->SetXTitle(h->GetXaxis()->GetTitle());
  hRot->SetYTitle(h->GetYaxis()->GetTitle());
  for (int i = 0; i < dim; ++i)
  {
    hRot->GetXaxis()->SetBinLabel(i+1,h->GetYaxis()->GetBinLabel(i+1));
    hRot->GetYaxis()->SetBinLabel(i+1,h->GetXaxis()->GetBinLabel(i+1));
    for (int j = 0; j < dim; ++j)
    {
      hRot->SetBinContent(j+1,dim-i,h->GetBinContent(i+1,j+1));
    }
  }
  return hRot;
}
TH1F *RandomizeHisto(TH1F * h,int events){
  TH1F* hRan=(TH1F*)h->Clone();
  hRan->Reset();
  if (events == -1) events=int(h->Integral());
  hRan->FillRandom(h,events);
  return hRan;
}
void DrawWhiteBin(int ibinx, int ibiny, TH2D* h){
  double x_edge_low,x_edge_high,y_edge_low,y_edge_high;
  if (h == 0){
    x_edge_low=double(ibinx);
    x_edge_high=double(ibinx+1);
    y_edge_low=double(ibiny);
    y_edge_high=double(ibiny+1);
  }else{
    x_edge_low=h->GetXaxis()->GetBinLowEdge(ibinx+1);
    x_edge_high=h->GetXaxis()->GetBinLowEdge(ibinx+2);
    y_edge_low=h->GetYaxis()->GetBinLowEdge(ibiny+1);
    y_edge_high=h->GetYaxis()->GetBinLowEdge(ibiny+2);
  }
  double x[4] = {x_edge_low,x_edge_high,x_edge_high,x_edge_low};
  double y[4] = {y_edge_low,y_edge_low,y_edge_high,y_edge_high};
  TPolyLine *pline = new TPolyLine(4,x,y);
  pline->SetFillColor(kWhite);
  pline->SetLineColor(kWhite);
  pline->Draw("f");
}
void FILL(TH1F *hist, double x, double w){
  // Fill taking care of overflows and underflows
  // Those are reported in the first and last bin respectively
  double xmin= hist->GetXaxis()->GetXmin();
  double xmax= hist->GetXaxis()->GetXmax();
  double binwh= hist->GetBinWidth(1)/2;
  if (x>=xmin && x<=xmax) hist->Fill(x, w);
  if (x<xmin) hist->Fill(xmin+binwh, w);
  if (x>xmax) hist->Fill(xmax-binwh, w);
}
TH1F *GetRatio(TH1F *data, TH1F *mcexp, int col, double min, double max, bool normalized, double corrsinglebins){

  //TH1F *ratio = new TH1F ("ratio", "ratio", data->GetNbinsX(), data->GetXaxis()->GetXmin(), data->GetXaxis()->GetXmax() );
  TH1F *ratio = (TH1F*) data->Clone("ratio");
  ratio->Sumw2();

  if (normalized) ratio->Divide(data, mcexp, 1/data->Integral(), 1/mcexp->Integral());
  else ratio->Divide(data, mcexp);
  ratio->SetLabelSize(0.18, "X");
  ratio->SetLabelSize(0.16, "Y");
  ratio->SetTitleSize(0.18,"Y");
  ratio->SetTitleOffset(0.3,"Y");
  ratio->SetTitleSize(0.18,"X");
  ratio->SetTitleOffset(1.2,"X");
  //ratio->SetTitleOffset(0.5,"X");
  ratio->SetXTitle(data->GetXaxis()->GetTitle());
  ratio->SetYTitle("data/MC");
  ratio->SetName(Form("%s/%s",data->GetName(),mcexp->GetName()));
  if (col != -1) ratio->SetFillColor(col);
  else ratio->SetFillColor(data->GetFillColor());
  if (col != -1) ratio->SetLineColor(col);
  else ratio->SetLineColor(data->GetLineColor());
  ratio->SetLineWidth(2);
  ratio->SetMarkerStyle(20);
  ratio->SetMarkerColor(col);
  ratio->GetYaxis()->SetNdivisions(5);
  if (max != -1) ratio->SetMaximum(max);
  if (min != -1) ratio->SetMinimum(min);

  if (corrsinglebins){
    for (int i = 0; i < data->GetNbinsX(); ++i)
    {
      double n =data->GetBinContent(i+1) / (normalized?data->Integral():1);
      double ne=data->GetBinError(i+1) / (normalized?data->Integral():1);
      double N =mcexp->GetBinContent(i+1) / (normalized?mcexp->Integral():1);
      double Ne=mcexp->GetBinError(i+1) / (normalized?mcexp->Integral():1);
      double bine=ratiounc(n, N, ne, Ne, corrsinglebins);
      ratio->SetBinError(i+1,bine);
    }
  }
 
  int ndiv = TMath::Min(int(data->GetXaxis()->GetNdivisions()), int(mcexp->GetXaxis()->GetNdivisions()));
  ratio->GetXaxis()->SetNdivisions(ndiv);
    
  return ratio;
}
double GC_up(double data) {
//Used in WZ observation
 if (data == 0 ) return 0;
 return 0.5*TMath::ChisquareQuantile(1.-0.1586555,2.*(data+1))-data;
}
double GC_down(double data) {
//Used in WZ observation
 if (data == 0 ) return 0;
 return data-0.5*TMath::ChisquareQuantile(0.1586555,2.*data);
}
TGraphAsymmErrors* poissonize(TH1 *h) {
  vector<int> points_to_remove;
  TGraphAsymmErrors* gr= new TGraphAsymmErrors(h);
  for (unsigned int i=0; i< fabs(double(gr->GetN())); i++) {
   double content = (gr->GetY())[i];
   gr->SetPointError(i,0,0,GC_down(content),GC_up(content));
   if(content==0){
    gr->RemovePoint(i);
    i--;
   }
  }
  
  return gr;
}
void CheckHistoCompatibility(TH1F *data, TH1F *mcexp, double textx, bool displaychisq, float xmin, float xmax, int nfuncpar, TH2D* h_invcov){
  // Perform 
  // 1) Kolmogorov test for histo compatibility,
  // 2) Chi2 test, assuming one of the histo to be error free: assume mcexp has no error.
  // 3) run test
  bool performChisqtest=true;
  bool performRuntest=false;
  bool performKStest=false;
  if (!performRuntest && !performKStest && !performChisqtest) return;
  bool POISSONIZE=false; // true=make use of asym err

  // make poisson errors for data
  TGraphAsymmErrors* gr = 0;
  if (POISSONIZE) gr =  new TGraphAsymmErrors(data);

  int minbin = 1;
  int maxbin = data -> GetNbinsX();

  if (xmin != xmax) {
    cout<<"Info: Histo compatibility tests with restricted range use full outer bins including xmax and xmin."<<endl;
    minbin = data->FindBin(xmin);
    maxbin = data->FindBin(xmax);
  }


  //calculate chi2
  double chi2 =0;
  int ndof =0;
  double nbins = maxbin - minbin + 1;
  for (int idx = minbin; idx <= maxbin; idx++) {
    double b_s = data->GetBinContent(idx);
    double be_s = data->GetBinError(idx);
    double b_b = mcexp->GetBinContent(idx);
    double be_b = mcexp->GetBinError(idx);

    if (!h_invcov){
      if (POISSONIZE) {
        // get the pos/ned error for the asym graph
        double content = (gr->GetY())[idx-1];    
      
        if (b_s>=b_b) be_s=GC_down(content);
        else          be_s=GC_up(content);
      }

      
      if (b_s > 0 && b_b>0) {
        double err = sqrt(be_s*be_s + be_b*be_b);
        chi2 += pow( (b_s-b_b)/err , 2);
        ndof ++;
      }
      // cout<<"Bin "<<idx<<" xval "<<data->GetBinCenter(idx)<<", data: "<<b_s<<" +- "<<be_s<<", exp: "<<b_b<<" +- "<<be_b<<", chi2: "<<chi2<<endl;
    }else{
      for (int jdx = minbin; jdx <= maxbin; jdx++)
      {
        double jb_s = data->GetBinContent(jdx);
        double jb_b = mcexp->GetBinContent(jdx);
        double invcov=h_invcov->GetBinContent(idx,jdx);
        double f= (b_s-b_b)*
                  invcov*
                  (jb_s-jb_b);
        chi2+=f;
        // printf("Bin %d/%d: ( %f - %f ) * %e * ( %f - %f ) = %f, chi2 = %f\n",idx,jdx,b_s,b_b,invcov,jb_s,jb_b,f,chi2);
      }
      ndof ++;
    }
  }
  ndof -= nfuncpar;
  double prob = TMath::Prob(chi2, ndof);



  // run-test: from R.J. Barlow "Statistics" pp 153-154
  float nA = 0; // n measurements above 
  float nB = 0; // n measurements below
  float Ntot = 0;
  int run = 0;
  double diff = 0;
  double exprun = 0;
  double varrun = 0;
  if (performRuntest){
    if (data->Integral() ==0 || mcexp->Integral()==0 || nbins<3){
      //      data->Integral()==mcexp->Integral()) { // likely this is the same histo
      cout << "skipping run-test" << endl;
      return;
    }
    for (int idx = minbin; idx <= maxbin; idx++) {
      
      double b_s = data->GetBinContent(idx)/data->Integral();   
      double b_b = mcexp->GetBinContent(idx)/mcexp->Integral();

      if ((b_s <= 0) || (b_b <= 0)) continue;
      if (b_s>=b_b) nA++;
      else nB++;

      if (b_s-b_b !=0) {
        if ((b_s-b_b)/fabs(b_s-b_b) != diff) {
          run++;
          diff = (b_s-b_b)/fabs(b_s-b_b);
        }
      }
      
    }
    Ntot = nA+nB;
    exprun = 1 + (2*nA*nB)/Ntot;
    varrun = (2*nA*nB*(2*nA*nB - Ntot))/(Ntot*Ntot*(Ntot-1));
  }

  char result[200];
  TPaveText *label;
  label = new TPaveText(textx,0.96,0.95,0.98,"NDC");
  label->SetFillColor(kNone);

  if (performRuntest && performKStest && performChisqtest) sprintf(result, "#chi2/dof = %1.2f/%d; Prob(#chi2|KS): (%1.2f | %1.2f); Run-test: %1.2f #sigma ", chi2, ndof, prob,data->KolmogorovTest(mcexp,""), fabs(float(run - exprun)/varrun ) );
  else if (performRuntest && performChisqtest) sprintf(result, "#chi2/dof = %1.2f/%d; Prob(#chi2): (%1.2f); Run-test: %1.2f #sigma ", chi2, ndof, prob, fabs(float(run - exprun)/varrun ) );
  else if (performKStest && performChisqtest) sprintf(result, "#chi2/dof = %1.2f/%d; Prob(#chi2|KS): (%1.2f | %1.2f);", chi2, ndof, prob,data->KolmogorovTest(mcexp,""));
  else if (performKStest && performRuntest) sprintf(result, "Prob(KS): %1.2f; Run-test: %1.2f #sigma ", data->KolmogorovTest(mcexp,""), fabs(float(run - exprun)/varrun ) );
  else if (performChisqtest) sprintf(result, "#chi2/dof = %1.2f/%d; Prob(#chi2) = %1.2f;", chi2, ndof, prob);
  else if (performKStest) sprintf(result, "Prob(KS) = (%1.2f)", data->KolmogorovTest(mcexp,""));
  else if (performRuntest) sprintf(result, "Run-test: %1.2f #sigma ",fabs(float(run - exprun)/varrun ) );
  else sprintf(result, "CheckHistoCompatibility: It's impossible to read this.");
  //sprintf(result, "#chi2/dof = %1.1f/%d; Prob(#chi2|KS): (%1.2f | %1.2f); run-test: %1.2f #sigma ", chi2, ndof, prob,data->KolmogorovTest(mcexp,"X"), fabs(float(run - exprun)/varrun ) );
  TText *t1 = label->AddText(result);t1->SetTextSize(0.033); t1->SetTextColor(kBlack);t1->SetTextAlign(11);
  if (displaychisq) label->Draw("same");        

  cout << result << endl;

  delete gr;

}
void CheckHistoCompatibility(TH1F *data, TF1 *func, double textx, bool displaychisq, float xmin, float xmax){
  TH1F *h_fit = (TH1F*) data->Clone();
  TF1 *f_fit  = (TF1*) func->Clone();
  h_fit -> Reset();
  for (int ibin = 1;ibin < h_fit -> GetNbinsX();ibin++){
    h_fit -> SetBinContent(ibin, f_fit->Eval(h_fit->GetBinCenter(ibin)));
    h_fit -> SetBinError(ibin, 0);
  }
  int nfuncpar = f_fit->GetNumberFreeParameters();
  CheckHistoCompatibility(data, h_fit, textx, displaychisq, xmin, xmax, nfuncpar);
  delete h_fit;
  delete f_fit;
}
TH1F* ProjectionY(TH1F *h,bool ExcludeEmptyBins, double max){
  TIter next(gDirectory->GetList());
  TH1F *h_del;
  TObject* obj;
  while((obj=(TObject*)next())){
     if(obj->InheritsFrom(TH1F::Class())){
        h_del = (TH1F*)obj;
        if (string(h_del->GetName()).find("h_proj") < 2) h_del->Delete();
     }
  }
  int newnbins=100;
  double newmax=h->GetBinContent(h->GetMaximumBin());
  if (max) newmax=max;
  double newmin=newmax;
  for (int i = 0; i < h->GetNbinsX(); ++i)
  {
    double cont=h->GetBinContent(i+1);
    if (cont != 0 && cont < newmin) newmin=cont;
  }
  TH1F *h_proj=new TH1F("h_proj",Form("Projection of %s;Bin content;Number of bins",h->GetName()),newnbins,newmin,newmax+(newmax-newmin)*0.01);
  for (int i = 0; i < h->GetNbinsX(); ++i)
  {
    double cont=h->GetBinContent(i+1);
    if (ExcludeEmptyBins && cont==0) continue;
    h_proj->Fill(cont);
  }
  return h_proj;
}
double* GetAverageBinContent(TH1F *h){
  // printf("Histogram %s has %.0f entries\n",h->GetName(),h->GetEntries() );
  bool ExcludeEmptyBins=true;
  TH1F*h_proj=(TH1F*) ProjectionY(h,ExcludeEmptyBins);
  double *value = new double[2];
  value[0] = h_proj->GetMean();
  value[1] = h_proj->GetMeanError();
  value[2] = h_proj->GetRMS();
  return value;
}
double GraphIntegral(TGraph* g){
  double xmin = TMath::MinElement(g->GetN(),g->GetX());
  double xmax = TMath::MaxElement(g->GetN(),g->GetX());
  TF1 f1("f",[&](double *x, double *){ return g->Eval(x[0]); },xmin,xmax,0);

#ifdef _WIN32
  return f1.Integral(xmin, xmax, 0, 0, 1e-6); //Root5
#else
  return f1.Integral(xmin,xmax,1e-6); //Root6
#endif

}
void DrawTH2F(TH2F* h, string drawoption="colz"){
  gPad->SetRightMargin(0.17);
  gPad->SetLeftMargin(0.15);
  gPad->SetBottomMargin(0.15);
  h->GetXaxis()->SetLabelOffset(0.01);
  h->GetXaxis()->SetTitleOffset(1);
  h->GetYaxis()->SetTitleOffset(1.1);
  h->GetXaxis()->SetLabelSize(0.06);
  h->GetYaxis()->SetLabelSize(0.06);
  h->GetXaxis()->SetTitleSize(0.06);
  h->GetYaxis()->SetTitleSize(0.06);
  h->GetZaxis()->SetTitleSize(0.06);
  h->Draw(drawoption.c_str());
}
void RotatePoly(TPolyLine* pline,double xcenter,double ycenter,double angle){
  ///Positibe angle: rotation
  ///Negative angle: invert x, then rotate
  int invert=angle<0?-1:1;
  angle=angle/360*2*TMath::Pi()*invert;
  int n=pline->GetN();
  double* oldx=pline->GetX();
  double* oldy=pline->GetY();
  const int maxpolynumber=10000;
  double newx[maxpolynumber];
  double newy[maxpolynumber];
  for (int i = 0; i < n; ++i)
  {
    TVector2 vec;
    vec.Set(invert*(oldx[i]-xcenter),oldy[i]-ycenter);
    TVector2 vecnew=vec.Rotate(angle);
    newx[i]=vecnew.X()+xcenter;
    newy[i]=vecnew.Y()+ycenter;
  }
  pline->SetPolyLine(n, newx, newy);
}
double* GetPolyCenterOfGravity(TPolyLine* pline){
  int n=pline->GetN();
  double* x=pline->GetX();
  double* y=pline->GetY();
  double x_center(0),y_center(0);
  for (int i = 0; i < n; ++i)
  {
    x_center+=x[i]/n;
    y_center+=y[i]/n;
  }
  double *value = new double[2];
  value[0] = x_center;
  value[1] = y_center;
  return value;
}
double FindFirstX(TSpline* spline,double yval, double xmin, double xmax){
  //returns smallest x value with a y value - yval between xmin and xmax
  //if none is found, return xmax
  int niter=2;
  int nsteps=100;
  double iter_xmin=xmin;
  double iter_xmax=xmax;
  double prev_val=spline->Eval(xmin);
  double curr_val=spline->Eval(xmin);
  double thisx=xmin;
  double prevx=xmin;
  bool foundit=false;
  for (int i_it = 0; i_it < niter; ++i_it)
  {
    for (int i_st = 0; i_st < nsteps; ++i_st)
    {
      thisx=iter_xmin+(iter_xmax-iter_xmin)/(nsteps-1)*i_st;
      curr_val=spline->Eval(thisx);
      // printf("Iter %d step %d prevval %.2f currval %.2f %.2f <= %.2f --> %.2f <-- < %.2f\n",i_it,i_st,prev_val,curr_val,iter_xmin,prevx,thisx ,iter_xmax);
      if ( (yval >= prev_val && yval < curr_val) || (yval <= prev_val && yval > curr_val) ) {
        iter_xmin=prevx;
        iter_xmax=thisx;
        foundit=true;
        break;
      }
      prev_val=curr_val;
      prevx=thisx;
    }
  }
  return foundit?(prevx+thisx)/2:std::numeric_limits<double>::quiet_NaN();
}

TPaveText* MakeLabel(double x1, double y1, double x2, double y2, double size, double align) {
  TPaveText* label = new TPaveText(x1,y1,x2,y2,"NDC");
  label->SetFillColor(kNone);
  label->SetLineColor(kNone);
  label->SetTextColor(kBlack);
  label->SetTextSize(float(size));
  label->SetTextAlign(float(align));
  return label;
}
