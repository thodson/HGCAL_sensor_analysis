#!/bin/bash

PICFORMAT=pdf
GEOFILE="geo/hex_positions_HPK_128ch_6inch_Fermilab.txt"
SPECIALCELLS="87,88,89,90,98,99,100,101,111,112,113,123:93,94,95,104,105,106,107,116,117,126:10,18,19,20,28,29,30,31,41,42,43,44,52,53,54,55:13,23,24,34,35,36,37,47,48,49,58,59,60,61"
SPECIALCELLNAMES="d=20#mum:d=40#mum:d=60#mum:d=80#mum"

FILENAMES=(
	HGC1003
	HGC1004
	# HGC1005
	# HGC1006
	# HGC1007
	# HGC1008
	# HGC1009
	# HGC1010
	# HGC1011
	# HGC1012
	# HGC1013
	# HGC1014
	# HGC1015
	# HGC1016
	# HGC1017
	# HGC1018
	# HGC1019
	# HGC1020
	# HGC1001 #have IV values only up to 300 V
	# HGC1002 #have IV values only up to 300 V
	)
ALLFILES=""
ALLDATACVFILES=""
ALLDATAIVFILES=""
for i in "${FILENAMES[@]}"
do
	echo "Processing/adding file $i..."
	ALLFILES=$ALLFILES$i.txt:
	ALLDATACVFILES=$ALLDATACVFILES../Data/${i}_CV.txt:
	ALLDATAIVFILES=$ALLDATAIVFILES../Data/${i}_IV.txt:
	# ./HexPlot -g $GEOFILE -i ../Data/${i}_IV.txt -of $PICFORMAT -od ../Results -IV -volt 1000 -p HEX -z 0:6
	# ./HexPlot -g $GEOFILE -i ../Data/${i}_IV.txt -of $PICFORMAT -od ../Results -IV -volt 1000 -p CHAN -z 0:10 #-sc $SPECIALCELLS -scn $SPECIALCELLNAMES
	# ./HexPlot -g $GEOFILE -i ../Data/${i}_IV.txt -of $PICFORMAT -od ../Results -IV -c 21 -p SEL
	# ./HexPlot -g $GEOFILE -i ../Data/${i}_IV.txt -of $PICFORMAT -od ../Results -IV -p FULL

	# ./HexPlot -g $GEOFILE -i ../Data/${i}_CV.txt -of $PICFORMAT -od ../Results -CV -p HEXDEP -z 150:200 -zu 0:8
	# ./HexPlot -g $GEOFILE -i ../Data/${i}_CV.txt -of $PICFORMAT -od ../Results -CV -volt 300 -p HEX -z 45:60 -zu 0:0.5
	# ./HexPlot -g $GEOFILE -i ../Data/${i}_CV.txt -of $PICFORMAT -od ../Results -CV -volt 300 -p CHANDEP -z 100:300 #-sc $SPECIALCELLS -scn $SPECIALCELLNAMES
	# ./HexPlot -g $GEOFILE -i ../Data/${i}_CV.txt -of $PICFORMAT -od ../Results -CV -volt 300 -p CHAN -z 10:200 #-sc $SPECIALCELLS -scn $SPECIALCELLNAMES
	# ./HexPlot -g $GEOFILE -i ../Data/${i}_CV.txt -of $PICFORMAT -od ../Results -CV -c 21 -p SEL
	# ./HexPlot -g $GEOFILE -i ../Data/${i}_CV.txt -of $PICFORMAT -od ../Results -CV -p FULL
done


# ./HexPlot -g $GEOFILE -od ../Results -p MERGE -mf $ALLFILES -o ../Results/sensors_allvalues.txt

# ./HexPlot -g $GEOFILE -i ${ALLDATAIVFILES} -of $PICFORMAT -od ../Results -IV -volt 1000 -p HEX -z 0:6
# ./HexPlot -g $GEOFILE -i ${ALLDATAIVFILES} -of $PICFORMAT -od ../Results -IV -volt 1000 -p HEX -z 0:6 -sc $SPECIALCELLS
# ./HexPlot -g $GEOFILE -i ${ALLDATAIVFILES} -of $PICFORMAT -od ../Results -IV -volt 1000 -p CHAN -z 0:6 -sc $SPECIALCELLS -scn $SPECIALCELLNAMES
# ./HexPlot -g $GEOFILE -i ${ALLDATAIVFILES} -of $PICFORMAT -od ../Results -IV -c 21 -p SEL
# ./HexPlot -g $GEOFILE -i ${ALLDATAIVFILES} -of $PICFORMAT -od ../Results -IV -c 39 -p SEL
# ./HexPlot -g $GEOFILE -i ${ALLDATAIVFILES} -of $PICFORMAT -od ../Results -IV -c 45 -p SEL
# ./HexPlot -g $GEOFILE -i ${ALLDATAIVFILES} -of $PICFORMAT -od ../Results -IV -c 136 -p SEL

# ./HexPlot -g $GEOFILE -i ${ALLDATACVFILES} -of $PICFORMAT -od ../Results -CV -p HEXDEP -z 150:200 -zu 0:8
# ./HexPlot -g $GEOFILE -i ${ALLDATACVFILES} -of $PICFORMAT -od ../Results -CV -volt 300 -p HEX -z 45:60 -zu 0:0.5
# ./HexPlot -g $GEOFILE -i ${ALLDATACVFILES} -of $PICFORMAT -od ../Results -CV -volt 300 -p HEX -z 45:60 -zu 0:0.5 -sc $SPECIALCELLS
# ./HexPlot -g $GEOFILE -i ${ALLDATACVFILES} -of $PICFORMAT -od ../Results -CV -volt 300 -p CHANDEP -z 100:300 -sc $SPECIALCELLS -scn $SPECIALCELLNAMES
# ./HexPlot -g $GEOFILE -i ${ALLDATACVFILES} -of $PICFORMAT -od ../Results -CV -volt 300 -p CHAN -z 46:58 -sc $SPECIALCELLS -scn $SPECIALCELLNAMES
# ./HexPlot -g $GEOFILE -i ${ALLDATACVFILES} -of $PICFORMAT -od ../Results -CV -volt 300 -p CHAN -z 10:200 -sc $SPECIALCELLS -scn $SPECIALCELLNAMES
# ./HexPlot -g $GEOFILE -i ${ALLDATACVFILES} -of $PICFORMAT -od ../Results -CV -c 21 -p SEL
# ./HexPlot -g $GEOFILE -i ${ALLDATACVFILES} -of $PICFORMAT -od ../Results -CV -c 39 -p SEL
# ./HexPlot -g $GEOFILE -i ${ALLDATACVFILES} -of $PICFORMAT -od ../Results -CV -c 45 -p SEL
# ./HexPlot -g $GEOFILE -i ${ALLDATACVFILES} -of $PICFORMAT -od ../Results -CV -c 136 -p SEL
# ./HexPlot -g $GEOFILE -i ${ALLDATACVFILES} -of $PICFORMAT -od ../Results -CV -c 21 -p FULL


# ./HexPlot -g $GEOFILE -i examples/test_data.txt -g geo/hex_positions_6inch_128.txt -od examples -of png -IV -volt 1000 -p HEX -pn 0 -sc $SPECIALCELLS

# NEWFILE='../Plots/sensors_allvalues.txt'
# mv $NEWFILE $TRASH
# for i in "${FILENAMES[@]}"
# do
# 	echo "Processing $i..."
# 	cat ../Plots/$i.txt >> $NEWFILE
# 	echo "" >> $NEWFILE
# 	echo "" >> $NEWFILE
# done
# echo "Added all sensor values in $NEWFILE"
