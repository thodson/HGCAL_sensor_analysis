#include "TROOT.h"
#include "TF1.h"
#include "TSpline.h"
#include "TArrow.h"
#include "CppUtils.h"
#include "FitUtils.h"

#include "HexMap.h"

const int MAXSELECTORS=500;
const double DEFAULT_SELECTOR=-9999999;
const double ALLCHAN_SELECTOR=-10000000;


TCanvas *c_depvolt_fit[MAXCELLS*MAXSENSORS]={0};
int depvolt_plots=0;
int currentcanv=0;
















inline static void PlotThisPad(TCanvas* c_pad,string sensorname, bool isfinal){
	int npadx=4;
	int npady=4;
	int npad=npadx*npady;
	int currentpad=depvolt_plots-currentcanv*npad+1;
	if (currentpad == 1) {
		c_depvolt_fit[currentcanv]=new TCanvas(Form("c_%s_details[%d-%d]",sensorname.c_str(),currentcanv*npad+1,(currentcanv+1)*npad),Form("c_%s_details[%d-%d]",sensorname.c_str(),currentcanv*npad+1,(currentcanv+1)*npad),0,0,1200,1000);
		c_depvolt_fit[currentcanv]->Divide(npadx,npady);
	}

	// printf("Drawing canvas %s in canvas %s into pad %d\n",c_pad->GetName(),c_depvolt_fit[currentcanv]->GetName(),currentpad );

	c_depvolt_fit[currentcanv]->cd(currentpad);
	gPad->SetRightMargin(0);
	gPad->SetLeftMargin(0);
	gPad->SetTopMargin(0);
	gPad->SetBottomMargin(0);

	c_pad->DrawClonePad();

	++depvolt_plots;
	if (currentpad == npad || isfinal) {
		c_depvolt_fit[currentcanv]->Print(Form("%s_details_%d_%d.pdf",string(OUTPUTDIR + (OUTPUTDIR.empty() ? "" : "/")+sensorname).c_str(),currentcanv*npad+1,(currentcanv+1)*npad));
		++currentcanv;
	}
	if (isfinal){
		currentcanv=0;
		depvolt_plots=0;
	}
}














inline static TObject** LoadValues(string inputfile,string inputformat,string mapfile,double yscale,int verbose,double &voltage,int singlechan, string measunc, int appearance){
	if (verbose) printf("Executing LoadValues...\n");

	if (inputfile==""){
		printf("Warning: no inputfile provided!\n");
		return 0;
	}

	//reading from file into tree
	TTree *tree_val = new TTree("tree_val", "tree_val");
	vector<string> inputfiles = split(inputfile, IsWindowsCompilation?';':':');
	for (unsigned int i = 0; i < inputfiles.size(); ++i)
	{
		if (i == 0) tree_val->ReadFile(inputfiles.at(i).c_str(), inputformat.c_str());
		else tree_val->ReadFile(inputfiles.at(i).c_str());
	}
	bool isVoltage=tree_val->FindBranch("SELECTOR");
	float PadNumber(0), Value(0), Voltage(0),inter0(0),inter1(0),inter2(0),inter3(0),inter4(0),inter5(0),inter6(0),inter7(0),inter8(0),inter9(0);
	tree_val->SetBranchAddress("PADNUM",&PadNumber);
	tree_val->SetBranchAddress("VAL",&Value);
	if (isVoltage) tree_val->SetBranchAddress("SELECTOR",&Voltage);
	int highestinterpad=-1;
	if (inputformat.find("INTER0")<inputformat.size()) {tree_val->SetBranchAddress("INTER0",&inter0); ++highestinterpad;}
	if (inputformat.find("INTER1")<inputformat.size()) {tree_val->SetBranchAddress("INTER1",&inter1); ++highestinterpad;}
	if (inputformat.find("INTER2")<inputformat.size()) {tree_val->SetBranchAddress("INTER2",&inter2); ++highestinterpad;}
	if (inputformat.find("INTER3")<inputformat.size()) {tree_val->SetBranchAddress("INTER3",&inter3); ++highestinterpad;}
	if (inputformat.find("INTER4")<inputformat.size()) {tree_val->SetBranchAddress("INTER4",&inter4); ++highestinterpad;}
	if (inputformat.find("INTER5")<inputformat.size()) {tree_val->SetBranchAddress("INTER5",&inter5); ++highestinterpad;}
	if (inputformat.find("INTER6")<inputformat.size()) {tree_val->SetBranchAddress("INTER6",&inter6); ++highestinterpad;}
	if (inputformat.find("INTER7")<inputformat.size()) {tree_val->SetBranchAddress("INTER7",&inter7); ++highestinterpad;}
	if (inputformat.find("INTER8")<inputformat.size()) {tree_val->SetBranchAddress("INTER8",&inter8); ++highestinterpad;}
	if (inputformat.find("INTER9")<inputformat.size()) {tree_val->SetBranchAddress("INTER9",&inter9); ++highestinterpad;}

	//prepare variables to fill
	std::vector<double> volt_vec;
	TF1 *err_func=new TF1("err_func",measunc.c_str(),0.,1000000.);
	std::vector<std::vector<double>> val_vec,err_vac,inter0_val_vec,inter1_val_vec,inter2_val_vec,inter3_val_vec,inter4_val_vec, inter5_val_vec,inter6_val_vec,inter7_val_vec,inter8_val_vec, inter9_val_vec;
	for (int i = 0; i < MAXCELLS; ++i)
	{
		std::vector<double> tmp_vec;
		for (int j = 0; j < MAXSELECTORS; ++j)
		{
			tmp_vec.push_back(0);
		}
		val_vec.push_back(tmp_vec);
		err_vac.push_back(tmp_vec);
		inter0_val_vec.push_back(tmp_vec);
		inter1_val_vec.push_back(tmp_vec);
		inter2_val_vec.push_back(tmp_vec);
		inter3_val_vec.push_back(tmp_vec);
		inter4_val_vec.push_back(tmp_vec);
		inter5_val_vec.push_back(tmp_vec);
		inter6_val_vec.push_back(tmp_vec);
		inter7_val_vec.push_back(tmp_vec);
		inter8_val_vec.push_back(tmp_vec);
		inter9_val_vec.push_back(tmp_vec);
	}
	int maxpad[MAXSELECTORS];
	int minpad[MAXSELECTORS];
	std::vector<std::vector<int>> ispadfilledvec;
	bool ispadindatafile[MAXCELLS];
	int cell_appearance_count[MAXCELLS];
	for (int i = 0; i < MAXSELECTORS; ++i)
	{
		maxpad[i]=0;
		minpad[i]=1000000;
		std::vector<int> v_tmp;
		for (int j = 0; j < MAXCELLS; ++j)
		{
			v_tmp.push_back(0);
			ispadindatafile[j]=false;
		}
		ispadfilledvec.push_back(v_tmp);
	}

	//loop tree and fill values
	for (int i = 0; i < tree_val->GetEntries(); ++i)
	{
		tree_val->GetEntry(i);
		int this_volt_pos = find(volt_vec.begin(), volt_vec.end(), Voltage) - volt_vec.begin();
		if (this_volt_pos == int(volt_vec.size())){
			volt_vec.push_back(Voltage);
		}
		int finalbin=MappedNumber(int(PadNumber),mapfile,verbose);
		if (finalbin == -1) continue;
		if (appearance >= 0 && cell_appearance_count[int(PadNumber)]>appearance) continue;
		if (Value == SKIPVALUE) continue;
		++cell_appearance_count[int(PadNumber)];
		if (maxpad[this_volt_pos]<finalbin) maxpad[this_volt_pos]=finalbin;
		if (minpad[this_volt_pos]>finalbin) minpad[this_volt_pos]=finalbin;
		ispadfilledvec[this_volt_pos][finalbin]=1;
		ispadindatafile[finalbin]=true;
		val_vec.at(finalbin).at(this_volt_pos) = Value*yscale;
		err_vac.at(finalbin).at(this_volt_pos) = err_func->Eval(val_vec.at(finalbin).at(this_volt_pos));
		if (highestinterpad>=0) inter0_val_vec.at(finalbin).at(this_volt_pos) = inter0*yscale;
		if (highestinterpad>=1) inter1_val_vec.at(finalbin).at(this_volt_pos) = inter1*yscale;
		if (highestinterpad>=2) inter2_val_vec.at(finalbin).at(this_volt_pos) = inter2*yscale;
		if (highestinterpad>=3) inter3_val_vec.at(finalbin).at(this_volt_pos) = inter3*yscale;
		if (highestinterpad>=4) inter4_val_vec.at(finalbin).at(this_volt_pos) = inter4*yscale;
		if (highestinterpad>=5) inter5_val_vec.at(finalbin).at(this_volt_pos) = inter5*yscale;
		if (highestinterpad>=6) inter6_val_vec.at(finalbin).at(this_volt_pos) = inter6*yscale;
		if (highestinterpad>=7) inter7_val_vec.at(finalbin).at(this_volt_pos) = inter7*yscale;
		if (highestinterpad>=8) inter8_val_vec.at(finalbin).at(this_volt_pos) = inter8*yscale;
		if (highestinterpad>=9) inter9_val_vec.at(finalbin).at(this_volt_pos) = inter9*yscale;
	}
	if (!volt_vec.size()) printf("Warning! No voltages found in file! Maybe wrong format for TTree::ReadFile()?\n");
	if (verbose > 0){
		printf("Found these voltages in file:\n");
		for (unsigned int i = 0; i < volt_vec.size(); ++i)
		{
			printf("%d.: %.2f\n",i,volt_vec.at(i) );
		}
	}
	if(voltage!=DEFAULT_SELECTOR && std::find(volt_vec.begin(), volt_vec.end(), voltage) == volt_vec.end()){
		printf("Warning: no entries in %s matching voltage %.2e!\n",inputfile.c_str(),voltage );
		// return 0;
	}
	if (voltage == DEFAULT_SELECTOR){
		//take highest voltage in file
		printf("Warning: no voltage given, select highest one!\n");
		for (unsigned int i = 0; i < volt_vec.size(); ++i)
		{
			if (voltage < volt_vec.at(i)) voltage = volt_vec.at(i);
		}
	}

	//now fill histograms to be returned
	int global_maxpad=0;
	int global_minpad=1000000;
	for (int i = 0; i < MAXSELECTORS; ++i)
	{
		if (maxpad[i]>global_maxpad) global_maxpad=maxpad[i];
		if (minpad[i]<global_minpad) global_minpad=minpad[i];
	}
	TH1F*values_h1d=0;
	TH1F*inter0_h1d=0;
	TH1F*inter1_h1d=0;
	TH1F*inter2_h1d=0;
	TH1F*inter3_h1d=0;
	TH1F*inter4_h1d=0;
	TH1F*inter5_h1d=0;
	TH1F*inter6_h1d=0;
	TH1F*inter7_h1d=0;
	TH1F*inter8_h1d=0;
	TH1F*inter9_h1d=0;
	TH1F **final_obj_1d = new TH1F*[11];

	TH2F*values_h2d=0;
	TH2F*dummy_h2d=0;
	TH2F **final_obj_2d = new TH2F*[11];

	bool isZeroBased = (global_minpad == 0);

	if (singlechan==-1 && voltage == ALLCHAN_SELECTOR){
			if (verbose > 1) printf("Setting values for all channels and all voltages\n");
			int nvolts=volt_vec.size();
			// values_h2d=new TH2F("values_h2d","values_h2d",global_maxpad-global_minpad+1,global_minpad-0.5,global_maxpad+0.5,nvolts,0,nvolts);
			values_h2d=new TH2F("values_h2d","values_h2d",global_maxpad,0.5,global_maxpad+0.5,nvolts,0,nvolts);
			dummy_h2d=new TH2F("dummy_h2d","dummy_h2d",global_maxpad,0.5,global_maxpad+0.5,nvolts,0,nvolts);
			for (int i_volt = 0; i_volt < nvolts; ++i_volt)
			{
				for (int i_pad = global_minpad; i_pad <= global_maxpad; ++i_pad)
				{
					if (!ispadindatafile[i_pad]) continue;
					double fillval=val_vec[i_pad][i_volt];
					double fillerr=err_vac[i_pad][i_volt];
					if (!ispadfilledvec[i_volt][i_pad]) {
						fillval=std::numeric_limits<double>::quiet_NaN();
					}
					// if (i_pad > maxpad[i_volt] || i_pad < minpad[i_volt]) continue;
					//if (verbose > 1) printf("Setting bin %d/%.0f value to %.2e\n",i_pad,volt_vec.at(i_volt),valarr[i_pad][i_volt] );
					if (verbose > 1) printf("Setting bin %d/%.0f value to %.2e +- %.2e(singlechan==-1 && voltage == %.0f)\n", i_pad+1, volt_vec.at(i_volt), fillval,fillerr,DEFAULT_SELECTOR);
					values_h2d->Fill(i_pad+1, i_volt,fillval );
					values_h2d->SetBinError(values_h2d->FindBin(i_pad+1,i_volt),fillerr);
				}
				values_h2d->GetYaxis()->SetBinLabel(i_volt+1,Form("%.0f",volt_vec.at(i_volt)));
			}
			if (verbose > 1) printf("Histogram has %d xbins and %d ybins\n", values_h2d->GetNbinsX(), values_h2d->GetNbinsY());
			if (verbose) printf("LoadValues finished!\n");
			final_obj_2d[0]=values_h2d;
			final_obj_2d[1]=dummy_h2d;
			final_obj_2d[2]=dummy_h2d;
			final_obj_2d[3]=dummy_h2d;
			final_obj_2d[4]=dummy_h2d;
			final_obj_2d[5]=dummy_h2d;
			final_obj_2d[6]=dummy_h2d;
			return (TObject**)final_obj_2d;
	}else if (singlechan == -1){
		int volt_pos = find(volt_vec.begin(), volt_vec.end(), voltage) - volt_vec.begin();
		if (verbose > 1) printf("Setting values for all channels\n");
		if (verbose > 1) printf("maxpad %d minpad %d TH1F('values_h1d','values_h1d',%d,%f,%f)\n",maxpad[volt_pos],minpad[volt_pos],global_maxpad,0.5,global_maxpad-0.5);
		values_h1d=new TH1F("values_h1d","values_h1d",global_maxpad,0.5,global_maxpad-0.5);
		inter0_h1d=new TH1F("inter0_h1d","inter0_h1d",global_maxpad,0.5,global_maxpad-0.5);
		inter1_h1d=new TH1F("inter1_h1d","inter1_h1d",global_maxpad,0.5,global_maxpad-0.5);
		inter2_h1d=new TH1F("inter2_h1d","inter2_h1d",global_maxpad,0.5,global_maxpad-0.5);
		inter3_h1d=new TH1F("inter3_h1d","inter3_h1d",global_maxpad,0.5,global_maxpad-0.5);
		inter4_h1d=new TH1F("inter4_h1d","inter4_h1d",global_maxpad,0.5,global_maxpad-0.5);
		inter5_h1d=new TH1F("inter5_h1d","inter5_h1d",global_maxpad,0.5,global_maxpad-0.5);
		inter6_h1d=new TH1F("inter6_h1d","inter6_h1d",global_maxpad,0.5,global_maxpad-0.5);
		inter7_h1d=new TH1F("inter7_h1d","inter7_h1d",global_maxpad,0.5,global_maxpad-0.5);
		inter8_h1d=new TH1F("inter8_h1d","inter8_h1d",global_maxpad,0.5,global_maxpad-0.5);
		inter9_h1d=new TH1F("inter9_h1d","inter9_h1d",global_maxpad,0.5,global_maxpad-0.5);


		for (int i_pad = global_minpad; i_pad <= global_maxpad; ++i_pad)
		{
			if (!ispadfilledvec[volt_pos][i_pad]) continue;
			// if (i_pad > maxpad[volt_pos] || i_pad < minpad[volt_pos]) continue;
			//if (verbose > 1) printf("Setting bin %d value to %.2e\n",i_pad,valarr[i_pad][volt_pos] );
			int this_bin = isZeroBased? i_pad + 1 : i_pad;
			if (verbose > 1) {
				printf("Setting bin %d value to %.2e +- %.2e (singlechan == -1)\n", this_bin, val_vec[i_pad][volt_pos],err_vac[i_pad][volt_pos]);
				if (highestinterpad>-1) printf("Inter-pad values: %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e\n",inter0_val_vec[i_pad][volt_pos],inter1_val_vec[i_pad][volt_pos],inter2_val_vec[i_pad][volt_pos],inter3_val_vec[i_pad][volt_pos],inter4_val_vec[i_pad][volt_pos],inter5_val_vec[i_pad][volt_pos],inter6_val_vec[i_pad][volt_pos],inter7_val_vec[i_pad][volt_pos],inter8_val_vec[i_pad][volt_pos],inter9_val_vec[i_pad][volt_pos]);
			}

			//h1d->SetBinContent(i_pad,valarr[i_pad][volt_pos]);
			values_h1d->SetBinContent(this_bin, val_vec[i_pad][volt_pos]);
			values_h1d->SetBinError(this_bin,err_vac[i_pad][volt_pos]);
			inter0_h1d->SetBinContent(this_bin, inter0_val_vec[i_pad][volt_pos]);
			inter1_h1d->SetBinContent(this_bin, inter1_val_vec[i_pad][volt_pos]);
			inter2_h1d->SetBinContent(this_bin, inter2_val_vec[i_pad][volt_pos]);
			inter3_h1d->SetBinContent(this_bin, inter3_val_vec[i_pad][volt_pos]);
			inter4_h1d->SetBinContent(this_bin, inter4_val_vec[i_pad][volt_pos]);
			inter5_h1d->SetBinContent(this_bin, inter5_val_vec[i_pad][volt_pos]);
			inter6_h1d->SetBinContent(this_bin, inter6_val_vec[i_pad][volt_pos]);
			inter7_h1d->SetBinContent(this_bin, inter7_val_vec[i_pad][volt_pos]);
			inter8_h1d->SetBinContent(this_bin, inter8_val_vec[i_pad][volt_pos]);
			inter9_h1d->SetBinContent(this_bin, inter9_val_vec[i_pad][volt_pos]);
		}
		if (verbose > 1) printf("Histogram has %d xbins\n", values_h1d->GetNbinsX());
		if (verbose) printf("LoadValues finished!\n");
		final_obj_1d[0]=values_h1d;
		final_obj_1d[1]=inter0_h1d;
		final_obj_1d[2]=inter1_h1d;
		final_obj_1d[3]=inter2_h1d;
		final_obj_1d[4]=inter3_h1d;
		final_obj_1d[5]=inter4_h1d;
		final_obj_1d[6]=inter5_h1d;
		final_obj_1d[7]=inter6_h1d;
		final_obj_1d[8]=inter7_h1d;
		final_obj_1d[9]=inter8_h1d;
		final_obj_1d[10]=inter9_h1d;
		return (TObject**)final_obj_1d;
	}else if (singlechan >= 0){
		if (verbose > 1) printf("Setting values for channel %d\n",singlechan);
		int nvolts=volt_vec.size();
		values_h1d=new TH1F("values_h1d","values_h1d",nvolts,0,nvolts);
		inter0_h1d=new TH1F("inter0_h1d","inter0_h1d",nvolts,0,nvolts);
		inter1_h1d=new TH1F("inter1_h1d","inter1_h1d",nvolts,0,nvolts);
		inter2_h1d=new TH1F("inter2_h1d","inter2_h1d",nvolts,0,nvolts);
		inter3_h1d=new TH1F("inter3_h1d","inter3_h1d",nvolts,0,nvolts);
		inter4_h1d=new TH1F("inter4_h1d","inter4_h1d",nvolts,0,nvolts);
		inter5_h1d=new TH1F("inter5_h1d","inter5_h1d",nvolts,0,nvolts);
		inter6_h1d=new TH1F("inter6_h1d","inter6_h1d",nvolts,0,nvolts);
		inter7_h1d=new TH1F("inter7_h1d","inter7_h1d",nvolts,0,nvolts);
		inter8_h1d=new TH1F("inter8_h1d","inter8_h1d",nvolts,0,nvolts);
		inter9_h1d=new TH1F("inter9_h1d","inter9_h1d",nvolts,0,nvolts);
		for (int i = 0; i < nvolts; ++i)
		{
			//if (verbose > 1) printf("Setting bin %d value to %.2e\n",i,valarr[singlechan][i] );
			if (verbose > 1) {
				printf("Setting bin %d value to %.2e +- %.2e (singlechan >= 0)\n", i, val_vec[singlechan][i],err_vac[singlechan][i]);
				if (highestinterpad>-1) printf("Inter-pad values: %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e\n",inter0_val_vec[singlechan][i],inter1_val_vec[singlechan][i],inter2_val_vec[singlechan][i],inter3_val_vec[singlechan][i],inter4_val_vec[singlechan][i],inter5_val_vec[singlechan][i],inter6_val_vec[singlechan][i],inter7_val_vec[singlechan][i],inter8_val_vec[singlechan][i],inter9_val_vec[singlechan][i]);
			}
			//h1d->SetBinContent(i+1,valarr[singlechan][i]);
			values_h1d->SetBinContent(i + 1, val_vec[singlechan][i]);
			values_h1d->SetBinError(i+1,err_vac[singlechan][i]);
			values_h1d->GetXaxis()->SetBinLabel(i+1,Form("%.0f",volt_vec.at(i)));
			inter0_h1d->SetBinContent(i + 1, inter0_val_vec[singlechan][i]);
			inter1_h1d->SetBinContent(i + 1, inter1_val_vec[singlechan][i]);
			inter2_h1d->SetBinContent(i + 1, inter2_val_vec[singlechan][i]);
			inter3_h1d->SetBinContent(i + 1, inter3_val_vec[singlechan][i]);
			inter4_h1d->SetBinContent(i + 1, inter4_val_vec[singlechan][i]);
			inter5_h1d->SetBinContent(i + 1, inter5_val_vec[singlechan][i]);
			inter6_h1d->SetBinContent(i + 1, inter6_val_vec[singlechan][i]);
			inter7_h1d->SetBinContent(i + 1, inter7_val_vec[singlechan][i]);
			inter8_h1d->SetBinContent(i + 1, inter8_val_vec[singlechan][i]);
			inter9_h1d->SetBinContent(i + 1, inter9_val_vec[singlechan][i]);
		}
		if (verbose) printf("LoadValues finished!\n");
		final_obj_1d[0]=values_h1d;
		final_obj_1d[1]=inter0_h1d;
		final_obj_1d[2]=inter1_h1d;
		final_obj_1d[3]=inter2_h1d;
		final_obj_1d[4]=inter3_h1d;
		final_obj_1d[5]=inter4_h1d;
		final_obj_1d[6]=inter5_h1d;
		final_obj_1d[7]=inter6_h1d;
		final_obj_1d[8]=inter7_h1d;
		final_obj_1d[9]=inter8_h1d;
		final_obj_1d[10]=inter9_h1d;
		return (TObject**)final_obj_1d;
	}else{
		printf("Warning: invalid singlechan %d\n",singlechan );
		return 0;
	}
	if (verbose) printf("LoadValues finished!\n");
	return 0;
}














inline static TObject* LoadAverageValues(string inputfiles,string inputformat,string mapfile,double yscale,int verbose,double voltage,int singlechan, string measunc, int appearance){

	if (verbose) printf("Executing LoadAverageValues...\n");
	vector<string> filelist = split(inputfiles, IsWindowsCompilation?';':':');

	bool is1d=!(singlechan==-1 && voltage == DEFAULT_SELECTOR);
	for (int i = 0; i < MAXSENSORS; ++i)
	{
		for (int j = 0; j < MAXCELLS; ++j)
		{
			SensorValuesForAverage[i][j]=0;
		}
	}
	TH1F*h1d=0;
	TH2F*h2d=0;
	if (is1d) {
		h1d=(TH1F*)LoadValues(filelist.at(0),inputformat,mapfile,yscale,verbose,voltage,singlechan,measunc,appearance)[0];
		h1d->Reset();
	}
	else {
		h2d=(TH2F*)LoadValues(filelist.at(0),inputformat,mapfile,yscale,verbose,voltage,singlechan,measunc,appearance)[0];
		h2d->Reset();
	}
	int n_valid_files(0);
	int nbins_first_file(0);
	for (unsigned int i_file = 0; i_file < filelist.size(); ++i_file){
		string inputfile=filelist.at(i_file);
		if (verbose) printf("Getting file %s!\n",inputfile.c_str() );
		TH1F*h1dtmp=0;
		TH2F*h2dtmp=0;
		if (is1d) {
			h1dtmp=(TH1F*)LoadValues(inputfile,inputformat,mapfile,yscale,verbose,voltage,singlechan,measunc,appearance)[0];
			if (h1dtmp->Integral()==0) {
				printf("No content in histogram corresponding to %s! Skip it!\n",inputfile.c_str() );
				continue;
			}
			if (verbose>1) printf("Filling %d bin contents into graph\n",h1dtmp->GetNbinsX() );
			bool skipfile=false;
			for (int j_bin = 0; j_bin < h1dtmp->GetNbinsX(); ++j_bin)
			{
				if (n_valid_files==0) {
					gr_contents[j_bin]=new TGraph(0);
					nbins_first_file=h1dtmp->GetNbinsX();
				}
				if (nbins_first_file!=h1dtmp->GetNbinsX()){
					skipfile=true;
					printf("Incompatible number of bins in histogram corresponding to %s! Skip it!\n",inputfile.c_str() );
					break;
				}
				double cont=h1dtmp->GetBinContent(j_bin+1);
				gr_contents[j_bin]->SetPoint(n_valid_files,n_valid_files,cont);
				SensorValuesForAverage[i_file][j_bin]=cont;
			}
			if (skipfile) continue;
			VALID_SENSOR_NAMES[n_valid_files]=getFileNameNoExt(filelist.at(i_file),"_");
			++n_valid_files;
			delete h1dtmp;
		}
		else {
			h2dtmp=(TH2F*)LoadValues(inputfile,inputformat,mapfile,yscale,verbose,voltage,singlechan,measunc,appearance)[0];
			if (h2dtmp->Integral()==0) {
				printf("No content in histogram corresponding to %s!\n",inputfile.c_str() );
				continue;
			}
			for (int j_bin = 0; j_bin < h2dtmp->GetSize(); ++j_bin)
			{
				double oldcont=h2d->GetBinContent(j_bin+1);
				double tmpcont=h2dtmp->GetBinContent(j_bin+1);
				double newcont=(oldcont*n_valid_files+tmpcont)/(n_valid_files+1);
				h2d->SetBinContent(j_bin+1,newcont);
				if (n_valid_files==0) gr_contents[j_bin]=new TGraph(0);
				gr_contents[j_bin]->SetPoint(n_valid_files,n_valid_files,h2dtmp->GetBinContent(j_bin+1));
			}
			++n_valid_files;
			delete h2dtmp;
		}
	}
	if (verbose>1) printf("Copy bin contents into average histogram\n");
	TH1F*h_errors[MAXCELLS*MAXSELECTORS];
	if (is1d) {
		for (int j_bin = 0; j_bin < h1d->GetNbinsX(); ++j_bin)
		{
			double grmax=GetMaximum(gr_contents[j_bin]);
			double grmin=GetMinimum(gr_contents[j_bin]);
			double range=grmax-grmin;
			h_errors[j_bin]=new TH1F(Form("h_errors[%d]",j_bin),Form("h_errors[%d]",j_bin),100,grmin-0.01*range,grmax+0.01*range);
			for (int i_file = 0; i_file < n_valid_files; ++i_file){
				h_errors[j_bin]->Fill(gr_contents[j_bin]->GetY()[i_file]);
			}
			// printf("Bin %d with content %f (%f), adding error %f\n",j_bin+1,h1d->GetBinContent(j_bin+1),h_errors[j_bin]->GetMean(),h_errors[j_bin]->GetRMS());
			h1d->SetBinContent(j_bin+1,h_errors[j_bin]->GetMean());
			h1d->SetBinError(j_bin+1,h_errors[j_bin]->GetRMS());
		}
	} else {
		for (int j_bin = 0; j_bin < h2d->GetSize(); ++j_bin)
		{
			double grmax=GetMaximum(gr_contents[j_bin]);
			double grmin=GetMinimum(gr_contents[j_bin]);
			double range=grmax-grmin;
			h_errors[j_bin]=new TH1F(Form("h_errors[%d]",j_bin),Form("h_errors[%d]",j_bin),100,grmin-0.01*range,grmax+0.01*range);
			for (int i_file = 0; i_file < n_valid_files; ++i_file){
				h_errors[j_bin]->Fill(gr_contents[j_bin]->GetY()[i_file]);
			}
			// printf("Bin %d / %d with content %f (%f), adding error %f\n",j_bin+1,h2d->GetSize(),h2d->GetBinContent(j_bin+1),h_errors[j_bin]->GetMean(),h_errors[j_bin]->GetRMS());
			h2d->SetBinContent(j_bin+1,h_errors[j_bin]->GetMean());
			h2d->SetBinError(j_bin+1,h_errors[j_bin]->GetRMS());
			delete h_errors[j_bin];
		}
	}

	N_VALID_FILES=n_valid_files;

	if (verbose) printf("LoadAverageValues finished!\n");

	if (is1d) return h1d;
	else      return h2d;
}















inline static TObject* LoadDepletionVoltages(string inputfile,string inputformat,string mapfile,double yscale,int verbose, string measunc, int appearance, string fitfunc){
	if (verbose) printf("Executing LoadDepletionVoltages...\n");

	TH2F *h_fill=0;
	double default_selector=ALLCHAN_SELECTOR;
	h_fill = (TH2F*)LoadValues(inputfile,inputformat,mapfile,yscale,verbose,default_selector,-1,measunc,appearance)[0];

	TH1F*h1dtmp=(TH1F*)h_fill->ProjectionX("h1dtmp");
	int newnbin=h1dtmp->GetNbinsX();
	double newbinmin=h1dtmp->GetBinCenter(1)-h1dtmp->GetBinWidth(1)/2;
	double newbinmax=h1dtmp->GetBinCenter(newnbin)+h1dtmp->GetBinWidth(newnbin)/2;
	delete h1dtmp;

	TH1F*h1d=new TH1F("h1d","h1d",newnbin,newbinmin,newbinmax);

	//now determine depletion voltage, i.e. plot 1/C^2 vs U
	TGraphErrors*gr_fill_trafo[MAXCELLS];
	TGraphErrors*gr_fill_std[MAXCELLS];
	TF1*f_volt_bias[MAXCELLS];
	double multiplocator=1000.;
	int nvolts_help=h_fill->GetNbinsY();
	int minpad_help=0;
	int maxpad_help=h_fill->GetNbinsX()-1;
	for (int ipad = minpad_help; ipad <= maxpad_help; ++ipad)
	{
		bool isfinal=(ipad == maxpad_help);

		gr_fill_trafo[ipad]=new TGraphErrors(0);
		gr_fill_std[ipad]=new TGraphErrors(0);
		double sumentries=0;
		int npoint=0;
		for (int ivolt = 0; ivolt < nvolts_help; ++ivolt)
		{
			// h2draw->Fill(ipad,ivolt,cont_new);
			double cont=h_fill->GetBinContent(ipad+1,ivolt+1);
			double err=h_fill->GetBinError(ipad+1,ivolt+1);
			if (cont!=cont) continue;
			sumentries+=cont;
			// printf("h_fill bin %d cont %f\n",ipad+1,cont );
			double newcont=cont>0?multiplocator/pow(cont,2):-1;
			double newerr=cont>0?multiplocator*err*2./pow(cont,3):0;
			gr_fill_trafo[ipad]->SetPoint(npoint, std::atof(h_fill->GetYaxis()->GetBinLabel(ivolt+1)),newcont);
			gr_fill_trafo[ipad]->SetPointError(npoint,0,newerr);
			gr_fill_std[ipad]->SetPoint(npoint, std::atof(h_fill->GetYaxis()->GetBinLabel(ivolt+1)),cont);
			gr_fill_std[ipad]->SetPointError(npoint,0,err);
			++npoint;
		}
		if (sumentries == 0) continue;

		//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
		//this is the part determining the value
		//fit with two linear functions
		double minbinx=gr_fill_trafo[ipad]->GetX()[0];
		double maxbinx=gr_fill_trafo[ipad]->GetX()[gr_fill_trafo[ipad]->GetN()-1];
		double almostminbinx=gr_fill_trafo[ipad]->GetX()[1];
		double almostmaxbinx=gr_fill_trafo[ipad]->GetX()[gr_fill_trafo[ipad]->GetN()-2];
		if (minbinx > maxbinx) {
			swap(minbinx,maxbinx);
			swap(almostminbinx,almostmaxbinx);
		}
		double fitrange=maxbinx-minbinx;
		double depvol=-1;
		double depvolerr=-1;
		string fit_options = verbose > 1 ? "0" : "Q0";
		if(verbose > 1) printf("Fitting CV curve for pad %d\n", ipad);

		vector<string> matches = split(fitfunc, ',');
		
		vector<double> rngs;
		for(unsigned int i = 1; i < matches.size(); i++) rngs.push_back(std::atof(matches.at(i).c_str()));

		if(verbose > 1) cout << "Fit function: " << matches.at(0) << " with " << rngs.size() << " range parameters" << endl;
		bool rangedfit = (rngs.size() > 0);

		if (matches.at(0) == "linint"){
			RangedBiLinearFitFunc* fit_function = new RangedBiLinearFitFunc(fit_function_linearintersection,rngs);

			f_volt_bias[ipad]=new TF1(Form("f_volt_bias[%d]",ipad),fit_function,minbinx-0.02*fitrange,maxbinx+0.02*fitrange,4);
			f_volt_bias[ipad]->SetParNames("Vdep", "slope1", "intercept1", "slope2");

			//make the second line a constant by forcing its gradient to 0
			f_volt_bias[ipad]->FixParameter(3,0);

			//give an initial gues and bound for the depletion voltage
			f_volt_bias[ipad]->SetParameter(0,(almostmaxbinx+almostminbinx)/2.);
			f_volt_bias[ipad]->SetParLimits(0,almostminbinx,almostmaxbinx+20);

			//if more than two ranges are given, use the center of the gap between the first two as a guess for vdep
			if(rngs.size() > 2) {
				f_volt_bias[ipad]->SetParameter(0,(rngs[1]+rngs[2])/2.0);
			}
			
			//do the fit and extract the depletion voltage
			gr_fill_trafo[ipad]->Fit(f_volt_bias[ipad],fit_options.c_str());
			depvol=f_volt_bias[ipad]->GetParameter(0);
			depvolerr=f_volt_bias[ipad]->GetParError(0);
		}
		if (matches.at(0) == "smoothlinint"){
			RangedBiLinearFitFunc* fit_function = new RangedBiLinearFitFunc(smoothed_linearintersection,rngs);

			f_volt_bias[ipad]=new TF1(Form("f_volt_bias[%d]",ipad),fit_function,minbinx-0.02*fitrange,maxbinx+0.02*fitrange,5);
			f_volt_bias[ipad]->SetParNames("Vdep", "slope1", "intercept1", "slope2", "k");

			//make the second line a constant by forcing its gradient to 0
			f_volt_bias[ipad]->FixParameter(3,0);

			//give an initial guess and bound for k
			f_volt_bias[ipad]->SetParameter(4,50);
			f_volt_bias[ipad]->SetParLimits(4,10,1000);

			//give an initial gues and bound for the depletion voltage
			f_volt_bias[ipad]->SetParameter(0,(almostmaxbinx+almostminbinx)/2.);
			f_volt_bias[ipad]->SetParLimits(0,almostminbinx,almostmaxbinx+20);

			//if more than two ranges are given, use the center of the gap between the first two as a guess for vdep
			if(rngs.size() > 2) {
				f_volt_bias[ipad]->SetParameter(0,(rngs[1]+rngs[2])/2.0);
			}
			
			//do the fit and extract the depletion voltage
			gr_fill_trafo[ipad]->Fit(f_volt_bias[ipad],fit_options.c_str());
			depvol=f_volt_bias[ipad]->GetParameter(0);
			depvolerr=f_volt_bias[ipad]->GetParError(0);
		}
		else if (matches.at(0) == "linear"){
			RangedBiLinearFitFunc* fit_function = new RangedBiLinearFitFunc(linear,rngs);

			f_volt_bias[ipad]=new TF1(Form("f_volt_bias[%d]",ipad),fit_function,minbinx-0.02*fitrange,maxbinx+0.02*fitrange,2);
			gr_fill_trafo[ipad]->Fit(f_volt_bias[ipad],fit_options.c_str());
			depvol=f_volt_bias[ipad]->GetParameter(1);
			depvolerr=f_volt_bias[ipad]->GetParError(1);
		}

		if (depvol == -1 && depvolerr == -1){
			printf("Warning: fit was not performed! Check specified fit function!\n");
		}
		// printf("Cell %d has depletion voltage %.2f +- %.2f\n",ipad-1,depvol,depvolerr );
		//this is the part determining the value
		//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

		h1d->SetBinContent(ipad+1,depvol);
		h1d->SetBinError(ipad+1,depvolerr);




		if (verbose > 0){

			//now plot it
			string sensorname=stripExtension(getFileNameNoExt(inputfile),"_");
			TCanvas *c_chanbias = new TCanvas(Form("c_%s_chanbias[%d]",sensorname.c_str(),ipad),Form("c_%s_chanbias[%d]",sensorname.c_str(),ipad),800,500,750,int(750/1.5));
			TPad *leftaxispad=new TPad("leftaxispad","",0.,0.,1.,1.);
			TPad *rightaxispad=new TPad("rightaxispad","",0.,0.,1.,1.);
			rightaxispad->SetFillStyle(4000); //will be transparent
			Float_t RightMargin=0.13;
			Float_t TopMargin=0.05;
			Float_t BottomMargin=0.1;
			Float_t LeftMargin=0.13;


			//draw leftaxispad
			leftaxispad->Draw();
			leftaxispad->cd();
			gPad->SetRightMargin(RightMargin);
			gPad->SetTopMargin(TopMargin);
			gPad->SetBottomMargin(BottomMargin);
			gPad->SetLeftMargin(LeftMargin);
			gPad->SetGridx();
			gr_fill_trafo[ipad]->GetHistogram()->GetXaxis()->SetTitle("Voltage [V]");
			gr_fill_trafo[ipad]->GetHistogram()->GetYaxis()->SetTitle(Form("1/C^{2} [%.0f/pF^{2}]",multiplocator));
			gr_fill_trafo[ipad]->GetHistogram()->GetYaxis()->SetTitleOffset(2);
			gr_fill_trafo[ipad]->SetLineColorAlpha(kBlue, 0.0);
			gr_fill_trafo[ipad]->SetTitle("");
			gr_fill_trafo[ipad]->SetMarkerStyle(20);
			gr_fill_trafo[ipad]->SetMarkerSize(0.8);
			gr_fill_trafo[ipad]->DrawClone("pla");
			gr_fill_trafo[ipad]->DrawClone("plesame");
			
			f_volt_bias[ipad]->SetLineWidth(1);
			//if it was ranged draw the whole line dotted
			if(rangedfit) f_volt_bias[ipad]->SetLineStyle(3);
			f_volt_bias[ipad]->DrawClone("same");
			
			Double_t x = depvol;
			Double_t y = f_volt_bias[ipad]->Eval(x);
			TArrow *deparrow = new TArrow(x,y*1.04,x,y*1.01,0.03,">");
			deparrow->SetLineWidth(1);
			deparrow->SetLineColor(kRed);
			deparrow->Draw();
			
			//draw solid lines showing where the fit ranges were
			if(rangedfit) {
				//draw segments in full
				f_volt_bias[ipad]->SetLineStyle(1);
				for(unsigned int i = 0; i < rngs.size(); i += 2) {
					if(i < (rngs.size() - 1)) {
						f_volt_bias[ipad]->SetRange(rngs.at(i),rngs.at(i+1));
						f_volt_bias[ipad]->DrawClone("same");
					}
					if(i == (rngs.size() - 1)) {
						f_volt_bias[ipad]->SetRange(rngs.at(i),1000);
						f_volt_bias[ipad]->DrawClone("same");
					}
				}

			}
			

			//some description
			TPaveText *padbias=0;
			padbias = new TPaveText(0.13,0.97,0.13,0.97,"NDC");
			padbias->SetFillColor(kNone);
			padbias->SetLineColor(kNone);
			padbias->SetTextSize(0.03);
			padbias->SetTextAlign(12);
			padbias->SetTextColor(kBlack);
			padbias->AddText(Form("%s pad %d: full depletion at %.2f #pm %.2f V",sensorname.c_str(),ipad+1,depvol,depvolerr));
			padbias->Draw("same");
			leftaxispad->Update(); //this will force the generation of the "stats" box
			leftaxispad->Modified();
			double xmin=gPad->GetUxmin(); //get value from this pad!
			double xmax=gPad->GetUxmax(); //get value from this pad!


			//draw rightaxispad
			c_chanbias->cd();
			//compute the pad range with suitable margins
			double ymin=gr_fill_std[ipad]->GetHistogram()->GetMinimum();
			double ymax=gr_fill_std[ipad]->GetHistogram()->GetMaximum();
			//make consistent margins
			Double_t dy = (ymax-ymin)/(1-TopMargin-BottomMargin);
			Double_t dx = (xmax-xmin)/(1-RightMargin-LeftMargin);
			rightaxispad->Range(xmin-LeftMargin*dx,ymin-BottomMargin*dy,xmax+RightMargin*dx,ymax+TopMargin*dy);
			rightaxispad->Draw();
			rightaxispad->cd();
			gr_fill_std[ipad]->SetTitle("");
			gr_fill_std[ipad]->SetMarkerStyle(20);
			gr_fill_std[ipad]->SetMarkerSize(0.8);
			gr_fill_std[ipad]->SetLineColor(kBlue);
			gr_fill_std[ipad]->SetMarkerColor(kBlue);
			// gr_fill_std[ipad]->DrawClone("pla");
			gr_fill_std[ipad]->DrawClone("][plesames");
			rightaxispad->Update();
			// draw axis on the right side of the pad
			TGaxis *rightaxis = new TGaxis(xmax,ymin,xmax,ymax,ymin,ymax,110,"+L");
			rightaxis->SetTitle("Capacity [pF]");
			rightaxis->SetTitleOffset(1.5);
			rightaxis->SetTitleSize(gr_fill_trafo[ipad]->GetHistogram()->GetYaxis()->GetTitleSize());
			rightaxis->SetLabelOffset(Float_t(gr_fill_trafo[ipad]->GetHistogram()->GetYaxis()->GetLabelOffset()*1.5));
			rightaxis->SetLabelSize(gr_fill_trafo[ipad]->GetHistogram()->GetYaxis()->GetLabelSize());
			rightaxis->SetLabelColor(kBlue);
			rightaxis->SetTitleColor(kBlue);
			rightaxis->SetTitleFont(gr_fill_trafo[ipad]->GetHistogram()->GetYaxis()->GetTitleFont());
			rightaxis->SetLabelFont(gr_fill_trafo[ipad]->GetHistogram()->GetYaxis()->GetLabelFont());
			rightaxis->Draw();

			//plot it
			PlotThisPad(c_chanbias,sensorname,isfinal);


		}



	}

	if (verbose) printf("LoadDepletionVoltages finished!\n");
	return h1d;
}












inline static TObject* LoadAverageDepletionVoltages(string inputfiles,string inputformat,string mapfile,double yscale,int verbose, string measunc, int appearance, string fitfunc){
	if (verbose) printf("Executing LoadAverageDepletionVoltages...\n");

	vector<string> filelist = split(inputfiles, IsWindowsCompilation?';':':');

	TH1F*h_tmp=0;
	TH1F*h_mean=0;
	int n_valid_files=0;
	int nbins_first_file=0;
	for (unsigned int ifile = 0; ifile < filelist.size(); ++ifile)
	{
		 h_tmp = (TH1F*)LoadDepletionVoltages(filelist.at(ifile), inputformat, mapfile, yscale, verbose,measunc,appearance,fitfunc);
		 if (nbins_first_file == 0) {
		 	h_mean=(TH1F*)h_tmp->Clone();
			nbins_first_file=h_mean->GetNbinsX();
		 	for (int ibin = 0; ibin < h_mean->GetNbinsX(); ++ibin)
		 	{
				gr_contents[ibin]=new TGraph(0);
	 			double newval=h_tmp->GetBinContent(ibin+1);
				gr_contents[ibin]->SetPoint(n_valid_files,n_valid_files,newval);
		 	}

		 } else{
			if (nbins_first_file!=h_mean->GetNbinsX()){
				printf("Incompatible number of bins in histogram corresponding to %s! Skip it!\n",filelist.at(ifile).c_str() );
				continue;
			}
		 	for (int ibin = 0; ibin < h_mean->GetNbinsX(); ++ibin)
		 	{
	 			double oldcont=h_mean->GetBinContent(ibin+1);
	 			double newval=h_tmp->GetBinContent(ibin+1);
	 			double newcont=(oldcont*ifile+newval)/(ifile+1);
	 			h_mean->SetBinContent(ibin+1,newcont);
				gr_contents[ibin]->SetPoint(n_valid_files,n_valid_files,newval);
		 	}
		 }
		 ++n_valid_files;
	}
	N_VALID_FILES=n_valid_files;
	if (verbose) printf("LoadAverageDepletionVoltages finished!\n");
	return h_mean;
}
















inline static TObject* LoadValuesToVoltage(string inputfile,string inputformat,string mapfile,double yscale,double selector,int verbose, string measunc, int appearance){
	if (verbose) printf("Executing LoadValuesToVoltage...\n");

	TH2F *h_fill=0;
	double default_selector=ALLCHAN_SELECTOR;
	h_fill = (TH2F*)LoadValues(inputfile,inputformat,mapfile,yscale,verbose,default_selector,-1,measunc,appearance)[0];

	TH1F*h1dtmp=(TH1F*)h_fill->ProjectionX("h1dtmp");
	int newnbin=h1dtmp->GetNbinsX();
	double newbinmin=h1dtmp->GetBinCenter(1)-h1dtmp->GetBinWidth(1)/2;
	double newbinmax=h1dtmp->GetBinCenter(newnbin)+h1dtmp->GetBinWidth(newnbin)/2;
	delete h1dtmp;

	TH1F*h1d=new TH1F("h1d","h1d",newnbin,newbinmin,newbinmax);

	printf("h1d(%d,%f,%f)\n", newnbin,newbinmin,newbinmax);

	//now determine voltage corresponding to selector
	TGraphErrors*gr_fill_std[MAXCELLS];
	int nvolts_help=h_fill->GetNbinsY();
	int minpad_help=0;
	int maxpad_help=h_fill->GetNbinsX()-1;
	for (int ipad = minpad_help; ipad <= maxpad_help; ++ipad)
	{
		bool isfinal=(ipad == maxpad_help);
		if (verbose > 1) printf("LoadValuesToVoltage: filling pad %d\n",ipad+1 );

		gr_fill_std[ipad]=new TGraphErrors(0);
		double sumentries=0;
		int npoint=0;
		for (int ivolt = 0; ivolt < nvolts_help; ++ivolt)
		{
			double cont=h_fill->GetBinContent(ipad+1,ivolt+1);
			double err=h_fill->GetBinError(ipad+1,ivolt+1);
			if (cont!=cont) continue;
			sumentries+=cont;
			gr_fill_std[ipad]->SetPoint(npoint, std::atof(h_fill->GetYaxis()->GetBinLabel(ivolt+1)),cont);
			gr_fill_std[ipad]->SetPointError(npoint,0,err);
			++npoint;
		}
		if (sumentries == 0) continue;

		//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
		//this is the part determining the value
		double minbinx=gr_fill_std[ipad]->GetX()[0];
		double maxbinx=gr_fill_std[ipad]->GetX()[gr_fill_std[ipad]->GetN()-1];
		TSpline3 *inter_spline = new TSpline3(Form("gr_fill_std[%d]",ipad),gr_fill_std[ipad]);
		double voltage=FindFirstX(inter_spline,selector,minbinx,maxbinx);
		double voltageerr=0;
		//this is the part determining the value
		//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

		h1d->SetBinContent(ipad+1,voltage);
		h1d->SetBinError(ipad+1,voltageerr);




		if (verbose > 1){
			//now plot it
			string sensorname=stripExtension(getFileNameNoExt(inputfile),"_");
			TCanvas *c_chanbias = new TCanvas(Form("c_%s_chanbias[%d]",sensorname.c_str(),ipad),Form("c_%s_chanbias[%d]",sensorname.c_str(),ipad),800,500,750,int(750/1.5));
			TPad *leftaxispad=new TPad("leftaxispad","",0.,0.,1.,1.);
			TPad *rightaxispad=new TPad("rightaxispad","",0.,0.,1.,1.);
			rightaxispad->SetFillStyle(4000); //will be transparent
			Float_t RightMargin=0.13;
			Float_t TopMargin = 0.05;
			Float_t BottomMargin = 0.1;
			Float_t LeftMargin = 0.13;


			//draw leftaxispad
			leftaxispad->Draw();
			leftaxispad->cd();
			gPad->SetRightMargin(RightMargin);
			gPad->SetTopMargin(TopMargin);
			gPad->SetBottomMargin(BottomMargin);
			gPad->SetLeftMargin(LeftMargin);
			gPad->SetGridx();
			gr_fill_std[ipad]->GetHistogram()->GetXaxis()->SetTitle("Voltage [V]");
			gr_fill_std[ipad]->GetHistogram()->GetYaxis()->SetTitle("Value [Arb. units]");
			gr_fill_std[ipad]->GetHistogram()->GetYaxis()->SetTitleOffset(2);
			gr_fill_std[ipad]->SetTitle("");
			gr_fill_std[ipad]->SetMarkerStyle(20);
			gr_fill_std[ipad]->SetMarkerSize(0.8);
			gr_fill_std[ipad]->DrawClone("pla");
			gr_fill_std[ipad]->DrawClone("plesame");
			inter_spline->SetLineColor(kRed);
			inter_spline->Draw("same");

			TArrow *valline = new TArrow(voltage,selector,voltage,gr_fill_std[ipad]->GetHistogram()->GetMinimum(),0.03,">");
			valline->SetLineWidth(1);
			valline->SetLineColor(kRed);
			valline->Draw();
			TArrow *valline1 = new TArrow(gr_fill_std[ipad]->GetX()[0],selector,voltage,selector,0.03,">");
			valline1->SetLineWidth(1);
			valline1->SetLineColor(kRed);
			valline1->Draw();

			//some description
			TPaveText *padbias=0;
			padbias = new TPaveText(0.13,0.97,0.13,0.97,"NDC");
			padbias->SetFillColor(kNone);
			padbias->SetLineColor(kNone);
			padbias->SetTextSize(0.03);
			padbias->SetTextAlign(12);
			padbias->SetTextColor(kBlack);
			padbias->AddText(Form("%s pad %d: voltage at %.2f = %.2f",sensorname.c_str(),ipad+1,selector,voltage));
			padbias->Draw("same");
			leftaxispad->Update(); //this will force the generation of the "stats" box
			leftaxispad->Modified();
			//plot it
			PlotThisPad(c_chanbias,sensorname,isfinal);
		}
	}
	if (verbose) printf("LoadValuesToVoltage finished!\n");
	return h1d;
}
















inline static TObject* LoadAverageValuesToVoltage(string inputfiles,string inputformat,string mapfile,double yscale,double selector,int verbose, string measunc, int appearance){
	if (verbose) printf("Executing LoadAverageValuesToVoltage...\n");

	vector<string> filelist = split(inputfiles, IsWindowsCompilation?';':':');

	TH1F*h_tmp=0;
	TH1F*h_mean=0;
	int n_valid_files=0;
	int nbins_first_file=0;
	for (unsigned int ifile = 0; ifile < filelist.size(); ++ifile)
	{
		 h_tmp = (TH1F*)LoadValuesToVoltage(filelist.at(ifile), inputformat, mapfile, yscale, selector, verbose,measunc,appearance);
		 if (nbins_first_file == 0) {
		 	h_mean=(TH1F*)h_tmp->Clone();
			nbins_first_file=h_mean->GetNbinsX();
		 	for (int ibin = 0; ibin < h_mean->GetNbinsX(); ++ibin)
		 	{
				gr_contents[ibin]=new TGraph(0);
	 			double newval=h_tmp->GetBinContent(ibin+1);
				gr_contents[ibin]->SetPoint(n_valid_files,n_valid_files,newval);
		 	}
		 } else{
			if (nbins_first_file!=h_mean->GetNbinsX()){
				printf("Incompatible number of bins in histogram corresponding to %s! Skip it!\n",filelist.at(ifile).c_str() );
				continue;
			}
		 	for (int ibin = 0; ibin < h_mean->GetNbinsX(); ++ibin)
		 	{
	 			double oldcont=h_mean->GetBinContent(ibin+1);
	 			double newval=h_tmp->GetBinContent(ibin+1);
	 			double newcont=(oldcont*ifile+newval)/(ifile+1);
	 			h_mean->SetBinContent(ibin+1,newcont);
				gr_contents[ibin]->SetPoint(n_valid_files,n_valid_files,newval);
		 	}
		 }
		 ++n_valid_files;
	}
	N_VALID_FILES=n_valid_files;
	if (verbose) printf("LoadAverageValuesToVoltage finished!\n");
	return h_mean;
}











inline static TObject** LoadDataHisto(bool isAverageValues, string ValueOption,
																			string inputfile, string inputformat,
																			string mapfile, double yscale, int verbose,
																			double &selector, int singlechan=-1,
																			string measunc="", int appearance=-1,
																			string fitfunc="linint"){
	if (verbose) printf("Executing LoadDataHisto...\n");
	bool is1d=true;

	TH1F **final_obj_1d = new TH1F*[7];
	TH2F **final_obj_2d = new TH2F*[7];


	if (ValueOption == "DEP"){
		if (!isAverageValues) final_obj_1d[0] = (TH1F*)LoadDepletionVoltages(inputfile, inputformat, mapfile, yscale, verbose,measunc,appearance,fitfunc);
		else                  final_obj_1d[0] = (TH1F*)LoadAverageDepletionVoltages(inputfile,inputformat,mapfile,yscale,verbose,measunc,appearance,fitfunc);
	}else if (ValueOption == "VAL"){
		if (!isAverageValues) final_obj_1d[0] = (TH1F*)LoadValuesToVoltage(inputfile, inputformat, mapfile, yscale, selector,verbose,measunc,appearance);
		else                  final_obj_1d[0] = (TH1F*)LoadAverageValuesToVoltage(inputfile,inputformat,mapfile,yscale,selector,verbose,measunc,appearance);
	}else{
		if (!isAverageValues) final_obj_1d    = (TH1F**)LoadValues(inputfile, inputformat, mapfile, yscale, verbose, selector, singlechan,measunc,appearance);
		else                  final_obj_1d[0] = (TH1F*)LoadAverageValues(inputfile,inputformat,mapfile,yscale,verbose,selector, singlechan,measunc,appearance);
	}

	if (verbose) printf("LoadDataHisto finished!\n");
	if (is1d) return (TObject**)final_obj_1d;
	else      return (TObject**)final_obj_2d;
}
