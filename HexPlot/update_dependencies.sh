#!/bin/bash
DEPDIR=/afs/cern.ch/user/a/amaier/Utils
echo Updating depedencies from $DEPDIR

cp $DEPDIR/cpp/CppUtils.h .
cp $DEPDIR/cpp/CppUtils.cxx .
cp $DEPDIR/cpp/RootUtils.h .
cp $DEPDIR/cpp/RootUtils.cxx .
cp $DEPDIR/cpp/FitFunctions.h .
