#ifndef THEX_H
#define THEX_H


#include <cmath>
#include "TPaletteAxis.h"
#include "TMath.h"
#include "TPave.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TPolyLine.h"
#include "TPaveText.h"
#include "TTree.h"
#include "TList.h"
#include "TVector2.h"


using namespace std;


const int MAXPADNUMBER=100000;
const int MAXINTERCELL=10;
const int MAXCELLCORNERS=10000;

class THex {
public:
	void Fill(int, double);
	void Fill(TH1F*);
	void FillInterCell(TH1F* h){FillInterCell(h,intercells++);};
	void FillInterCell(TH1F*,int);
	void Draw();
	void SetZtitle(string s){ztitle=s;};
	void SetZeroSuppress(bool b){zerosuppress=b;};
	void SetZrange(double z_min,double z_max){zmin=z_min;zmax=z_max;};
	void SetTopLeftInfo(string s){ topleftinfo = s; infostring += "TOPLEFT"; };
	void SetTopRightInfo(string s){ toprightinfo = s; infostring += "TOPRIGHT"; };
	void SetLowLeftInfo(string s){ lowleftinfo = s; infostring += "LOWLEFT"; };
	void SetLowRightInfo(string s){ lowrightinfo = s; infostring += "LOWRIGHT"; };
	void SetDebug(bool b){debug=b;};
	void SetOutputfile(string s){outputfile=s;};
	void SetPadTypeScale(int i,double d){padtypescale[i]=d;};
	void SetPadScale(int i,double d){padscale[i]=d;};
	void SetNoAxis(bool b){noaxis=b;};
	void SetInfo(bool b){infostring=b?"ALL":"NONE";};
	void SetInfo(string s){ infostring = s; };
	void SetGeoFile(string s){geofile=s;};
	void SetPadNumberOption(int i){padnumberoption=i;};
	void SetPadValuePrecision(int i){numdigits=i;};
	void HighlightPad(int i){highlightpads.push_back(i);};
	void HighlightPadOption(string s="dotted"){highlightcell_treatothers=s;};
	void GreyOutPads(){greyoutpads=true;};
	int GetPadType(int);
	double GetPadSurface(int);
	string GetPadTypeString(int);
	string GetTypeString(int);
	void Reset();
	THex();
	~THex();

private:
	int npads;
	int numdigits;
	int padnumberoption;
	int intercells;
	string ztitle;
	string outputfile;
	string topleftinfo;
	string toprightinfo;
	string lowleftinfo;
	string lowrightinfo;
	string geofile;
	string infostring;
	bool debug;
	bool noaxis;
	bool geofileisread;
	bool drawinterpadvalues;
	bool greyoutpads;
	bool zerobased;
	bool zerosuppress;
	string highlightcell_treatothers;
	// TH2F*h_draw;
	TH2F*h_vals;
	TH1*h_cols;
	TH2F*h_vals_inter[MAXINTERCELL];
	TH1*h_cols_inter[MAXINTERCELL];
	TTree *geotree;
	double honey_halfwidth;
	double honey_halfheight;
	double padtypescale[MAXPADNUMBER];
	double padscale[MAXPADNUMBER];
	double zmin;
	double zmax;
	std::vector<int> padnumbers;
	std::vector<int> padtypes;
	std::vector<double> padxpositions;
	std::vector<double> padypositions;
	std::vector<double> padsizes;
	std::vector<double> padrotations;
	std::vector<int> highlightpads;
	void DrawWhiteBox();
	void DrawCircle(double,double,double,double,int,int);
	void DrawEllipse(double,double,double,double,double,int,int);
	void DrawEquiPoly(double,double,double,double,int,int[1+MAXINTERCELL],int,int);
	void DrawHex(double,double,double,double,int[1+MAXINTERCELL],int,int);
	void DrawHexCornercut(double,double,double,double,int[1+MAXINTERCELL],int,int);
	void DrawHexHalf(double,double,double,double,int[1+MAXINTERCELL],int,int);
	void DrawHexOtherHalf(double,double,double,double,int[1+MAXINTERCELL],int,int);
	void DrawHexSmallOtherHalf(double,double,double,double,int[1+MAXINTERCELL],int,int);
	void DrawHexJumper(double,double,double,double,int[1+MAXINTERCELL],int,int);
	void DrawHexMousebite(double,double,double,double,int[1+MAXINTERCELL],int,int);
	void DrawHexRing(double,double,double,double,double,int[1+MAXINTERCELL],int,int);
	void DrawGuardRing(double,double,double,double,double,int[1+MAXINTERCELL],int);
	void DrawSquare(double,double,double,double,int[1+MAXINTERCELL],int,int);
	void DrawTriangle(double,double,double,double,int[1+MAXINTERCELL],int,int);
	void DrawHalfcircle(double,double,double,double,int[1+MAXINTERCELL],int);
	void DrawPizza(double,double,double,double,int[1+MAXINTERCELL],int);
	void DrawPent(double,double,double,double,int[1+MAXINTERCELL],int,int);
	void DrawHept(double,double,double,double,int[1+MAXINTERCELL],int,int);
	void DrawOct(double,double,double,double,int[1+MAXINTERCELL],int,int);
	void DrawDiamond(double,double,double,double,int[1+MAXINTERCELL],int,int);
	bool isSpecialPad(int);
	void DrawPolygons();
	void DrawNumbers();
	void DrawInfo();
	void FillGeoInfo();
	void AdjustZrange();
	int GetBinNumber(int);
};

#endif
