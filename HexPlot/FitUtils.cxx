#include "FitUtils.h"
#include <TMath.h>
#include "TF1.h"

//use fancy contructor magic to populate the class member variables
RangedBiLinearFitFunc::RangedBiLinearFitFunc(Double_t (*f)(Double_t*, Double_t*), vector<double> r): ranges(r), fitfunc(f) {};

//when an instance of this class is used to construct a TF1, the operator method is used as the function
Double_t RangedBiLinearFitFunc::operator() (Double_t* x, Double_t* par) {
  
  //should we use this point in the fit?
  bool accept_point = false;

  //if no ranges are given, default to the whole range
  if(ranges.size() == 0) accept_point = true;

  //test if the point is within one of the ranges
  for(unsigned int i = 0; i < ranges.size(); i += 2) {
    if(i < (ranges.size() - 1)) {
      accept_point |= ((ranges.at(i) < x[0]) && (x[0] < ranges.at(i+1)));
    }
    
    //deal with the case where an odd number of numbers are given
    if(i == (ranges.size() - 1)) {
      accept_point |= (ranges.at(i) < x[0]);
    }
  }

  //if it's in none of the ranges, reject it from the fit
  if(!accept_point) {
      TF1::RejectPoint();
  }

  //return the value either way, otherwise it doesn't plot correctly
  return fitfunc(x, par);
};
