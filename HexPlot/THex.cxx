#include "THex.h"
#include "TDirectory.h"

#include "RootUtils.h"

double HEX_TRAFO = 2./sqrt(3.);


THex::THex(){
	h_vals=new TH2F("h_vals","h_vals",100,-7,7,100,-7,7);
	h_cols=new TH1F("h_cols","h_cols",10000,0,10000);
	for (int i = 0; i < MAXINTERCELL; ++i)
	{
		h_vals_inter[i]=new TH2F(Form("h_vals_inter[%d]",i),Form("h_vals_inter[%d]",i),100,-7,7,100,-7,7);
		h_cols_inter[i]=new TH1F(Form("h_cols_inter[%d]",i),Form("h_cols_inter[%d]",i),10000,0,10000);
	}
	geotree=0;
	npads=0;
	honey_halfwidth=0.5;
	honey_halfheight=2*honey_halfwidth/sqrt(3.);
	for (int i = 0; i < MAXPADNUMBER; ++i)
	{
		padtypescale[i]=1;
		padscale[i]=1;
	}
	debug=false;
	noaxis=false;
	greyoutpads=false;
	zerobased=false;
	highlightcell_treatothers="dotted";
	infostring="ALL";
	geofileisread=false;
	outputfile="hex.png";
	ztitle="";
	topleftinfo="";
	lowrightinfo="";
	lowleftinfo="";
	toprightinfo = "";
	geofile="";
	padnumberoption=0;
	zmin=0;
	zmax=0;
	numdigits=1;
	drawinterpadvalues=false;
	intercells=0;
}
void THex::Reset(){
	h_vals->Reset();
	h_cols->Reset();
	geotree=0;
	npads=0;
	honey_halfwidth=0.5;
	honey_halfheight=2*honey_halfwidth/sqrt(3.);
	for (int i = 0; i < MAXPADNUMBER; ++i)
	{
		padtypescale[i]=1;
		padscale[i]=1;
	}
	outputfile="hex.png";
	zmin=0;
	zmax=0;
	highlightpads.clear();
}
int THex::GetPadType(int padnum){
	FillGeoInfo();
	for (unsigned int i = 0; i < padnumbers.size(); ++i)
	{
		if (padnum == padnumbers.at(i)) return padtypes.at(i);
	}
	return -1;
}
double THex::GetPadSurface(int padtype){
	//areas in square micron
	double standardarea=108824232.749822;
	double padarea=1;
	if (padtype==0)      padarea=108824232.749822;//1.;// "standard";
	else if (padtype==1) padarea= 54601907.9709;//1.4;// "guard ring";
	else if (padtype==2) padarea= 53815632.292657;//0.5;// "half cell";
	else if (padtype==3) padarea= 37258667.86556;//0.4;// "mouse bite";
	else if (padtype==4) padarea= 93016364.267491;//0.7;// "around calibration cell";
	else if (padtype==5) padarea= 14527691.4154;//0.3;// "calibration cell";
	//                   padarea=109214686.563422;//d=20um
	//                   padarea=108824232.749822;//d=40um
	//                   padarea=108437842.789322;//d=60um
	//                   padarea=108052139.999822;//d=80um
	else if (padtype==6) padarea=standardarea*0.7;// "test capacity 0"
	else if (padtype==7) padarea=standardarea*1.;// "test capacity 1"
	else if (padtype==8) padarea=standardarea*1.3;// "test capacity 2"
	else if (padtype==9) padarea=standardarea*2.8;// "test capacity 3"
	else if (padtype==10) padarea= 58258917.8096164;//jumper on HPK 8inch
	else if (padtype==11) padarea= 1;//corner cut cell
	else if (padtype==12) padarea= 1;//square cell
	else if (padtype==13) padarea= 1;//triangle cell
	else if (padtype==14) padarea= 1;//circle cell
	else if (padtype==15) padarea= 1;//halfcircle cell
	else if (padtype==16) padarea= 1;//pizza cell
	else if (padtype==17) padarea= 1;//pentagon cell
	else if (padtype==18) padarea= 1;//heptagon cell
	else if (padtype==19) padarea= 1;//octagon cell
	else return 1.;
	return padarea/standardarea;
}
string THex::GetTypeString(int padtype){
	if (padtype==0) return "standard";
	else if (padtype==1) return "guard ring";
	else if (padtype==2) return "half cell";
	else if (padtype==3) return "mouse bite";
	else if (padtype==4) return "around calibration cell";
	else if (padtype==5) return "calibration cell";
	else if (padtype==6) return "test capacity 0";
	else if (padtype==7) return "test capacity 1";
	else if (padtype==8) return "test capacity 2";
	else if (padtype==9) return "test capacity 3";
	else if (padtype==10) return "jumper cell";
	else if (padtype==11) return "cornercut cell";
	else if (padtype==12) return "square cell";
	else if (padtype==13) return "triangle cell";
	else if (padtype==14) return "circle cell";
	else if (padtype==15) return "halfcircle cell";
	else if (padtype==16) return "pizza cell";
	else if (padtype==17) return "pentagon cell";
	else if (padtype==18) return "heptagon cell";
	else if (padtype==19) return "octagon cell";
	else if (padtype==20) return "ellipse cell";
	else if (padtype==21) return "edge half cell";
	else if (padtype==22) return "edge small half cell";
	else if (padtype==23) return "diamond cell";
	else return "unspecified";
}
string THex::GetPadTypeString(int padnum){
	int padtype=GetPadType(padnum);
	return GetTypeString(padtype);
}
void THex::FillGeoInfo(){
	if (geofileisread) return;
	geotree = new TTree("geotree", "geotree");
	geotree->ReadFile(geofile.c_str(), "num/I:xpos/D:ypos/D:size/D:type/I:rotation/D");
	int num, type;
	double xpos,ypos,size,rotation;
	geotree->SetBranchAddress("num",&num);
	geotree->SetBranchAddress("type",&type);
	geotree->SetBranchAddress("xpos",&xpos);
	geotree->SetBranchAddress("ypos",&ypos);
	geotree->SetBranchAddress("size",&size);
	geotree->SetBranchAddress("rotation",&rotation);
	padnumbers.clear();
	padtypes.clear();
	padxpositions.clear();
	padypositions.clear();
	padsizes.clear();
	padrotations.clear();
	for (int i = 0; i < geotree->GetEntries(); ++i)
	{
		geotree->GetEntry(i);
		if (num == 0) zerobased = true;
		if (IsInVector(num,padnumbers)) {
			printf("Warning: padnumber %d appears more than once in geometry file! Overwriting last value!\n",num );
			int pos = find(padnumbers.begin(), padnumbers.end(), num) - padnumbers.begin();
			padnumbers.at(pos)=num;
			padtypes.at(pos)=type;
			padxpositions.at(pos)=xpos;
			padypositions.at(pos)=ypos;
			padsizes.at(pos)=size;
			padrotations.at(pos)=rotation;
		}else{
			if (debug) printf("Filling geo data: num %d type %d x %f y %f size %f rot %f\n", num, type, xpos, ypos, size, rotation);
			padnumbers.push_back(num);
			padtypes.push_back(type);
			padxpositions.push_back(xpos);
			padypositions.push_back(ypos);
			padsizes.push_back(size);
			padrotations.push_back(rotation);
		}
	}
	geofileisread=true;
}
int THex::GetBinNumber(int bin_number_no_overflow){
	int bin_number(0);
	for (int i = 0; i < h_vals->GetSize(); ++i)
	{
		if (h_vals->IsBinOverflow(i)) continue;
		if (h_vals->IsBinUnderflow(i)) continue;
		++bin_number;
		if (bin_number == bin_number_no_overflow) return i;
	}
	return -1;
}
void THex::AdjustZrange(){
	double plotmax=-1e20;
	double plotmin=1e20;
	for (int pad = 0; pad <= npads; ++pad)
	{
		if (!IsInVector(pad,padnumbers)) {
			continue;
		}
		int bin=GetBinNumber(pad);
		int type=GetPadType(pad);
		if (type==-1) {
			if (debug) printf("Setting content of pad %d from %.2e to 0!\n",pad,h_vals->GetBinContent(bin) );
			h_vals->SetBinContent(bin,0);
		}
		double bincont=h_vals->GetBinContent(bin);
		bincont*=padtypescale[type];
		bincont*=padscale[pad];
		if (zerosuppress && bincont == 0) {
			if (debug) printf("Skipping  bin %d pad %d with cont %f because it is empty\n",bin,pad,bincont );
			continue;
		}
		// printf("Processing bin %d pad %d with cont %f and type %d with padscale %f\n",bin,pad,bincont,type,padscale[pad]);
		if (plotmax<bincont) plotmax=bincont;
		if (plotmin>bincont) plotmin=bincont;
	}
	// printf("Final plotmax %f\n",plotmax );
	h_vals->SetMaximum(plotmax);
	h_vals->SetMinimum(plotmin);
}
void THex::Fill(int bin, double value){
		h_vals->SetBinContent(GetBinNumber(bin), value);
		if (debug) printf("Setting hex bin content %d (%d global) to %.3e\n",bin,GetBinNumber(bin),h_vals->GetBinContent(GetBinNumber(bin)) );
		++npads;
}
void THex::Fill(TH1F* h_fill){
	for (int bin = 0; bin < h_fill->GetNbinsX(); ++bin)
	{
		h_vals->SetBinContent(GetBinNumber(bin+1), h_fill->GetBinContent(bin+1));
		if (debug) printf("Setting hex bin content %d (%d global) to %.3e (should be %.3e)\n",bin+1,GetBinNumber(bin+1),h_vals->GetBinContent(GetBinNumber(bin+1)),h_fill->GetBinContent(bin+1) );
		++npads;
	}
	// printf("Info: loaded values of %d pads!\n",npads);
}
void THex::FillInterCell(TH1F* h_fill, int intercell){
	if (intercell >= MAXINTERCELL){
		printf("Warning! Maximum number of intercell displays reached! (%d) Skipping this!\n",MAXINTERCELL);
		return;
	}
	for (int bin = 0; bin < h_fill->GetNbinsX(); ++bin)
	{
		h_vals_inter[intercell]->SetBinContent(GetBinNumber(bin+1), h_fill->GetBinContent(bin+1));
		if (debug) printf("Setting intercell %d of bin %d (%d global) to %.3e (should be %.3e)\n",intercell,bin+1,GetBinNumber(bin+1),h_vals_inter[intercell]->GetBinContent(GetBinNumber(bin+1)),h_fill->GetBinContent(bin+1) );
		++npads;
	}
	++intercell;
	drawinterpadvalues=true;
}
void THex::DrawWhiteBox(){
	TPaveText *whitebox;
	double x_min=h_vals->GetXaxis()->GetBinLowEdge(h_vals->GetXaxis()->GetFirst());
	double x_max=h_vals->GetXaxis()->GetBinUpEdge(h_vals->GetXaxis()->GetLast());
	double y_min=h_vals->GetYaxis()->GetBinLowEdge(h_vals->GetYaxis()->GetFirst());
	double y_max=h_vals->GetYaxis()->GetBinUpEdge(h_vals->GetYaxis()->GetLast());
	whitebox = new TPaveText(x_min,y_min-(y_max-y_min)*0.1,x_max+(x_max-x_min)*0.01,y_max*1.01);
	whitebox->SetFillColor(kWhite);
	whitebox->SetLineColor(kNone);
	whitebox->SetFillStyle(1001);
	whitebox->Draw("same");
}
void THex::DrawEquiPoly(double x, double y, double side, double rotation, int corners, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	int npar=corners;
	double xvals[MAXCELLCORNERS];
	double yvals[MAXCELLCORNERS];
	for (int i = 0; i < npar+1; ++i)
	{
		double angle=i*2*TMath::Pi()/npar;
		xvals[i]=x+side/2*TMath::Sin(angle);
		yvals[i]=y+side/2*TMath::Cos(angle);
	}
	TPolyLine *pline = new TPolyLine(npar+1,xvals,yvals);
	pline->SetFillColor(colarr[0]);
	pline->SetFillStyle(style);
	pline->SetLineColor(colarr[1]);
	RotatePoly(pline,x,y,rotation);
	pline->Draw("f");
	pline->Draw("");

	//now draw the inter-cell shapes
	double* rot_xvals=pline->GetX();
	double* rot_yvals=pline->GetY();
	if (drawinterpadvalues){
		int maxintercell=TMath::Min(intercells,npar);
		for (int i = 0; i < maxintercell; ++i)
		{
			int j=(i+1)%npar;
			double inter_x=(rot_xvals[i]+rot_xvals[j])/2;
			double inter_y=(rot_yvals[i]+rot_yvals[j])/2;
			double howfarin=0.05;
			//parallel to edge
			double inter_xlength=rot_xvals[i]-rot_xvals[j];
			double inter_ylength=rot_yvals[i]-rot_yvals[j];
			double inter_rot=180+TMath::ATan(inter_ylength/inter_xlength)/(2*TMath::Pi())*360;
			//decide which side of edge and move ellipse a bit inside
			double xoffset=fabs(TMath::Cos((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double yoffset=fabs(TMath::Sin((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double* center=GetPolyCenterOfGravity(pline);
			inter_x=inter_x + ((inter_x<center[0])?1:-1)*xoffset;
			inter_y=inter_y + ((inter_y<center[1])?1:-1)*yoffset;
			//determine length according to edge length
			double length=sqrt(pow(inter_xlength,2)+pow(inter_ylength,2));
			if (colarr[2+i]) DrawEllipse(inter_x,inter_y,0.8*length,side/8,inter_rot,colarr[2+i],intercellstyle);
		}
	}
}
void THex::DrawTriangle(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	DrawEquiPoly(x,y,side,rotation,3,colarr,style,intercellstyle);
}
void THex::DrawSquare(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	DrawEquiPoly(x,y,side,rotation,4,colarr,style,intercellstyle);
}
void THex::DrawPent(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	DrawEquiPoly(x,y,side,rotation,5,colarr,style,intercellstyle);
}
void THex::DrawHex(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	DrawEquiPoly(x,y,2*side,rotation,6,colarr,style,intercellstyle);
}
void THex::DrawHept(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	DrawEquiPoly(x,y,side,rotation,7,colarr,style,intercellstyle);
}
void THex::DrawOct(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	DrawEquiPoly(x,y,side,rotation,8,colarr,style,intercellstyle);
}
void THex::DrawEllipse(double x, double y, double side0, double side1, double rotation, int col, int style=1001){
	const int npar=100;
	double xvals[npar];
	double yvals[npar];
	for (int i = 0; i < npar; ++i)
	{
		double angle=i*2*TMath::Pi()/npar;
		xvals[i]=x+side0/2*TMath::Sin(angle);
		yvals[i]=y+side1/2*TMath::Cos(angle);
	}
	TPolyLine *pline = new TPolyLine(npar,xvals,yvals);
	pline->SetFillColor(col);
	pline->SetFillStyle(style);
	pline->SetLineColor(col);
	RotatePoly(pline,x,y,rotation);
	pline->Draw("f");
	pline->Draw("");
}
void THex::DrawCircle(double x, double y, double side, double rotation, int col, int style=1001){
	DrawEllipse(x,y,side,side,rotation,col,style);
}
void THex::DrawHexMousebite(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	const int npar=6;
	double xvals[npar];
	double yvals[npar];
	xvals[0]=x;
	xvals[1]=x+side/HEX_TRAFO;
	xvals[2]=x;
	xvals[3]=x-side/HEX_TRAFO;
	xvals[4]=x-side/HEX_TRAFO;
	xvals[5]=xvals[0];
	yvals[0]=y;
	yvals[1]=y-side/2;
	yvals[2]=y-side;
	yvals[3]=y-side/2;
	yvals[4]=y;
	yvals[5]=yvals[0];
	TPolyLine *pline = new TPolyLine(npar,xvals,yvals);
	pline->SetFillColor(colarr[0]);
	pline->SetFillStyle(style);
	pline->SetLineColor(colarr[1]);
	RotatePoly(pline,x,y,rotation);
	pline->Draw("f");
	pline->Draw("");

	//now draw the inter-cell shapes
	double* rot_xvals=pline->GetX();
	double* rot_yvals=pline->GetY();
	if (drawinterpadvalues){
		int maxintercell=TMath::Min(intercells,npar);
		for (int i = 0; i < maxintercell; ++i)
		{
			int j=(i+1)%npar;
			double inter_x=(rot_xvals[i]+rot_xvals[j])/2;
			double inter_y=(rot_yvals[i]+rot_yvals[j])/2;
			double howfarin=0.05;
			//parallel to edge
			double inter_xlength=rot_xvals[i]-rot_xvals[j];
			double inter_ylength=rot_yvals[i]-rot_yvals[j];
			double inter_rot=180+TMath::ATan(inter_ylength/inter_xlength)/(2*TMath::Pi())*360;
			//decide which side of edge and move ellipse a bit inside
			double xoffset=fabs(TMath::Cos((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double yoffset=fabs(TMath::Sin((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double* center=GetPolyCenterOfGravity(pline);
			inter_x=inter_x + ((inter_x<center[0])?1:-1)*xoffset;
			inter_y=inter_y + ((inter_y<center[1])?1:-1)*yoffset;
			//determine length according to edge length
			double length=sqrt(pow(inter_xlength,2)+pow(inter_ylength,2));
			if (colarr[2+i]) DrawEllipse(inter_x,inter_y,0.8*length,side/8,inter_rot,colarr[2+i],intercellstyle);
		}
	}
}
void THex::DrawHexCornercut(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	const int npar=6;
	double xvals[npar];
	double yvals[npar];
	xvals[0]=x+side/HEX_TRAFO;
	xvals[1]=x+side/HEX_TRAFO;
	xvals[2]=x;
	xvals[3]=x-side/HEX_TRAFO;
	xvals[4]=x-side/HEX_TRAFO;
	xvals[5]=xvals[0];
	yvals[0]=y+side/2;
	yvals[1]=y-side/2;
	yvals[2]=y-side;
	yvals[3]=y-side/2;
	yvals[4]=y+side/2;
	yvals[5]=yvals[0];
	TPolyLine *pline = new TPolyLine(npar,xvals,yvals);
	pline->SetFillColor(colarr[0]);
	pline->SetFillStyle(style);
	pline->SetLineColor(colarr[1]);
	RotatePoly(pline,x,y,rotation);
	pline->Draw("f");
	pline->Draw("");

	//now draw the inter-cell shapes
	double* rot_xvals=pline->GetX();
	double* rot_yvals=pline->GetY();
	if (drawinterpadvalues){
		int maxintercell=TMath::Min(intercells,npar);
		for (int i = 0; i < maxintercell; ++i)
		{
			int j=(i+1)%npar;
			double inter_x=(rot_xvals[i]+rot_xvals[j])/2;
			double inter_y=(rot_yvals[i]+rot_yvals[j])/2;
			double howfarin=0.05;
			//parallel to edge
			double inter_xlength=rot_xvals[i]-rot_xvals[j];
			double inter_ylength=rot_yvals[i]-rot_yvals[j];
			double inter_rot=180+TMath::ATan(inter_ylength/inter_xlength)/(2*TMath::Pi())*360;
			//decide which side of edge and move ellipse a bit inside
			double xoffset=fabs(TMath::Cos((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double yoffset=fabs(TMath::Sin((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double* center=GetPolyCenterOfGravity(pline);
			inter_x=inter_x + ((inter_x<center[0])?1:-1)*xoffset;
			inter_y=inter_y + ((inter_y<center[1])?1:-1)*yoffset;
			//determine length according to edge length
			double length=sqrt(pow(inter_xlength,2)+pow(inter_ylength,2));
			if (colarr[2+i]) DrawEllipse(inter_x,inter_y,0.8*length,2*side/8,inter_rot,colarr[2+i],intercellstyle);
		}
	}
}
void THex::DrawHexOtherHalf(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	double smallerfrac=0; //remove even more of the cell
	const int npar=6;
	double xvals[npar];
	double yvals[npar];
	xvals[0]=x;
	xvals[1]=x;
	xvals[2]=x-(1-smallerfrac)*side/2;
	xvals[3]=x-side;
	xvals[4]=x-(1-smallerfrac)*side/2;
	xvals[5]=xvals[0];
	yvals[0]=y+side/HEX_TRAFO;
	yvals[1]=y-side/HEX_TRAFO;
	yvals[2]=y-side/HEX_TRAFO;
	yvals[3]=y;
	yvals[4]=y+side/HEX_TRAFO;
	yvals[5]=yvals[0];
	TPolyLine *pline = new TPolyLine(npar,xvals,yvals);
	pline->SetFillColor(colarr[0]);
	pline->SetFillStyle(style);
	pline->SetLineColor(colarr[1]);
	RotatePoly(pline,x,y,rotation);
	pline->Draw("f");
	pline->Draw("");

	//now draw the inter-cell shapes
	double* rot_xvals=pline->GetX();
	double* rot_yvals=pline->GetY();
	if (drawinterpadvalues){
		int maxintercell=TMath::Min(intercells,npar);
		for (int i = 0; i < maxintercell; ++i)
		{
			int j=(i+1)%npar;
			double inter_x=(rot_xvals[i]+rot_xvals[j])/2;
			double inter_y=(rot_yvals[i]+rot_yvals[j])/2;
			double howfarin=0.05;
			//parallel to edge
			double inter_xlength=rot_xvals[i]-rot_xvals[j];
			double inter_ylength=rot_yvals[i]-rot_yvals[j];
			double inter_rot=180+TMath::ATan(inter_ylength/inter_xlength)/(2*TMath::Pi())*360;
			//decide which side of edge and move ellipse a bit inside
			double xoffset=fabs(TMath::Cos((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double yoffset=fabs(TMath::Sin((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double* center=GetPolyCenterOfGravity(pline);
			inter_x=inter_x + ((inter_x<center[0])?1:-1)*xoffset;
			inter_y=inter_y + ((inter_y<center[1])?1:-1)*yoffset;
			//determine length according to edge length
			double length=sqrt(pow(inter_xlength,2)+pow(inter_ylength,2));
			if (colarr[2+i]) DrawEllipse(inter_x,inter_y,0.8*length,2*side/8,inter_rot,colarr[2+i],intercellstyle);
		}
	}
}
void THex::DrawDiamond(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	double smallerfrac=0.7; //remove even more of the cell
	const int npar=6;
	double xvals[npar];
	double yvals[npar];
	xvals[0]=x + (1-smallerfrac)*side/HEX_TRAFO;
	xvals[1]=x + (1-smallerfrac)*side/HEX_TRAFO;
	xvals[2]=x;
	xvals[3]=x-side/2;
	xvals[4]=x;
	xvals[5]=xvals[0];
	yvals[0]=y+smallerfrac*side/HEX_TRAFO;
	yvals[1]=y-smallerfrac*side/HEX_TRAFO;
	yvals[2]=y-side/HEX_TRAFO;
	yvals[3]=y;
	yvals[4]=y+side/HEX_TRAFO;
	yvals[5]=yvals[0];
	TPolyLine *pline = new TPolyLine(npar,xvals,yvals);
	pline->SetFillColor(colarr[0]);
	pline->SetFillStyle(style);
	pline->SetLineColor(colarr[1]);
	RotatePoly(pline,x,y,rotation);
	pline->Draw("f");
	pline->Draw("");

	//now draw the inter-cell shapes
	double* rot_xvals=pline->GetX();
	double* rot_yvals=pline->GetY();
	if (drawinterpadvalues){
		int maxintercell=TMath::Min(intercells,npar);
		for (int i = 0; i < maxintercell; ++i)
		{
			int j=(i+1)%npar;
			double inter_x=(rot_xvals[i]+rot_xvals[j])/2;
			double inter_y=(rot_yvals[i]+rot_yvals[j])/2;
			double howfarin=0.05;
			//parallel to edge
			double inter_xlength=rot_xvals[i]-rot_xvals[j];
			double inter_ylength=rot_yvals[i]-rot_yvals[j];
			double inter_rot=180+TMath::ATan(inter_ylength/inter_xlength)/(2*TMath::Pi())*360;
			//decide which side of edge and move ellipse a bit inside
			double xoffset=fabs(TMath::Cos((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double yoffset=fabs(TMath::Sin((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double* center=GetPolyCenterOfGravity(pline);
			inter_x=inter_x + ((inter_x<center[0])?1:-1)*xoffset;
			inter_y=inter_y + ((inter_y<center[1])?1:-1)*yoffset;
			//determine length according to edge length
			double length=sqrt(pow(inter_xlength,2)+pow(inter_ylength,2));
			if (colarr[2+i]) DrawEllipse(inter_x,inter_y,0.8*length,2*side/8,inter_rot,colarr[2+i],intercellstyle);
		}
	}
}
void THex::DrawHexSmallOtherHalf(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	double smallerfrac=0.5; //remove even more of the cell
	const int npar=6;
	double xvals[npar];
	double yvals[npar];
	xvals[0]=x - smallerfrac*side/2;
	xvals[1]=x - smallerfrac*side/2;
	xvals[2]=x-side/2;
	xvals[3]=x-side;
	xvals[4]=x-side/2;
	xvals[5]=xvals[0];
	yvals[0]=y+side/HEX_TRAFO;
	yvals[1]=y-side/HEX_TRAFO;
	yvals[2]=y-side/HEX_TRAFO;
	yvals[3]=y;
	yvals[4]=y+side/HEX_TRAFO;
	yvals[5]=yvals[0];
	TPolyLine *pline = new TPolyLine(npar,xvals,yvals);
	pline->SetFillColor(colarr[0]);
	pline->SetFillStyle(style);
	pline->SetLineColor(colarr[1]);
	RotatePoly(pline,x,y,rotation);
	pline->Draw("f");
	pline->Draw("");

	//now draw the inter-cell shapes
	double* rot_xvals=pline->GetX();
	double* rot_yvals=pline->GetY();
	if (drawinterpadvalues){
		int maxintercell=TMath::Min(intercells,npar);
		for (int i = 0; i < maxintercell; ++i)
		{
			int j=(i+1)%npar;
			double inter_x=(rot_xvals[i]+rot_xvals[j])/2;
			double inter_y=(rot_yvals[i]+rot_yvals[j])/2;
			double howfarin=0.05;
			//parallel to edge
			double inter_xlength=rot_xvals[i]-rot_xvals[j];
			double inter_ylength=rot_yvals[i]-rot_yvals[j];
			double inter_rot=180+TMath::ATan(inter_ylength/inter_xlength)/(2*TMath::Pi())*360;
			//decide which side of edge and move ellipse a bit inside
			double xoffset=fabs(TMath::Cos((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double yoffset=fabs(TMath::Sin((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double* center=GetPolyCenterOfGravity(pline);
			inter_x=inter_x + ((inter_x<center[0])?1:-1)*xoffset;
			inter_y=inter_y + ((inter_y<center[1])?1:-1)*yoffset;
			//determine length according to edge length
			double length=sqrt(pow(inter_xlength,2)+pow(inter_ylength,2));
			if (colarr[2+i]) DrawEllipse(inter_x,inter_y,0.8*length,2*side/8,inter_rot,colarr[2+i],intercellstyle);
		}
	}

}
void THex::DrawHexHalf(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	double smallerfrac=0; //remove even more of the cell
	const int npar=5;
	double xvals[npar];
	double yvals[npar];
	xvals[0]=x-smallerfrac*side/HEX_TRAFO;
	xvals[1]=x-side/HEX_TRAFO;
	xvals[2]=x-side/HEX_TRAFO;
	xvals[3]=x-smallerfrac*side/HEX_TRAFO;
	xvals[4]=xvals[0];
	yvals[0]=y+(0.5+0.5*(1-smallerfrac))*side;
	yvals[1]=y+side/2;
	yvals[2]=y-side/2;
	yvals[3]=y-(0.5+0.5*(1-smallerfrac))*side;
	yvals[4]=yvals[0];
	TPolyLine *pline = new TPolyLine(npar,xvals,yvals);
	pline->SetFillColor(colarr[0]);
	pline->SetFillStyle(style);
	pline->SetLineColor(colarr[1]);
	RotatePoly(pline,x,y,rotation);
	pline->Draw("f");
	pline->Draw("");

	//now draw the inter-cell shapes
	double* rot_xvals=pline->GetX();
	double* rot_yvals=pline->GetY();
	if (drawinterpadvalues){
		int maxintercell=TMath::Min(intercells,npar);
		for (int i = 0; i < maxintercell; ++i)
		{
			int j=(i+1)%npar;
			double inter_x=(rot_xvals[i]+rot_xvals[j])/2;
			double inter_y=(rot_yvals[i]+rot_yvals[j])/2;
			double howfarin=0.05;
			//parallel to edge
			double inter_xlength=rot_xvals[i]-rot_xvals[j];
			double inter_ylength=rot_yvals[i]-rot_yvals[j];
			double inter_rot=180+TMath::ATan(inter_ylength/inter_xlength)/(2*TMath::Pi())*360;
			//decide which side of edge and move ellipse a bit inside
			double xoffset=fabs(TMath::Cos((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double yoffset=fabs(TMath::Sin((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double* center=GetPolyCenterOfGravity(pline);
			inter_x=inter_x + ((inter_x<center[0])?1:-1)*xoffset;
			inter_y=inter_y + ((inter_y<center[1])?1:-1)*yoffset;
			//determine length according to edge length
			double length=sqrt(pow(inter_xlength,2)+pow(inter_ylength,2));
			if (colarr[2+i]) DrawEllipse(inter_x,inter_y,0.8*length,2*side/8,inter_rot,colarr[2+i],intercellstyle);
		}
	}
}
void THex::DrawHexJumper(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	double jumperwidth=0.07;
	double jumperheight=1.5;
	const int npar=10;
	double xvals[npar];
	double yvals[npar];
	xvals[0]=x-jumperwidth*side;
	xvals[1]=x-jumperwidth*side;
	xvals[2]=x+jumperwidth*side;
	xvals[3]=x+jumperwidth*side;
	xvals[4]=x+side/HEX_TRAFO;
	xvals[5]=x+side/HEX_TRAFO;
	xvals[6]=x;
	xvals[7]=x-side/HEX_TRAFO;
	xvals[8]=x-side/HEX_TRAFO;
	xvals[9]=xvals[0];
	yvals[0]=y+(1-jumperwidth)*side;
	yvals[1]=y+(1-jumperwidth)*side+jumperheight*side;
	yvals[2]=y+(1-jumperwidth)*side+jumperheight*side;
	yvals[3]=y+(1-jumperwidth)*side;
	yvals[4]=y+side/2;
	yvals[5]=y-side/2;
	yvals[6]=y-side;
	yvals[7]=y-side/2;
	yvals[8]=y+side/2;
	yvals[9]=yvals[0];
	TPolyLine *pline = new TPolyLine(npar,xvals,yvals);
	pline->SetFillColor(colarr[0]);
	pline->SetFillStyle(style);
	pline->SetLineColor(colarr[1]);
	RotatePoly(pline,x,y,rotation);
	pline->Draw("f");
	pline->Draw("");

	//now draw the inter-cell shapes
	int starthere=3;
	double* rot_xvals=pline->GetX();
	double* rot_yvals=pline->GetY();
	if (drawinterpadvalues){
		int maxintercell=TMath::Min(intercells,npar);
		for (int i = 0; i < maxintercell; ++i)
		{
			int j=(i+1+starthere)%npar;
			double inter_x=(rot_xvals[i+starthere]+rot_xvals[j])/2;
			double inter_y=(rot_yvals[i+starthere]+rot_yvals[j])/2;
			double howfarin=0.05;
			//parallel to edge
			double inter_xlength=rot_xvals[i+starthere]-rot_xvals[j];
			double inter_ylength=rot_yvals[i+starthere]-rot_yvals[j];
			double inter_rot=180+TMath::ATan(inter_ylength/inter_xlength)/(2*TMath::Pi())*360;
			//decide which side of edge and move ellipse a bit inside
			double xoffset=fabs(TMath::Cos((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double yoffset=fabs(TMath::Sin((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double* center=GetPolyCenterOfGravity(pline);
			inter_x=inter_x + ((inter_x<center[0])?1:-1)*xoffset;
			inter_y=inter_y + ((inter_y<center[1])?1:-1)*yoffset;
			//determine length according to edge length
			double length=sqrt(pow(inter_xlength,2)+pow(inter_ylength,2));
			if (colarr[2+i]) DrawEllipse(inter_x,inter_y,0.8*length,2*side/8,inter_rot,colarr[2+i],intercellstyle);
		}
	}
}
void THex::DrawHexRing(double x, double y, double side, double width, double rotation, int colarr[2+MAXINTERCELL], int style=1001, int intercellstyle=1001){
	const int npar=14;
	double xvals[npar];
	double yvals[npar];
	side*=2;
	for (int i = 0; i < npar/2; ++i)
	{
		double angle=i*2*TMath::Pi()/(npar/2-1);
		xvals[i]=x+side/2*(1-width)*TMath::Sin(angle);
		yvals[i]=y+side/2*(1-width)*TMath::Cos(angle);
	}
	for (int i = npar/2; i < npar; ++i)
	{
		double angle=i*2*TMath::Pi()/(npar/2-1);
		xvals[i]=x+side/2*TMath::Sin(angle);
		yvals[i]=y+side/2*TMath::Cos(angle);
	}
	TPolyLine *pline = new TPolyLine(npar,xvals,yvals);
	pline->SetFillColor(colarr[0]);
	pline->SetFillStyle(style);
	pline->SetLineColor(colarr[0]);
	// pline->SetLineColor(colarr[1]);
	RotatePoly(pline,x,y,rotation);
	pline->Draw("f");
	// pline->Draw("");

	//now draw borders (simple way not possible because of connecting element between outer and inner ring)
	DrawHex(x, y, side/2, rotation, colarr, 0, 0);
	DrawHex(x, y, side/2*(1-width), rotation, colarr, 0, 0);
	

	//now draw the inter-cell shapes
	int starthere=npar/2;
	double* rot_xvals=pline->GetX();
	double* rot_yvals=pline->GetY();
	if (drawinterpadvalues){
		int maxintercell=TMath::Min(intercells,npar);
		for (int i = 0; i < maxintercell; ++i)
		{
			int j=(i+1+starthere)%npar;
			double inter_x=(rot_xvals[i+starthere]+rot_xvals[j])/2;
			double inter_y=(rot_yvals[i+starthere]+rot_yvals[j])/2;
			double howfarin=0.05;
			//parallel to edge
			double inter_xlength=rot_xvals[i+starthere]-rot_xvals[j];
			double inter_ylength=rot_yvals[i+starthere]-rot_yvals[j];
			double inter_rot=180+TMath::ATan(inter_ylength/inter_xlength)/(2*TMath::Pi())*360;
			//decide which side of edge and move ellipse a bit inside
			double xoffset=fabs(TMath::Cos((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double yoffset=fabs(TMath::Sin((90-inter_rot)/360*2*TMath::Pi())*side*howfarin);
			double* center=GetPolyCenterOfGravity(pline);
			inter_x=inter_x + ((inter_x<center[0])?1:-1)*xoffset;
			inter_y=inter_y + ((inter_y<center[1])?1:-1)*yoffset;
			//determine length according to edge length
			double length=sqrt(pow(inter_xlength,2)+pow(inter_ylength,2));
			if (colarr[2+i]) DrawEllipse(inter_x,inter_y,0.8*length,side/8,inter_rot,colarr[2+i],intercellstyle);
		}
	}
}
void THex::DrawGuardRing(double x, double y, double side, double width, double rotation, int colarr[2+MAXINTERCELL], int style=1001){ //can replace DrawHexRing with angleoffset=0
	const int npar=26;
	double xvals[npar];
	double yvals[npar];
	double angleoffset=0.045*TMath::Pi(); //smaller -> larger mouse bite distance
	for (int i = 0; i < npar/2; ++i)
	{
		// double angle=i*2*TMath::Pi()/(npar/2-1)+TMath::Pi()/4;
		bool isEven=(i%2==0);
		double angle=i*2*TMath::Pi()/(npar/2-1)+TMath::Pi()/4+(isEven?+angleoffset:-angleoffset);
		xvals[i]=x+side/2*TMath::Sin(angle);
		yvals[i]=y+side/2*TMath::Cos(angle);
	}
	for (int i = npar/2; i < npar; ++i)
	{
		// double angle=i*2*TMath::Pi()/(npar/2-1)+TMath::Pi()/4;
		bool isEven=(i%2==0);
		double angle=i*2*TMath::Pi()/(npar/2-1)+TMath::Pi()/4+(isEven?+angleoffset:-angleoffset);
		xvals[i]=x+side/2*(1-width)*TMath::Sin(angle);
		yvals[i]=y+side/2*(1-width)*TMath::Cos(angle);
	}
	TPolyLine *pline = new TPolyLine(npar,xvals,yvals);
	pline->SetFillColor(colarr[0]);
	pline->SetFillStyle(style);
	pline->SetLineColor(colarr[1]);
	RotatePoly(pline,x,y,rotation);
	pline->Draw("f");
	pline->Draw("");
}
void THex::DrawHalfcircle(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001){
	const int npar=50;
	double xvals[npar];
	double yvals[npar];
	for (int i = 0; i < npar; ++i)
	{
		double angle=i*TMath::Pi()/npar;
		xvals[i]=x+side/2*TMath::Sin(angle);
		yvals[i]=y+side/2*TMath::Cos(angle);
	}
	TPolyLine *pline = new TPolyLine(npar,xvals,yvals);
	pline->SetFillColor(colarr[0]);
	pline->SetFillStyle(style);
	pline->SetLineColor(colarr[1]);
	RotatePoly(pline,x,y,rotation);
	pline->Draw("f");
	pline->Draw("");
}
void THex::DrawPizza(double x, double y, double side, double rotation, int colarr[2+MAXINTERCELL], int style=1001){
	const int npar=25;
	double xvals[npar+1];
	double yvals[npar+1];
	xvals[0]=x;
	yvals[0]=y;
	for (int i = 1; i < npar; ++i)
	{
		double angle=i*TMath::Pi()/2/(npar-1);
		xvals[i]=x+side/2*TMath::Sin(angle);
		yvals[i]=y+side/2*TMath::Cos(angle);
	}
	xvals[npar]=xvals[0];
	yvals[npar]=yvals[0];
	TPolyLine *pline = new TPolyLine(npar+1,xvals,yvals);
	pline->SetFillColor(colarr[0]);
	pline->SetFillStyle(style);
	pline->SetLineColor(colarr[1]);
	RotatePoly(pline,x,y,rotation);
	pline->Draw("f");
	pline->Draw("");
}
void THex::DrawPolygons(){
	for (unsigned int i = 0; i < padnumbers.size(); ++i)
	{
		int num=padnumbers.at(i);
		int type=padtypes.at(i);
		double xpos=padxpositions.at(i);
		double ypos=padypositions.at(i);
		double size=padsizes.at(i);
		if (debug) printf("Drawing hex pad %d xpos %f ypos %f size %f type %d\n",num, xpos,ypos,size,type );
		if (padtypescale[type]!=1){
			double oldval=h_vals->GetBinContent(GetBinNumber(num));
			double newval=oldval*padtypescale[type];
			h_vals->SetBinContent(GetBinNumber(num),newval);
			printf("This is the %s value: %.3e. For display scaling it by %.2f to %.3e!\n",GetPadTypeString(num).c_str(),oldval,padtypescale[type],h_vals->GetBinContent(GetBinNumber(num)));
		}
		if (padscale[num]!=1){
			double oldval=h_vals->GetBinContent(GetBinNumber(num));
			double newval=oldval*padscale[num];
			h_vals->SetBinContent(GetBinNumber(num),newval);
			printf("This is the pad %d value: %.3e. For display scaling it by %.2f to %.3e!\n",num,oldval,padscale[num],h_vals->GetBinContent(GetBinNumber(num)));
		}
	}

	//first get colors of the hexagons
	double oldmax=h_vals->GetMaximum();
	h_vals->SetMaximum(oldmax*1.01); //this is for the color retrieval to work. The maximum color is always buggy.
	gPad->Update();
	TPaletteAxis *palette = (TPaletteAxis*)h_vals->GetListOfFunctions()->FindObject("palette");
	int currentpad(0);
	for (int i = 0; i < npads+1; ++i)
	{
		int this_bin = zerobased ? i+1 : i;
		if (!IsInVector(i,padnumbers)) {
			if (debug) printf("Bin %d (%d) content %f, color_id %d not in geo file -> skip it\n", this_bin,GetBinNumber(this_bin), h_vals->GetBinContent(GetBinNumber(this_bin)), palette->GetValueColor(h_vals->GetBinContent(GetBinNumber(this_bin))));
			continue;
		}
		//first for main cell value histogram
		double binc=h_vals->GetBinContent(GetBinNumber(this_bin));
		Int_t color_id=palette->GetValueColor(binc);
		if (greyoutpads){
			h_cols->SetBinContent(currentpad+1,15);
		}else if (binc == 0){
			if (debug) printf("retrieving color bin %d/%d (global %d): %d but bin content %f -> set white\n",this_bin,npads,GetBinNumber(this_bin),color_id,binc);
			h_cols->SetBinContent(currentpad+1,0);
		}else if (zmin!=zmax && binc>zmax){
			if (debug) printf("retrieving color bin %d/%d (global %d): %d but bin content %f > max %f -> set red\n",this_bin,npads,GetBinNumber(this_bin),color_id,binc,zmax);
			h_cols->SetBinContent(currentpad+1,2);
		}else if (zmin!=zmax &&  binc<zmin){
			if (debug) printf("retrieving color bin %d/%d (global %d): %d but bin content %f < min %f -> set grey\n",this_bin,npads,GetBinNumber(this_bin),color_id,binc,zmin);
			h_cols->SetBinContent(currentpad+1,18);
		}else if (binc != binc){
			if (debug) printf("retrieving color bin %d/%d (global %d): %d but bin content %f -> set grey\n",this_bin,npads,GetBinNumber(this_bin),color_id,binc);
			h_cols->SetBinContent(currentpad+1,18);
		}else{
			if (debug) printf("retrieving color bin %d/%d (global %d) with content %f: %d\n",this_bin,npads,GetBinNumber(this_bin),binc,color_id);
			h_cols->SetBinContent(currentpad+1,color_id);
		}
		//then for inter-cell histograms
		for (int j = 0; j < intercells; ++j)
		{
			double inter_zmax=h_vals->GetMaximum();
			double inter_zmin=h_vals->GetMinimum();
			binc=h_vals_inter[j]->GetBinContent(GetBinNumber(this_bin));
			color_id=palette->GetValueColor(binc);
			if (binc == 0){
				if (debug && drawinterpadvalues) printf(" intercell %d color: %d but bin content %f -> set white\n",j,color_id,binc);
				h_cols_inter[j]->SetBinContent(currentpad+1,0);
			}else if (inter_zmin!=inter_zmax && binc>inter_zmax){
				if (debug && drawinterpadvalues) printf(" intercell %d color: %d but bin content %f > max of main hexagons %f -> set red\n",j,color_id,binc,inter_zmax);
				h_cols_inter[j]->SetBinContent(currentpad+1,2);
			}else if (inter_zmin!=inter_zmax &&  binc<inter_zmin){
				if (debug && drawinterpadvalues) printf(" intercell %d color: %d but bin content %f < min of main hexagons %f -> set grey\n",j,color_id,binc,inter_zmin);
				h_cols_inter[j]->SetBinContent(currentpad+1,18);
			}else if (binc != binc){
				if (debug && drawinterpadvalues) printf(" intercell %d color: %d but bin content %f -> set grey\n",j,color_id,binc);
				h_cols_inter[j]->SetBinContent(currentpad+1,18);
			}else{
				if (debug && drawinterpadvalues) printf(" intercell %d color with content %f: %d\n",j,binc,color_id);
				h_cols_inter[j]->SetBinContent(currentpad+1,color_id);
			}
		}
		++currentpad;
	}

	//now draw the hexagons
	double dist = 1;
	for (unsigned int i = 0; i < padnumbers.size(); ++i)
	{
		int type=padtypes.at(i);
		double xpos=padxpositions.at(i);
		double ypos=padypositions.at(i);
		double size=padsizes.at(i);
		double rotation=padrotations.at(i);
		double x=xpos*dist;
		double y=ypos*dist/HEX_TRAFO;

		// int style=IsInVector(i+1,highlightpads)?3003:1001;
		int style=1001;
		int intercellstyle=1001;
		//get color array
		int colarr[2+MAXINTERCELL];
		for (int j = 0; j < 2+MAXINTERCELL; ++j)
		{
			colarr[j]=0;
		}
		colarr[0]=int(h_cols->GetBinContent(i+1));
		colarr[1]=int(h_cols->GetBinContent(i+1));
		for (int j = 0; j < intercells; ++j)
		{
			colarr[2+j]=int(h_cols_inter[j]->GetBinContent(i+1));
		}

		if(isSpecialPad(i)) {
			if (Contains(highlightcell_treatothers,"greyout")) {colarr[0]=18; colarr[1]=18;}
			if  (Contains(highlightcell_treatothers,"dense")) style=3001;
			else if (Contains(highlightcell_treatothers,"dotted")) style=3002;
			else if  (Contains(highlightcell_treatothers,"checked")) style=3144;
			else if  (Contains(highlightcell_treatothers,"loose")) style=3003;
			if (Contains(highlightcell_treatothers,"blackborder")) colarr[1]=kBlack;
		}else if (highlightpads.size()>0 ){
			if (Contains(highlightcell_treatothers,"redborder")) colarr[1]=kRed;
		}

		if (type == 1) { //guard ring
			DrawGuardRing(x,y,size,0.03,rotation,colarr,style);
			// DrawGuardRing(x,y,size,0.03,rotation,int(h_cols->GetBinContent(i+1)),style,intercellstyle);
		}
		else if (type == 2) { //half cell
			DrawHexHalf(x,y,size,rotation,colarr,style,intercellstyle);
		}
		else if (type == 3) { //mouse bite
			DrawHexMousebite(x,y,size,rotation,colarr,style,intercellstyle);
		}
		else if (type == 4) { //calibration cell on top
			DrawHexRing(x,y,size,0.4,rotation,colarr,style,intercellstyle);
		}
		else if (type == 5) { //calibration cell
			DrawHex(x,y,size,rotation,colarr,style,intercellstyle);
		}
		else if (type == 10) { //jumper
			DrawHexJumper(x,y,size,rotation,colarr,style,intercellstyle);
		}
		else if (type == 11) { //cut edge cell
			DrawHexCornercut(x,y,size,rotation,colarr,style,intercellstyle);
		}
		else if (type == 12) { //square cell
			DrawSquare(x,y,size,rotation,colarr,style,intercellstyle);
		}
		else if (type == 13) { //triangle cell
			DrawTriangle(x,y,size,rotation,colarr,style,intercellstyle);
		}
		else if (type == 14) { //circle cell
			DrawCircle(x,y,size,rotation,colarr[0],style);
		}
		else if (type == 15) { //halfcircle cell
			DrawHalfcircle(x,y,size,rotation,colarr,style);
		}
		else if (type == 16) { //pizza cell
			DrawPizza(x,y,size,rotation,colarr,style);
		}
		else if (type == 17) { //pentagon cell
			DrawPent(x,y,size,rotation,colarr,style,intercellstyle);
		}
		else if (type == 18) { //heptagon cell
			DrawHept(x,y,size,rotation,colarr,style,intercellstyle);
		}
		else if (type == 19) { //octagon cell
			DrawOct(x,y,size,rotation,colarr,style,intercellstyle);
		}
		else if (type == 20) { //ellipse cell
			DrawEllipse(x,y,size,size*0.66,rotation,colarr[0],style);
		}
		else if (type == 21) { //half cell split along the edges
			DrawHexOtherHalf(x,y,size,rotation,colarr,style,intercellstyle);
		}
		else if (type == 22) { //half cell split along the edges
			DrawHexSmallOtherHalf(x,y,size,rotation,colarr,style,intercellstyle);
		}
		else if (type == 23) { //half cell split along the edges
			DrawDiamond(x,y,size,rotation,colarr,style,intercellstyle);
		}
		else {
			DrawHex(x,y,size,rotation,colarr,style,intercellstyle);
		}
	}
	h_vals->GetXaxis()->SetAxisColor(0,0);
	h_vals->GetYaxis()->SetAxisColor(0,0);
	h_vals->GetZaxis()->SetAxisColor(0,0);
	if (!noaxis) {
		h_vals->Draw("axiscolzsame"); //remove any overlap with zaxis
	}
}
bool THex::isSpecialPad(int i){
	return (highlightpads.size()>0 && find(highlightpads.begin(), highlightpads.end(), i+1) - highlightpads.begin()>=int(highlightpads.size()));
}
void THex::DrawNumbers(){
	if (padnumberoption==3) return;
	double dist = 1;
	for (unsigned int i = 0; i < padnumbers.size(); ++i)
	{
		int num=padnumbers.at(i);
		int type=padtypes.at(i);
		double xpos=padxpositions.at(i);
		double ypos=padypositions.at(i);
		double size=padsizes.at(i);
		// double rotation=padrotations.at(i);
		double x=xpos*dist;
		double y=ypos*dist/HEX_TRAFO;

		double padcont=h_vals->GetBinContent(GetBinNumber(zerobased ? num+1 : num));
		if (num != -1){
			TPaveText *padnum=0;
			if (type == 1){
				padnum = new TPaveText(x-0.525*size/HEX_TRAFO,y,x-0.525*size/HEX_TRAFO,y);
			}
			else if (type == 4){
				padnum = new TPaveText(x,y+0.7*size,x,y+0.7*size);
			}
			else {
				padnum = new TPaveText(x,y,x,y);
			}
			padnum->SetTextAlign(22);
			padnum->SetFillColor(kNone);
			padnum->SetLineColor(kNone);
			padnum->SetTextSize(0.02);
			padnum->SetTextColor(kBlack);
			if (padnumberoption==0) padnum->AddText(Form("%d",num));
			else if (padnumberoption==1) padnum->AddText(Form("%.*f",numdigits,padcont));
			else if (padnumberoption==2) padnum->AddText(Form("%d",type));

			if(isSpecialPad(i)){
				if (Contains(highlightcell_treatothers,"nonumbers")) continue;
				else if (Contains(highlightcell_treatothers,"smallnumbers")) padnum->SetTextSize(0.013);
			}
			padnum->Draw("same");
		}
	}
}
void THex::DrawInfo(){

	bool draw_all = infostring == "ALL";
	bool draw_none = (infostring.find("NONE")<infostring.size());
	bool draw_topleft = draw_all || (infostring.find("TOPLEFT")<infostring.size());
	bool draw_topright = draw_all || (infostring.find("TOPRIGHT")<infostring.size());
	bool draw_lowleft = draw_all || (infostring.find("LOWLEFT")<infostring.size());
	bool draw_lowright = draw_all || (infostring.find("LOWRIGHT")<infostring.size());
	if (draw_none) return;

	TPaveText *padnum = 0;

	if (draw_topleft){
		padnum = new TPaveText(-6, 6.8, -6, 6.8);
		padnum->SetFillColor(kNone);
		padnum->SetLineColor(kNone);
		padnum->SetTextSize(0.02);
		padnum->SetTextAlign(12);
		padnum->SetTextColor(kBlack);
		padnum->AddText(topleftinfo.c_str());
		padnum->Draw("same");
	}

	if (draw_topright && toprightinfo.empty()){
		int nlines(1);
		std::vector<int> types_treated;
		for (unsigned int i = 0; i < padtypes.size(); ++i)
		{
			int type = padtypes.at(i);
			if (std::find(types_treated.begin(), types_treated.end(), type) != types_treated.end()) continue;
			if (padtypescale[type] != 1) ++nlines;
			types_treated.push_back(type);
		}
		for (int i = 0; i <= npads; ++i)
		{
			if (padscale[i] != 1) ++nlines;
		}
		padnum = new TPaveText(6, 7 - 0.3*nlines, 6, 7);
		padnum->SetFillColor(kNone);
		padnum->SetLineColor(kNone);
		padnum->SetTextSize(0.02);
		padnum->SetTextAlign(32);
		padnum->SetTextColor(kBlack);
		if (nlines>1) padnum->AddText("Scales applied to pad value:");
		types_treated.clear();
		for (unsigned int i = 0; i < padtypes.size(); ++i)
		{
			int type = padtypes.at(i);

			if (std::find(types_treated.begin(), types_treated.end(), type) != types_treated.end()) continue;
			if (padtypescale[type] != 1) padnum->AddText(Form("%s: %.0e", GetTypeString(type).c_str(), padtypescale[type]));
			types_treated.push_back(type);
		}
		for (int i = 0; i <= npads; ++i)
		{
			if (padscale[i] != 1) padnum->AddText(Form("Pad %d (%s): %.0e", i, GetPadTypeString(i).c_str(), padscale[i]));
		}
		padnum->Draw("same");
	}
	else if (draw_topright){
		padnum = new TPaveText(6, 7, 6, 7);
		padnum->SetFillColor(kNone);
		padnum->SetLineColor(kNone);
		padnum->SetTextSize(0.02);
		padnum->SetTextAlign(32);
		padnum->SetTextColor(kBlack);
		padnum->AddText(toprightinfo.c_str());
		padnum->Draw("same");
	}

	if (draw_lowleft){
		padnum = new TPaveText(-6, -6.8, -6, -6.8);
		padnum->SetFillColor(kNone);
		padnum->SetLineColor(kNone);
		padnum->SetTextSize(0.02);
		padnum->SetTextAlign(12);
		padnum->SetTextColor(kBlack);
		padnum->AddText(lowleftinfo.c_str());
		padnum->Draw("same");
	}

	if (draw_lowright){
		padnum = new TPaveText(6, -6.8, 6, -6.8);
		padnum->SetFillColor(kNone);
		padnum->SetLineColor(kNone);
		padnum->SetTextSize(0.02);
		padnum->SetTextAlign(32);
		padnum->SetTextColor(kBlack);
		padnum->AddText(lowrightinfo.c_str());
		padnum->Draw("same");
	}
}
void THex::Draw(){
	FillGeoInfo();
	gStyle->SetOptStat(0);

	h_vals->GetZaxis()->SetTitle(ztitle.c_str());
	h_vals->GetZaxis()->SetTitleSize(0.05);
	h_vals->GetZaxis()->SetTitleOffset(0.9);
	h_vals->SetTitle("");

	double canvwidth=750;
	TCanvas *c_hex = new TCanvas("c_hex","c_hex",800,500,int(canvwidth),int(noaxis?canvwidth:canvwidth/1.1));
	c_hex->cd();
	if (noaxis) {
		gPad->SetTopMargin(0);
		gPad->SetRightMargin(0);
		gPad->SetBottomMargin(0);
		gPad->SetLeftMargin(0);
	}
	else {
		gPad->SetTopMargin(0.04);
		gPad->SetRightMargin(0.15);
		gPad->SetBottomMargin(0.02);
		gPad->SetLeftMargin(0);
	}
	AdjustZrange();
	if (zmin!=0 || zmax!=0) h_vals->GetZaxis()->SetRangeUser(zmin,zmax);
	h_vals->Draw("colz");
	DrawWhiteBox();
	DrawPolygons();
	DrawNumbers();
	DrawInfo();
	c_hex->Print(outputfile.c_str());

	if (debug){
		string outputdir=getFileDir(outputfile);
		outputdir+=outputdir.size()?"/":"";
		TCanvas *c_values = new TCanvas("c_values","c_values",800,500,600,500);
		c_values->cd(0);
		gPad->SetRightMargin(0.2);
		h_vals->Draw("colz");
		c_values->Print(Form("%sh_vals.png",outputdir.c_str()));

		TCanvas *c_support = new TCanvas("c_support","c_support",800,500,1400,500);
		c_support->cd();
		h_cols->GetXaxis()->SetRangeUser(0,npads);
		h_cols->Draw("texthist");
		c_support->Print(Form("%sh_cols.png",outputdir.c_str()));

		delete c_values;
		delete c_support;
	}
	delete c_hex;
}
