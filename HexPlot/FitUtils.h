#include <vector>
#include <TMath.h>

using namespace std;

//wrap a fit function and cause it to only fit to points within defined ranges
class RangedBiLinearFitFunc {
  public:
    vector<double> ranges;   //a list of ranges {min1, max1, min2, max2...}
    Double_t (*fitfunc)(Double_t*, Double_t*); //the wrapped function

    //use fancy contructor magic to populate the above variables
    RangedBiLinearFitFunc(Double_t (*f)(Double_t*, Double_t*), vector<double> r);

    //when an instance of this class is used to construct a TF1, the operator method is used as the function
    Double_t operator() (Double_t* x, Double_t* par);
};