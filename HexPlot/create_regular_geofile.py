from __future__ import print_function

print('Creating regular geofile...')

filename = 'regular_geo.txt'

patterns = ['HexEdges', 'Hex', 'Hex30', 'Switch']
pattern = patterns[3]

MIN_X = -6.0
MAX_X = 6.0
MIN_Y = -7.0
MAX_Y = 7.0

padtype = 0
current_padnum = 1


def print_header(myfile):
    print('#', file=myfile)
    print('# This is an automatically generated geo file', file=myfile)
    print('# Script: ../create_regular_geofile.py', file=myfile)
    print('#', file=myfile)
    print('# draws all available cell shapes', file=myfile)
    print('#', file=myfile)
    print('# padnumber	xposition	yposition	size	type	rotation\n\n', file=myfile)


if pattern == 'HexEdges':
    padtype = 0
    padsize = 0.40
    padrot = 0

    chan_dist_x = -0.1
    chan_dist_y = -0.05

    current_x = padsize
    current_y = MAX_Y - padsize

    def draw_diamond(x, y, rot):
        global current_padnum
        current_padnum += 1
        return str(current_padnum-1) + '\t' + str(x) + '\t' + str(y) + '\t' + str(padsize*0.8) + '\t' + str(23) + '\t' + str(rot)

    def draw_hex_row(x_center, y_center, npads, edge):
        global current_padnum
        paddist_x = chan_dist_x/2 + padsize*2
        for i_chan in range(0, npads):
            this_x = x_center + i_chan * paddist_x - (npads * paddist_x) / 2
            this_y = y_center
            this_padtype = padtype
            this_padrot = padrot
            if edge == 1:
                this_padtype = 22
                this_padrot = 90
            elif edge == 2:
                this_padtype = 22
                this_padrot = 270
            elif edge == 3:
                if i_chan == 0:
                    this_padtype = 22
                    this_padrot = 150
                elif i_chan == npads-1:
                    this_padtype = 22
                    this_padrot = 30
            elif edge == 4:
                if i_chan == 0:
                    this_padtype = 22
                    this_padrot = 210
                elif i_chan == npads-1:
                    this_padtype = 22
                    this_padrot = 330
            yield str(current_padnum) + '\t' + str(this_x) + '\t' + str(this_y) + '\t' + str(padsize) + '\t' + str(this_padtype) + '\t' + str(this_padrot)
            current_padnum += 1

    def draw_hex_rows(x_center, nrows):
        paddist_y = chan_dist_y/2 + padsize*2
        for i_row in range(0, nrows):
            global current_y
            this_npads = 0
            this_edge = 0
            if i_row == 0:
                this_edge = 1
                this_npads = 8
            elif i_row < 9:
                this_edge = 3
                this_npads = i_row+10
            elif i_row == 9:
                this_npads = i_row+8
            elif i_row == nrows-1:
                this_edge = 2
                this_npads = 8
            else:
                this_edge = 4
                this_npads = 9 + nrows - i_row
            yield draw_hex_row(x_center, current_y, this_npads, this_edge)
            current_y -= paddist_y

    with open(filename, 'w') as myfile:
        print_header(myfile)

        rows = draw_hex_rows(current_x, 19)
        for row in rows:
            for cell in row:
                print(cell, file=myfile)
        lefthexoffset = 6.95
        ritehexoffset = 6.8
        print(draw_diamond(MIN_X+lefthexoffset*padsize, MAX_Y-1.7*padsize, 300), file=myfile)
        print(draw_diamond(MAX_X-ritehexoffset*padsize, MAX_Y-1.7*padsize, 240), file=myfile)
        print(draw_diamond(MIN_X-1.2*padsize, 0-padsize*0.9, 0), file=myfile)
        print(draw_diamond(MAX_X+1.3*padsize, 0-padsize*0.9, 180), file=myfile)
        print(draw_diamond(MIN_X+lefthexoffset*padsize, MIN_Y-0.2*padsize, 60), file=myfile)
        print(draw_diamond(MAX_X-ritehexoffset*padsize, MIN_Y-0.2*padsize, 120), file=myfile)

if pattern == 'Hex':
    padtype = 0
    padsize = 0.40
    padrot = 0

    chan_dist_x = -0.1
    chan_dist_y = -0.05

    current_x = padsize
    current_y = MAX_Y - padsize

    def draw_hex_row(x_center, y_center, npads):
        global current_padnum
        paddist_x = chan_dist_x/2 + padsize*2
        for i_chan in range(0, npads):
            this_x = x_center + i_chan * paddist_x - (npads * paddist_x) / 2
            this_y = y_center
            yield str(current_padnum) + '\t' + str(this_x) + '\t' + str(this_y) + '\t' + str(padsize) + '\t' + str(padtype) + '\t' + str(padrot)
            current_padnum += 1

    def draw_hex_rows(x_center, nrows):
        paddist_y = chan_dist_y/2 + padsize*2
        for i_row in range(0, nrows):
            global current_y
            this_npads = 0
            if i_row < 9:
                this_npads = i_row+9
            else:
                this_npads = 8 + nrows - i_row
            yield draw_hex_row(x_center, current_y, this_npads)
            current_y -= paddist_y

    with open(filename, 'w') as myfile:
        print_header(myfile)

        rows = draw_hex_rows(current_x, 17)
        for row in rows:
            for cell in row:
                print(cell, file=myfile)

elif pattern == 'Hex30':
    padtype = 0
    padsize = 0.45
    padrot = 30

    chan_dist_x = 0.1
    chan_dist_y = 0.05

    current_x = padsize * 3/2
    current_y = MAX_Y + padsize * 3/2

    def draw_hex_row(x_center, y_center, npads):
        global current_padnum
        paddist_x = chan_dist_x/2 + padsize*6/2
        for i_chan in range(0, npads):
            this_x = x_center + i_chan * paddist_x - (npads * paddist_x) / 2
            this_y = y_center
            yield str(current_padnum) + '\t' + str(this_x) + '\t' + str(this_y) + '\t' + str(padsize) + '\t' + str(padtype) + '\t' + str(padrot)
            current_padnum += 1

    def draw_hex_rows(x_center, nrows):
        paddist_y = chan_dist_y/2 + padsize
        for i_row in range(0, nrows):
            global current_y
            this_npads = 0
            if i_row < 9:
                this_npads = i_row+1
            elif i_row < 25:
                this_npads = 9 - i_row % 2
            elif i_row < 33:
                this_npads = nrows - i_row
            yield draw_hex_row(x_center, current_y, this_npads)
            current_y -= paddist_y

    with open(filename, 'w') as myfile:
        print_header(myfile)

        rows = draw_hex_rows(current_x, 33)
        for row in rows:
            for cell in row:
                print(cell, file=myfile)


elif pattern == 'Switch':
    padtype = 12
    current_padnum = 0

    padrot = 45
    padsize = 0.4

    chan_dist_x = 0.2
    chan_dist_y = 0.4

    current_x = MIN_X
    current_y = MAX_Y

    def draw_multiplexer(x_center, y_center):
        global current_padnum
        paddist_x = chan_dist_x/2 + padsize/2
        paddist_y = chan_dist_y/2 + padsize/2
        for i_chan in range(0, 8):
            this_x = 0
            if i_chan % 2 == 0:
                this_x = x_center - paddist_x
            else:
                this_x = x_center + paddist_x
            this_y = y_center + 2 * paddist_y - int(i_chan / 2) * paddist_y
            yield str(current_padnum) + '\t' + str(this_x) + '\t' + str(this_y) + '\t' + str(padsize) + '\t' + str(padtype) + '\t' + str(padrot)
            current_padnum += 1

    def draw_multiplexer_col(x_center):
        for i_col in range(0, 8):
            this_y = MAX_Y - i_col * (MAX_Y - MIN_Y) / 7
            yield draw_multiplexer(x_center, this_y)

    def draw_multiplexer_rows():
        for i_row in range(0, 8):
            this_x = MIN_X + i_row * (MAX_X - MIN_X) / 7
            yield draw_multiplexer_col(this_x)

    with open(filename, 'w') as myfile:
        print_header(myfile)
        rows = draw_multiplexer_rows()
        for row in rows:
            for col in row:
                for plex in col:
                    print(plex, file=myfile)
